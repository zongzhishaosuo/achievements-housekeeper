/*
 Navicat Premium Data Transfer

 Source Server         : 百度云RDS
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : 106.12.102.66:3306
 Source Schema         : achievements

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 17/06/2019 22:21:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tab_archives
-- ----------------------------
DROP TABLE IF EXISTS `tab_archives`;
CREATE TABLE `tab_archives`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `awards_belongs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `belongs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `finished` bit(1) NULL DEFAULT NULL,
  `indicator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `positioning` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `task_id` bigint(20) NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `uploader` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `year` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tab_archives_handlers
-- ----------------------------
DROP TABLE IF EXISTS `tab_archives_handlers`;
CREATE TABLE `tab_archives_handlers`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `approver` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `approver_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `archives_id` bigint(20) NULL DEFAULT NULL,
  `auditor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `auditor_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tab_certification
-- ----------------------------
DROP TABLE IF EXISTS `tab_certification`;
CREATE TABLE `tab_certification`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `id_card_front_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_card_reverse_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `personal_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `revocation` bit(1) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `submitted` bit(1) NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `verifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tab_certification_record
-- ----------------------------
DROP TABLE IF EXISTS `tab_certification_record`;
CREATE TABLE `tab_certification_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `certification_id` bigint(20) NULL DEFAULT NULL,
  `passed` bit(1) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `verifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tab_common_resource
-- ----------------------------
DROP TABLE IF EXISTS `tab_common_resource`;
CREATE TABLE `tab_common_resource`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `common_class` int(11) NULL DEFAULT NULL,
  `common_id` bigint(20) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `unique_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tab_dept
-- ----------------------------
DROP TABLE IF EXISTS `tab_dept`;
CREATE TABLE `tab_dept`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `unit_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_tlx1hfhaeyrtpgcy2cfh1eygf`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tab_dept
-- ----------------------------
INSERT INTO `tab_dept` VALUES (1, NULL, NULL, 0, NULL, 1, '1', NULL, '人事部', NULL, '0001');
INSERT INTO `tab_dept` VALUES (2, NULL, NULL, 0, NULL, 2, '2', NULL, '财务部', NULL, '0001');
INSERT INTO `tab_dept` VALUES (3, NULL, NULL, 0, NULL, 3, '3', NULL, '技术部', NULL, '0001');
INSERT INTO `tab_dept` VALUES (4, NULL, NULL, 0, NULL, 4, '4', NULL, '人事部', NULL, '0002');
INSERT INTO `tab_dept` VALUES (5, NULL, NULL, 0, NULL, 5, '5', NULL, '财务部', NULL, '0002');
INSERT INTO `tab_dept` VALUES (6, NULL, NULL, 0, NULL, 6, '6', NULL, '技术部', NULL, '0002');
INSERT INTO `tab_dept` VALUES (7, NULL, NULL, 0, NULL, 7, '7', NULL, '人事部', NULL, '0003');
INSERT INTO `tab_dept` VALUES (8, NULL, NULL, 0, NULL, 8, '8', NULL, '财务部', NULL, '0003');
INSERT INTO `tab_dept` VALUES (9, NULL, NULL, 0, NULL, 9, '9', NULL, '技术部', NULL, '0003');
INSERT INTO `tab_dept` VALUES (10, NULL, NULL, 0, NULL, 10, '10', NULL, '人事部', NULL, '0004');
INSERT INTO `tab_dept` VALUES (11, NULL, NULL, 0, NULL, 11, '11', NULL, '财务部', NULL, '0004');
INSERT INTO `tab_dept` VALUES (12, NULL, NULL, 0, NULL, 12, '12', NULL, '技术部', NULL, '0004');
INSERT INTO `tab_dept` VALUES (13, NULL, NULL, 0, NULL, 13, '13', NULL, '人事部', NULL, '0005');
INSERT INTO `tab_dept` VALUES (14, NULL, NULL, 0, NULL, 14, '14', NULL, '财务部', NULL, '0005');
INSERT INTO `tab_dept` VALUES (15, NULL, NULL, 0, NULL, 15, '15', NULL, '技术部', NULL, '0005');
INSERT INTO `tab_dept` VALUES (16, NULL, NULL, 0, NULL, 16, '16', NULL, '人事部', NULL, '0006');
INSERT INTO `tab_dept` VALUES (17, NULL, NULL, 0, NULL, 17, '17', NULL, '财务部', NULL, '0006');
INSERT INTO `tab_dept` VALUES (18, NULL, NULL, 0, NULL, 18, '18', NULL, '技术部', NULL, '0006');

-- ----------------------------
-- Table structure for tab_notepad
-- ----------------------------
DROP TABLE IF EXISTS `tab_notepad`;
CREATE TABLE `tab_notepad`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `have_attach` bit(1) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tab_notice
-- ----------------------------
DROP TABLE IF EXISTS `tab_notice`;
CREATE TABLE `tab_notice`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `end_time` datetime(0) NULL DEFAULT NULL,
  `have_attach` bit(1) NULL DEFAULT NULL,
  `overhead` bit(1) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `revocation` bit(1) NULL DEFAULT NULL,
  `start_time` datetime(0) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `subtitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tab_notice_scope
-- ----------------------------
DROP TABLE IF EXISTS `tab_notice_scope`;
CREATE TABLE `tab_notice_scope`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `dept_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notice_id` bigint(20) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `unit_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tab_phone_code
-- ----------------------------
DROP TABLE IF EXISTS `tab_phone_code`;
CREATE TABLE `tab_phone_code`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `timestamp` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tab_task
-- ----------------------------
DROP TABLE IF EXISTS `tab_task`;
CREATE TABLE `tab_task`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `end_time` datetime(0) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `revocation` bit(1) NULL DEFAULT NULL,
  `start_time` datetime(0) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `subtitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tab_task_scope
-- ----------------------------
DROP TABLE IF EXISTS `tab_task_scope`;
CREATE TABLE `tab_task_scope`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `dept_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `task_id` bigint(20) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `unit_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tab_unit
-- ----------------------------
DROP TABLE IF EXISTS `tab_unit`;
CREATE TABLE `tab_unit`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `business_super_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `genus` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `level_super_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_1ig70t9tgncchwmh10fo39fg7`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tab_unit
-- ----------------------------
INSERT INTO `tab_unit` VALUES (1, NULL, NULL, 0, NULL, 1, '0', '0001', '001', NULL, '0', '政法委', NULL);
INSERT INTO `tab_unit` VALUES (2, NULL, NULL, 0, NULL, 2, '0', '0002', '002', NULL, '0001', '公安局', NULL);
INSERT INTO `tab_unit` VALUES (3, NULL, NULL, 0, NULL, 3, '0', '0003', '002', NULL, '0001', '检察院', NULL);
INSERT INTO `tab_unit` VALUES (4, NULL, NULL, 0, NULL, 4, '0', '0004', '002', NULL, '0001', '法院', NULL);
INSERT INTO `tab_unit` VALUES (5, NULL, NULL, 0, NULL, 5, '0', '0005', '002', NULL, '0001', '司', NULL);
INSERT INTO `tab_unit` VALUES (6, NULL, NULL, 0, NULL, 6, '0001', '0006', '003', NULL, '0001', '乡镇场', NULL);

-- ----------------------------
-- Table structure for tab_unit_role
-- ----------------------------
DROP TABLE IF EXISTS `tab_unit_role`;
CREATE TABLE `tab_unit_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `super_role_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `unit_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tab_unit_role
-- ----------------------------
INSERT INTO `tab_unit_role` VALUES (1, NULL, NULL, 0, NULL, 1, '超级管理员', '0010', '0', '0001');
INSERT INTO `tab_unit_role` VALUES (2, NULL, NULL, 0, NULL, 2, '一级审核员', '0020', '0010', '0001');
INSERT INTO `tab_unit_role` VALUES (3, NULL, NULL, 0, NULL, 3, '超级管理员', '0010', '0', '0002');
INSERT INTO `tab_unit_role` VALUES (4, NULL, NULL, 0, NULL, 4, '一级审核员', '0020', '0010', '0002');
INSERT INTO `tab_unit_role` VALUES (5, NULL, NULL, 0, NULL, 5, '二级审核员', '0030', '0020', '0002');
INSERT INTO `tab_unit_role` VALUES (6, NULL, NULL, 0, NULL, 6, '普通政法干警', '0040', '0030', '0002');
INSERT INTO `tab_unit_role` VALUES (7, NULL, NULL, 0, NULL, 7, '超级管理员', '0010', '0', '0003');
INSERT INTO `tab_unit_role` VALUES (8, NULL, NULL, 0, NULL, 8, '一级审核员', '0020', '0010', '0003');
INSERT INTO `tab_unit_role` VALUES (9, NULL, NULL, 0, NULL, 9, '二级审核员', '0030', '0020', '0003');
INSERT INTO `tab_unit_role` VALUES (10, NULL, NULL, 0, NULL, 10, '普通政法干警', '0040', '0030', '0003');
INSERT INTO `tab_unit_role` VALUES (11, NULL, NULL, 0, NULL, 11, '超级管理员', '0010', '0', '0004');
INSERT INTO `tab_unit_role` VALUES (12, NULL, NULL, 0, NULL, 12, '一级审核员', '0020', '0010', '0004');
INSERT INTO `tab_unit_role` VALUES (13, NULL, NULL, 0, NULL, 13, '二级审核员', '0030', '0020', '0004');
INSERT INTO `tab_unit_role` VALUES (14, NULL, NULL, 0, NULL, 14, '普通政法干警', '0040', '0030', '0004');
INSERT INTO `tab_unit_role` VALUES (15, NULL, NULL, 0, NULL, 15, '超级管理员', '0010', '0', '0005');
INSERT INTO `tab_unit_role` VALUES (16, NULL, NULL, 0, NULL, 16, '一级审核员', '0020', '0010', '0005');
INSERT INTO `tab_unit_role` VALUES (17, NULL, NULL, 0, NULL, 17, '二级审核员', '0030', '0020', '0005');
INSERT INTO `tab_unit_role` VALUES (18, NULL, NULL, 0, NULL, 18, '普通政法干警', '0040', '0030', '0005');
INSERT INTO `tab_unit_role` VALUES (19, NULL, NULL, 0, NULL, 19, '普通政法干警', '0040', '0', '0006');

-- ----------------------------
-- Table structure for tab_user_account
-- ----------------------------
DROP TABLE IF EXISTS `tab_user_account`;
CREATE TABLE `tab_user_account`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `head_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `usable` bit(1) NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_ryspa2swyk4r387ti3nfglp7o`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tab_user_account
-- ----------------------------
INSERT INTO `tab_user_account` VALUES (1, '2019-06-17 21:57:21', NULL, 0, '2019-06-17 21:57:21', 0, '', '干警1560779840891', 'E10ADC3949BA59ABBE56E057F20F883E', b'1', 'db7fe81e1473419bb8040420fff30194');

-- ----------------------------
-- Table structure for tab_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `tab_user_auth`;
CREATE TABLE `tab_user_auth`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `allot_notice_release_auth` bit(1) NULL DEFAULT NULL,
  `allot_task_release_auth` bit(1) NULL DEFAULT NULL,
  `modify_role_auth` bit(1) NULL DEFAULT NULL,
  `notice_release_auth` bit(1) NULL DEFAULT NULL,
  `task_release_auth` bit(1) NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_1r2hw83yyuwti6uvt7efbydll`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tab_user_auth
-- ----------------------------
INSERT INTO `tab_user_auth` VALUES (1, '2019-06-17 21:57:21', NULL, 0, '2019-06-17 21:57:21', 0, b'1', b'1', b'1', b'1', b'1', 'db7fe81e1473419bb8040420fff30194');

-- ----------------------------
-- Table structure for tab_user_info
-- ----------------------------
DROP TABLE IF EXISTS `tab_user_info`;
CREATE TABLE `tab_user_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birth_date` date NULL DEFAULT NULL,
  `certification` bit(1) NULL DEFAULT NULL,
  `complete_info` bit(1) NULL DEFAULT NULL,
  `dept_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dept_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` int(11) NULL DEFAULT NULL,
  `id_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `unit_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `unit_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_m6w7w19emjm9ur05m8g0sqwtv`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tab_user_info
-- ----------------------------
INSERT INTO `tab_user_info` VALUES (1, '2019-06-17 21:57:21', NULL, 0, '2019-06-17 21:57:21', 0, '广东省广州市', '2000-06-09', b'1', b'0', '1', '财务部', 0, '513436200006099716', 'yh', '18291876713', '财务员', '', '超级管理员', '0010', '0001', '政法委', 'db7fe81e1473419bb8040420fff30194');

-- ----------------------------
-- Table structure for tab_user_score
-- ----------------------------
DROP TABLE IF EXISTS `tab_user_score`;
CREATE TABLE `tab_user_score`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `total_score` int(11) NULL DEFAULT NULL,
  `usable_score` int(11) NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `year` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tab_user_score
-- ----------------------------
INSERT INTO `tab_user_score` VALUES (1, '2019-06-17 21:57:21', NULL, 0, '2019-06-17 21:57:21', 0, 0, 0, 'db7fe81e1473419bb8040420fff30194', 2019);
INSERT INTO `tab_user_score` VALUES (2, '2019-06-17 21:57:21', NULL, 0, '2019-06-17 21:57:21', 0, 0, 0, 'db7fe81e1473419bb8040420fff30194', 2016);
INSERT INTO `tab_user_score` VALUES (3, '2019-06-17 21:57:21', NULL, 0, '2019-06-17 21:57:21', 0, 0, 0, 'db7fe81e1473419bb8040420fff30194', 2017);
INSERT INTO `tab_user_score` VALUES (4, '2019-06-17 21:57:21', NULL, 0, '2019-06-17 21:57:21', 0, 0, 0, 'db7fe81e1473419bb8040420fff30194', 2018);

-- ----------------------------
-- Table structure for tab_user_score_record
-- ----------------------------
DROP TABLE IF EXISTS `tab_user_score_record`;
CREATE TABLE `tab_user_score_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(11) NULL DEFAULT NULL,
  `modify_time` datetime(0) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `score` int(11) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
