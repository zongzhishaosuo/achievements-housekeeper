package com.anyi.achievements.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yanhu
 * @version 2019/6/9 下午 01:27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserVO extends BaseVO {

    private String userId;

    private String phone;

    private String password;

    private String idNumber;

    private String code;

    private Integer way;

    private String unitCode;

    private String deptCode;

    private String roleCode;

    private Boolean award;

    private String name;

}
