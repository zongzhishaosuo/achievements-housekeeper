package com.anyi.achievements.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/13 下午 09:12
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class NotepadVO extends BaseVO {

    private String userId;

    private List<Long> ids;
}
