package com.anyi.achievements.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/20 下午 09:09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ArchivesVO extends BaseVO {

    private String processor;

    private Boolean processed;

    private Boolean passed;

    private Long archivesId;

    private String userId;

    private String locationMain;

    private String belong;

    private String year;

    private String remark;

    private String comment;

    private List<Long> ids;

}
