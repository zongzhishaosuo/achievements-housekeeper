package com.anyi.achievements.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/16 下午 06:38
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CertificationVO extends BaseVO {

    private String verifier;

    private Integer status;

    private List<Integer> statusList;

    private String userId;

    private String remark;
}
