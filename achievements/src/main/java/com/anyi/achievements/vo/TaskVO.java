package com.anyi.achievements.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/16 下午 02:27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TaskVO extends BaseVO {

    private String userId;

    private String author;

    private List<Long> ids;

    private String status;

}
