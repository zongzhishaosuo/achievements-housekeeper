package com.anyi.achievements.vo;

import lombok.Data;

/**
 * @author yanhu
 * @version 2019/6/16 下午 06:31
 */
@Data
public class UnitVO {

    private String code;

    private String unitCode;

    private String roleCode;

    private Boolean business;
}
