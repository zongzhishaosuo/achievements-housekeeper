package com.anyi.achievements.vo;

import lombok.Data;

/**
 * @author yanhu
 * @version 2019/6/15 下午 09:28
 */
@Data
public class PhoneVO {

    private String phone;

    private String code;
}
