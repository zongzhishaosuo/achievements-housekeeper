package com.anyi.achievements.vo;

import lombok.Data;

/**
 * @author yanhu
 * @version 2019/3/26 上午 09:42
 */
@Data
public abstract class BaseVO {

    private Integer number;

    private Integer size;
}
