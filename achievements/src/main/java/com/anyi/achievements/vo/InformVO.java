package com.anyi.achievements.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author yh
 * @version 2019/7/15 上午 11:33
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class InformVO extends BaseVO {

    private String informer;

    private List<Long> ids;
    
}
