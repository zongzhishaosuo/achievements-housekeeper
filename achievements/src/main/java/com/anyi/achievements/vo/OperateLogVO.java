package com.anyi.achievements.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yh
 * @version 2019/7/15 上午 11:45
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OperateLogVO extends BaseVO {


}
