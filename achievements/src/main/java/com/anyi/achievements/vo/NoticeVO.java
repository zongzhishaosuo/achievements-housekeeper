package com.anyi.achievements.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/9 下午 10:43
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class NoticeVO extends BaseVO {

    private String userId;

    private String author;

    private String startTime;

    private String endTime;

    private List<Long> ids;

    private String status;
}
