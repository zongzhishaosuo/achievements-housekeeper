package com.anyi.achievements.util;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author yanhu
 * @version 2019/6/8 下午 08:59
 */
public class MD5Utils {

    public static String md5(String str) {

        String result = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            // 这句是关键
            md5.update(str.getBytes(Charset.forName("UTF-8")));
            byte[] bytes = md5.digest();
            int i;
            StringBuilder buf = new StringBuilder();

            for (byte b : bytes) {
                i = b;
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            result = buf.toString().toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

}
