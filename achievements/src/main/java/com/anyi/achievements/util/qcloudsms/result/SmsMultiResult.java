package com.anyi.achievements.util.qcloudsms.result;

import com.anyi.achievements.util.httpclient.HttpResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.json.JSONException;

import java.util.ArrayList;

@Data
@EqualsAndHashCode(callSuper = false)
public class SmsMultiResult extends SmsResultBase {

    private ArrayList<Detail> details;

    public SmsMultiResult() {
        this.details = new ArrayList<>();
    }

    @Override
    public SmsMultiResult parseFromHttpResponse(HttpResponse response) throws JSONException {
        return null;
    }

    @Data
    public class Detail {

        private Integer result;
        private String errmsg = "";
        private String mobile = "";
        private String nationcode = "";
        private String sid = "";
        private Integer fee = 0;
    }

}
