package com.anyi.achievements.util;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.Random;

/**
 * @author yanhu
 * @version 2019/5/14 下午 03:01
 */
public class TokenUtils {

    private static final String privateKey = "r44b0Yl16NN765gcn737r6Z1Gx34Be8H6mn499v0H9E76f4J6vT5lr50QkI4IQ04";

    private static String randomStr() {
        return randomStr(32);
    }

    private static String randomStr(int len) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < len; i++) {
            sb.append(randomChar(random));
        }
        return sb.toString();
    }

    private static char randomChar(Random random) {
        int c = random.nextInt(3);
        int a, b;
        switch (c) {
            case 0:
                a = 48;
                b = 57;
                break;
            case 1:
                a = 65;
                b = 90;
                break;
            default:
                a = 97;
                b = 122;
        }
        return (char) (random.nextInt(b - a + 1) + a);
    }
    // 45 - 46 . 95 _

    private static String toBinaryString(String str) {

        StringBuilder sb = new StringBuilder();
        int len = str.length();
        for (int i = 0; i < len; i++) {
            sb.append(char2Binary(str.charAt(i)));
        }
        return sb.toString();
    }

    private static String char2Binary(char c) {

        StringBuilder sb = new StringBuilder();
        int i = (int) c;
        for (int j = 0; j < 32; j++) {
            sb.append(i % 2);
            i /= 2;
        }
        return sb.reverse().toString();
    }

    private static char binary2Char(String str) {

        str = new StringBuilder(str).reverse().toString();
        int len = str.length();
        int num = 0;
        int s;
        for (int j = 0; j < len; j++) {
            if ('1' == str.charAt(j)) {

                if (j == 0) {
                    num++;
                } else {
                    s = 2;
                    for (int k = 0; k < (j - 1); k++) {
                        s *= 2;
                    }
                    num += s;
                }
            }
        }
        return (char) num;
    }


    private static final String defaultCharset = "UTF-8";
    private static final String KEY_AES = "AES";
    private static final String KEY = "123456";

    /**
     * 加密
     *
     * @param data 需要加密的内容
     * @param key  加密密码
     * @return
     */
    public static String encrypt(String data, String key) {
        return doAES(data, key, Cipher.ENCRYPT_MODE);
    }

    /**
     * 解密
     *
     * @param data 待解密内容
     * @param key  解密密钥
     * @return
     */
    public static String decrypt(String data, String key) {
        return doAES(data, key, Cipher.DECRYPT_MODE);
    }

    /**
     * 加解密
     *
     * @param data 待处理数据
     * @param mode 加解密mode
     * @return
     */
    private static String doAES(String data, String key, int mode) {
        try {
//            if (StringUtils.isBlank(data) || StringUtils.isBlank(key)) {
//                return null;
//            }
            //判断是加密还是解密
            boolean encrypt = mode == Cipher.ENCRYPT_MODE;
            byte[] content;
            //true 加密内容 false 解密内容
            if (encrypt) {
                content = data.getBytes(defaultCharset);
            } else {
                content = parseHexStr2Byte(data);
            }
            //1.构造密钥生成器，指定为AES算法,不区分大小写
            KeyGenerator kgen = KeyGenerator.getInstance(KEY_AES);
            //2.根据ecnodeRules规则初始化密钥生成器
            //生成一个128位的随机源,根据传入的字节数组
            kgen.init(128, new SecureRandom(key.getBytes()));
            //3.产生原始对称密钥
            SecretKey secretKey = kgen.generateKey();
            //4.获得原始对称密钥的字节数组
            byte[] enCodeFormat = secretKey.getEncoded();
            //5.根据字节数组生成AES密钥
            SecretKeySpec keySpec = new SecretKeySpec(enCodeFormat, KEY_AES);
            //6.根据指定算法AES自成密码器
            Cipher cipher = Cipher.getInstance(KEY_AES);// 创建密码器
            //7.初始化密码器，第一个参数为加密(Encrypt_mode)或者解密解密(Decrypt_mode)操作，第二个参数为使用的KEY
            cipher.init(mode, keySpec);// 初始化
            byte[] result = cipher.doFinal(content);
            if (encrypt) {
                //将二进制转换成16进制
                return parseByte2HexStr(result);
            } else {
                return new String(result, defaultCharset);
            }
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * 将二进制转换成16进制
     *
     * @param buf
     * @return
     */
    public static String parseByte2HexStr(byte buf[]) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 将16进制转换为二进制
     *
     * @param hexStr
     * @return
     */
    public static byte[] parseHexStr2Byte(String hexStr) {
        if (hexStr.length() < 1) {
            return null;
        }
        byte[] result = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }

    public static String getToken(String content) {

        String token = "";
        if (null != content) {
            int len = content.length();
            if (len > 0 && len < 999) {
                char[] chars = content.toCharArray();
                int size;
                if (len <= 16) {
                    size = 32;
                } else {
                    size = len * 2;
                }
                char[] tokens = new char[size];
                int j = 0;
                Random random = new Random();
                for (int i = 0; i < size; i++) {
                    if (i % 2 == 0 || j >= len) {
                        tokens[i] = randomChar(random);
                    } else {
                        tokens[i] = chars[j];
                        j++;
                    }
                }
                String lenStr;
                if (len < 10) {
                    lenStr = "00" + len;
                } else if (len < 100) {
                    lenStr = "0" + len;
                } else {
                    lenStr = "" + len;
                }
                token = new String(tokens) + lenStr;
            }
        }
        return token;
    }

    public static String getUserId(String token) {

        String userId = "";
        if (null != token && token.length() > 0) {
            int len = token.length();
            int idLen = Integer.parseInt(token.substring(len - 3));
            for (int i = 0; i < len; i++) {

            }
        }
        return userId;
    }

    public static void main(String[] args) {

        System.out.println(getToken("sdsdf"));
        System.out.println(getToken("sdsdf").length());
        System.out.println(new Random().nextInt(10));
    }
}
