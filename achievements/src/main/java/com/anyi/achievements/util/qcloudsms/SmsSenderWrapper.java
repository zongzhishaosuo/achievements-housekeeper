package com.anyi.achievements.util.qcloudsms;

import com.anyi.achievements.util.httpclient.HTTP;
import com.anyi.achievements.util.httpclient.HttpClientSender;
import com.anyi.achievements.util.httpclient.HttpMethod;
import com.anyi.achievements.util.httpclient.HttpRequest;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.util.HashMap;

/**
 * @author yanhu
 * @version 2019/6/18 下午 09:57
 */
@Slf4j
public abstract class SmsSenderWrapper implements SmsSenderBase {

    protected String url;

    @Autowired
    protected HttpClientSender sender;

    @PostConstruct
    abstract void init();

    /**
     * @param smsParam
     * @return
     */
    protected HttpRequest createHttpRequest(SmsParam smsParam) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(HTTP.CONTENT_TYPE, "application/json");

        long random = SmsUtils.getRandom();
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("sdkappid", appId);
        parameters.put("random", random + "");

        String body = createRequestBody(smsParam, random);
        return HttpRequest.builder()
                .method(HttpMethod.POST)
                .url(url)
                .headers(headers)
                .parameters(parameters)
                .body(body)
                .charset(Charset.forName("UTF-8"))
                .build();
    }

    /**
     * @param smsParam
     * @param random
     * @return
     */
    protected String createRequestBody(SmsParam smsParam, long random) {
        long time = SmsUtils.getCurrentTime();
        String[] tels = smsParam.getTels();
        String sig = SmsUtils.calculateSignature(appKey, random, time, tels);

        JSONObject root = new JSONObject()
                .put("ext", "")
                .put("extend", "")
                .put("params", smsParam.getParams())
                .put("sig", sig)
                .put("sign", sign)
                .put("tel", toTel(smsParam.getNationCode(), tels))
                .put("time", time)
                .put("tpl_id", smsParam.getTplId());
        log.info("\n{}", root.toString(4));
        return root.toString();
    }

    /**
     * @param nationCode
     * @param tels
     * @return
     */
    protected abstract Object toTel(String nationCode, String... tels) throws SmsException;
}
