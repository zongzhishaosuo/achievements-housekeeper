package com.anyi.achievements.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Json web token 操作
 *
 * @author yanhu
 * @date 2018/8/31
 */
@Slf4j
public class JwtTokenUtils {

    public static void main(String[] args) {
        String str = "B142F2A121E2F2B816D6D6DE5E4B3519B142F2A121E2F2B816D6D6DE5E4B3519";
        int num = 10000;
        createToken(str);
        long st = System.currentTimeMillis();
        for (int i = 0; i < num; i++) {
            System.out.println(getUserId(createToken(str + i)));
        }
        log.info("{}ms", (System.currentTimeMillis() - st));
    }


    /**
     * token秘钥，请勿泄露，请勿随便修改 backups:B142F2A121E2F2B816D6D6DE5E4B3519
     */
    private static final String SECRET = "B142F2A121E2F2B816D6D6DE5E4B3519";
    /**
     * token 过期时间: 1年
     */
    private static final int calendarField = Calendar.YEAR;

    private static final int calendarInterval = 1;

    /**
     * JWT生成Token.<br/>
     * <p>
     * JWT构成: header, payload, signature
     *
     * @param userId 登录成功人员userId, 参数userId不可传空
     */
    public static String createToken(String userId) {

        Date iatDate = new Date();
        // expire time
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(calendarField, calendarInterval);
        Date expiresDate = nowTime.getTime();

        // header Map
        Map<String, Object> map = new HashMap<>();
        map.put("alg", "HS256");
        map.put("typ", "JWT");

        // build token
        // param backups {iss:Service, aud:APP}
        String token = null; // signature
        try {
            token = JWT.create()
                    .withHeader(map) // header
                    .withClaim("iss", "Service") // payload
                    .withClaim("aud", "kitty")
                    .withClaim("userId", userId)
                    .withIssuedAt(iatDate) // sign time
                    .withExpiresAt(expiresDate) // expire time
                    .sign(Algorithm.HMAC256(SECRET));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return token;
    }

    /**
     * 解密Token
     *
     * @param token
     * @return
     * @throws Exception
     */
    public static Map<String, Claim> verifyToken(String token) {
        DecodedJWT jwt;
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SECRET)).build();
            jwt = verifier.verify(token);
        } catch (Exception e) {
            log.info("无效的token");
            // token 校验失败, 抛出Token验证非法异常
            return null;
        }
        return jwt.getClaims();
    }

    /**
     * 根据 Token 获取 userId
     *
     * @param token
     * @return user_id
     */
    public static String getUserId(String token) {
        Map<String, Claim> claims = verifyToken(token);
        if (null == claims) {
            return null;
        }
        Claim user_id_claim = claims.get("userId");
        if (null == user_id_claim || null == user_id_claim.asString()) {
            // token 校验失败, 抛出Token验证非法异常
            log.info("无效的token: " + token);
            return null;
        }
        return user_id_claim.asString();
    }
}
