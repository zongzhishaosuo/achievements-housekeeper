package com.anyi.achievements.util.qcloudsms;

import lombok.Data;

/**
 * @author yanhu
 * @version 2019/6/18 下午 09:17
 */
@Data
public class SmsParam {

    private String[] tels;

    private String[] params;

    private String tplId;

    private String extend = "";

    private String ext = "";

    private String nationCode = "86";

}
