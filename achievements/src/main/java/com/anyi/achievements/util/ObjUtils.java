package com.anyi.achievements.util;

/**
 * 对象非空验证
 *
 * @author yanhu
 * @version 2019/3/22 下午 04:22
 */
public class ObjUtils {

    /**
     * 值不为null
     *
     * @param objects
     * @return
     */
    public static boolean notNull(Object... objects) {

        if (null == objects || objects.length == 0) {
            return false;
        }

        for (Object object : objects) {
            if (null == object) {
                return false;
            }
        }
        return true;
    }

    /**
     * 非空
     *
     * @param object
     * @return
     */
    public static boolean notEmpty(Object object) {
        if (null == object) {
            return false;
        }
        return !(object instanceof String && "".equals(object));
    }

    /**
     * 值不为空
     *
     * @param objects
     * @return
     */
    public static boolean notEmpty(Object... objects) {

        if (null == objects || objects.length == 0) {
            return false;
        }

        for (Object object : objects) {
            if (null == object) {
                return false;
            }
            if (object instanceof String && "".equals(object)) {
                return false;
            }
        }
        return true;
    }


}
