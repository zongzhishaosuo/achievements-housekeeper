package com.anyi.achievements.util.httpclient;

import lombok.Builder;
import lombok.Data;

import java.nio.charset.Charset;
import java.util.Map;

/**
 * @author yanhu
 * @version 2019/6/14 下午 12:00
 */
@Data
@Builder
public final class HttpRequest {

    public String url;

    public HttpMethod method;

    public String body;

    public Map<String, String> headers;

    public Map<String, String> parameters;

    public int connectTimeout;

    public int requestTimeout;

    public Charset charset;

}
