package com.anyi.achievements.util.httpclient;

import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.UnknownHostException;

/**
 * @author yanhu
 * @version 2019/5/22 下午 12:01
 */
public final class HttpClientFactory {

    /**
     * 最大总并发
     */
    private static final int MAX_TOTAL = 100;
    /**
     * 每路并发
     */
    private static final int MAX_PER_ROUTE = 10;
    /**
     * 从池中获取链接超时时间，单位ms毫秒
     */
    private static final int CONNECTION_REQUEST_TIMEOUT = 5_000;
    /**
     * 建立链接超时时间，单位ms毫秒
     */
    private static final int CONNECTION_TIMEOUT = 30_000;
    /**
     * 读取超时时间，单位ms毫秒
     */
    private static final int SOCKET_TIMEOUT = 30_000;
    /**
     * 重连次数
     */
    private static final int RECONNECTION_NUMBER = 2;

    public static CloseableHttpClient getPoolHttpClient() {
        return getPoolHttpClient(null);
    }

    public static CloseableHttpClient getPoolHttpClient(RequestConfig requestConfig) {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(MAX_TOTAL);
        connectionManager.setDefaultMaxPerRoute(MAX_PER_ROUTE);

        // 请求配置
        if (null == requestConfig) {
            requestConfig = RequestConfig.custom()
                    .setConnectionRequestTimeout(CONNECTION_REQUEST_TIMEOUT)
                    .setConnectTimeout(CONNECTION_TIMEOUT)
                    .setSocketTimeout(SOCKET_TIMEOUT)
                    .build();
        }

        // 重连机制
        HttpRequestRetryHandler httpRequestRetryHandler =
                (IOException exception, int executionCount, HttpContext context) -> {
                    if (executionCount >= RECONNECTION_NUMBER) {
                        // 如果已经重试了2次，就放弃
                        return false;
                    }
                    if (exception instanceof NoHttpResponseException) {
                        // 如果服务器丢掉了连接，那么就重试
                        return true;
                    }
                    if (exception instanceof SSLHandshakeException) {
                        // 不
                        // 要重试SSL握手异常
                        return false;
                    }
                    if (exception instanceof InterruptedIOException) {
                        // 超时
                        return false;
                    }
                    if (exception instanceof UnknownHostException) {
                        // 目标服务器不可达
                        return false;
                    }
                    if (exception instanceof ConnectTimeoutException) {
                        // 连接被拒绝
                        return false;
                    }
                    if (exception instanceof SSLException) {
                        // SSL握手异常
                        return false;
                    }

                    HttpClientContext clientContext = HttpClientContext.adapt(context);
                    HttpRequest request = clientContext.getRequest();
                    // 如果请求是幂等的，就再次尝试
                    if (!(request instanceof HttpEntityEnclosingRequest)) {
                        return true;
                    }
                    return false;
                };

        return HttpClients.custom()
                .setConnectionManager(connectionManager)
                .setDefaultRequestConfig(requestConfig)
                .setRetryHandler(httpRequestRetryHandler).build();
    }

    public static CloseableHttpClient getDefaultHttpClient() {
        return HttpClients.createDefault();
    }
}
