package com.anyi.achievements.util.httpclient;

import lombok.Builder;
import lombok.Data;

import java.util.HashMap;

/**
 * @author yanhu
 * @version 2019/6/14 下午 02:30
 */
@Data
@Builder
public final class HttpResponse {

    public HttpRequest request;

    public int statusCode;

    public String reason;

    public String body;

    public HashMap<String, String> headers;

}