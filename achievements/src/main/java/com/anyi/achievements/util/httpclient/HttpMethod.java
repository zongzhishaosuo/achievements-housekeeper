package com.anyi.achievements.util.httpclient;

/**
 * @author yanhu
 * @version 2019/6/14 下午 12:01
 */
public enum HttpMethod {

    GET, POST, HEAD, PATCH, PUT, DLETE, OPTIONS

}
