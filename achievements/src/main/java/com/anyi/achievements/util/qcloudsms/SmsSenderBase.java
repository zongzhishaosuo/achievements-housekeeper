package com.anyi.achievements.util.qcloudsms;

import com.anyi.achievements.util.qcloudsms.result.SmsResultBase;

/**
 * @author yanhu
 * @version 2019/6/18 下午 09:20
 */
public interface SmsSenderBase {

    String appId = "1400219198";

    String appKey = "3b2e000761360733f4e1e27024c78ee6";

    String sign = "中共安义县委政法委员会";

    String verifyCodeTplId = "353832";

    String informTplId = "354883";

    SmsResultBase send(SmsParam smsParam);

}
