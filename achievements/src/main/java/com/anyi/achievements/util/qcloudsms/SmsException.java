package com.anyi.achievements.util.qcloudsms;

/**
 * @author yanhu
 * @version 2019/6/19 下午 03:49
 */
public class SmsException extends RuntimeException {

    /**
     * @param msg
     */
    public SmsException(String msg) {
        super(msg);
    }

}
