package com.anyi.achievements.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.util.Assert;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import static org.springframework.beans.BeanUtils.getPropertyDescriptors;

/**
 * 属性拷贝
 *
 * @author yanhu
 * @version 2019/3/22 下午 03:49
 */
public class BeanUtils {

    /**
     * 利用两个数组迭代复制
     *
     * @param source
     * @param target
     */
    public static void copyFiledByArray(Object source, Object target) {

        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");

        Field[] sourceFields = source.getClass().getDeclaredFields();
        Field[] targetFields = target.getClass().getDeclaredFields();

        Object value;
        try {
            for (Field field : sourceFields) {
                for (Field field2 : targetFields) {
                    if (field.getName().equals(field2.getName()) && field.getType().equals(field2.getType())) {

                        field.setAccessible(true);
                        value = field.get(source);
                        if (null == value || "" == value) {
                            continue;
                        }
                        field2.setAccessible(true);
                        field2.set(target, value);
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 利用一个数组和一个链表迭代复制
     *
     * @param source
     * @param target
     */
    public static void copyFiledByLink(Object source, Object target) {

        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");

        Field[] sourceFields = source.getClass().getDeclaredFields();
        Field[] targetFields = target.getClass().getDeclaredFields();

        LinkedList<Field> link = new LinkedList<>(Arrays.asList(targetFields));
        Object value;
        Field field2;
        int len;

        try {
            for (Field field : sourceFields) {
                len = link.size();
                for (int i = 0; i < len; i++) {
                    field2 = link.get(i);
                    if (field.getName().equals(field2.getName()) && field.getType().equals(field2.getType())) {

                        field.setAccessible(true);
                        value = field.get(source);
                        if (null == value || "" == value) {
                            continue;
                        }
                        field2.setAccessible(true);
                        field2.set(target, value);
                        link.remove(i);
                        break;
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 利用一个数组和一个HashMap迭代复制
     *
     * @param source
     * @param target
     */
    public static void copyFiledByMap(Object source, Object target) {

        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");

        Field[] sourceFields = source.getClass().getDeclaredFields();
        Field[] targetFields = target.getClass().getDeclaredFields();

        HashMap<String, Field> fieldMap = new HashMap<>(targetFields.length);
        for (Field targetField : targetFields) {
            fieldMap.put(targetField.getName(), targetField);
        }

        Object value;
        Field field2;

        try {
            for (Field field : sourceFields) {
                field2 = fieldMap.get(field.getName());
                if (field.getType().equals(field2.getType())) {

                    field.setAccessible(true);
                    value = field.get(source);
                    if (null == value || "" == value) {
                        continue;
                    }
                    field2.setAccessible(true);
                    field2.set(target, value);
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 从source拷贝到target
     *
     * @param source
     * @param target
     * @throws BeansException
     */
    public static void copyProperties(Object source, Object target) throws BeansException {

        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");

        PropertyDescriptor[] sourcePds = getPropertyDescriptors(source.getClass());

        for (PropertyDescriptor sourcePd : sourcePds) {
            if (sourcePd != null && sourcePd.getWriteMethod() != null && sourcePd.getReadMethod() != null) {
                try {
                    Method readMethod = sourcePd.getReadMethod();
                    if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                        readMethod.setAccessible(true);
                    }

                    Object value = readMethod.invoke(source);

                    // 这里判断以下value是否为空 当然这里也能进行一些特殊要求的处理 例如绑定时格式转换等等
                    if (value != null && value != "") {
                        Method writeMethod = sourcePd.getWriteMethod();
                        if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                            writeMethod.setAccessible(true);
                        }
                        writeMethod.invoke(target, value);
                    }
                } catch (Throwable ex) {
                    throw new FatalBeanException("Could not copy properties from source to target", ex);
                }
            }
        }
    }

}
