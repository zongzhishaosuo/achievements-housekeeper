package com.anyi.achievements.util.qcloudsms.result;

import com.anyi.achievements.util.httpclient.HttpResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yanhu
 * @version 2019/6/14 上午 11:02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SmsSingleResult extends SmsResultBase {

    // 短信计费的条数
    private Integer fee;

    // 本次发送标识 ID，标识一次短信下发记录
    private String sid;

    @Override
    public SmsResultBase parseFromHttpResponse(HttpResponse response) {
        return null;
    }
}
