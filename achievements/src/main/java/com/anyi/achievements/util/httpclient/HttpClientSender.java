package com.anyi.achievements.util.httpclient;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * @author yanhu
 * @version 2019/6/14 上午 11:38
 */
@Slf4j
@Component
public final class HttpClientSender implements HttpClientBase {

    private CloseableHttpClient httpClient;

    private Charset defaultCharset;

    @PostConstruct
    public void init() {
        this.httpClient = HttpClientFactory.getPoolHttpClient();
        this.defaultCharset = Charset.forName("UTF-8");
        log.info("http client pool init.");
    }

    /**
     * fetch
     *
     * @param request
     * @return
     * @throws IOException
     */
    @Override
    public HttpResponse fetch(HttpRequest request) throws IOException {
        HttpUriRequest uriRequest = buildRequest(request);
        CloseableHttpResponse response = httpClient.execute(uriRequest);
        return analysisResult(request, response);
    }

    /**
     * build request
     *
     * @param request
     * @return
     */
    private HttpUriRequest buildRequest(HttpRequest request) {
        RequestBuilder builder = RequestBuilder.create(request.method.name())
                .setUri(request.url);
        setHeader(builder, request);
        setParameter(builder, request);
        setEntityAndCharset(builder, request);
        return builder.build();
    }

    /**
     * set request header
     *
     * @param builder
     * @param request
     */
    private void setHeader(RequestBuilder builder, HttpRequest request) {
        Map<String, String> headers = request.headers;
        if (null != headers) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                builder.setHeader(entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * set request parameters
     *
     * @param builder
     * @param request
     */
    private void setParameter(RequestBuilder builder, HttpRequest request) {
        Map<String, String> parameters = request.parameters;
        if (null != parameters) {
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                builder.addParameter(entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * set entity and charset
     *
     * @param builder
     * @param request
     */
    private void setEntityAndCharset(RequestBuilder builder, HttpRequest request) {
        Charset charset = request.charset;
        if (null == charset) {
            charset = defaultCharset;
            request.setCharset(charset);
        }
        builder.setCharset(charset);
        String body = request.body;
        if (null != body) {
            builder.setEntity(new StringEntity(body, charset));
        }
    }

    /**
     * analytic response result
     *
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    private HttpResponse analysisResult(HttpRequest request, CloseableHttpResponse response) throws IOException {

        Integer statusCode = -1;
        String reason = "";
        String body = "";
        try {
            statusCode = response.getStatusLine().getStatusCode();
            reason = response.getStatusLine().getReasonPhrase();
            body = EntityUtils.toString(response.getEntity(), request.getCharset());
        } catch (IOException e) {
            log.error("解析响应报体异常");
            e.printStackTrace();
        } finally {
            response.close();
        }
        return HttpResponse.builder()
                .request(request)
                .statusCode(statusCode)
                .reason(reason)
                .body(body)
                .build();
    }

    /**
     * close pool
     *
     * @throws IOException
     */
    @PreDestroy
    @Override
    public void close() throws IOException {
        httpClient.close();
        log.info("http client pool close.");
    }

}
