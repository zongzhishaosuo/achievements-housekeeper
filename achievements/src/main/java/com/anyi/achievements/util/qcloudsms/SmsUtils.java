package com.anyi.achievements.util.qcloudsms;


import org.apache.commons.codec.digest.DigestUtils;

import java.util.Random;

/**
 * @author yanhu
 * @version 2019/6/14 上午 10:37
 */
public class SmsUtils {

    /**
     * @return
     */
    public static long getCurrentTime() {
        return System.currentTimeMillis() / 1000;
    }

    /**
     * @return
     */
    public static int getRandom() {
        return (new Random().nextInt(900000) + 100000);
    }

    /**
     * @param appkey
     * @param random
     * @param time
     * @param mobile
     * @return
     */
    public static String calculateSignature(String appkey, long random, long time, String mobile) {
        return calculateSignature(appkey, random, time, new String[]{mobile});
    }

    /**
     * @param appkey
     * @param random
     * @param time
     * @param mobileList
     * @return
     */
    public static String calculateSignature(String appkey, long random, long time, String[] mobileList) {

        StringBuilder buffer = new StringBuilder("appkey=")
                .append(appkey)
                .append("&random=")
                .append(random)
                .append("&time=")
                .append(time)
                .append("&mobile=");

        if (mobileList.length > 0) {
            for (String mobile : mobileList) {
                buffer.append(mobile);
                buffer.append(",");
            }
        }
        int len = buffer.length();
        buffer.delete(len - 1, len);
        return sha256(buffer.toString());
    }

    /**
     * @param str
     * @return
     */
    private static String sha256(String str) {
        return DigestUtils.sha256Hex(str);
    }
}
