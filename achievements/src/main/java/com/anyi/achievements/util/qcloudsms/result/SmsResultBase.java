package com.anyi.achievements.util.qcloudsms.result;

import com.anyi.achievements.util.httpclient.HttpResponse;
import lombok.Data;


/**
 * Sms result base class
 *
 * @since 1.0.0
 */
@Data
public abstract class SmsResultBase {

    protected HttpResponse response;

    // 错误码，0表示成功(计费依据)，非0表示失败
    protected Integer result;

    // 错误消息，result 非0时的具体错误信息
    protected String errmsg;

    // 用户的 session 内容
    protected String ext;

    /**
     * @param response
     * @return
     */
    public abstract SmsResultBase parseFromHttpResponse(HttpResponse response);


    public boolean success() {
        return null != this.result && 0 == this.result;
    }
}
