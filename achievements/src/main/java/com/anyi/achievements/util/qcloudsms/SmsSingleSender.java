package com.anyi.achievements.util.qcloudsms;

import com.alibaba.fastjson.JSON;
import com.anyi.achievements.util.httpclient.HttpRequest;
import com.anyi.achievements.util.httpclient.HttpResponse;
import com.anyi.achievements.util.httpclient.HttpStatus;
import com.anyi.achievements.util.qcloudsms.result.SmsSingleResult;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 单发短信
 *
 * @author yanhu
 * @version 2019/6/14 上午 11:00
 */
@Slf4j
@Component
public class SmsSingleSender extends SmsSenderWrapper {

    @Override
    void init() {
        this.url = "https://yun.tim.qq.com/v5/tlssmssvr/sendsms";
    }

    /**
     * 单发模板短信
     *
     * @param smsParam
     * @return
     */
    public SmsSingleResult send(SmsParam smsParam) {
        HttpRequest request = createHttpRequest(smsParam);
        SmsSingleResult ssr = null;
        try {
            HttpResponse response = sender.fetch(request);
            log.info("【单发模板短信】短信发送结果：{}", response);
            if (response.getStatusCode() == HttpStatus.SC_OK) {
                ssr = JSON.parseObject(response.body, SmsSingleResult.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("【单发模板短信】短信发送失败");
        }
        if (null == ssr) {
            ssr = new SmsSingleResult();
        }
        return ssr;
    }

    /**
     * @param nationCode
     * @param tels
     * @return
     */
    @Override
    protected Object toTel(String nationCode, String... tels) throws SmsException {
        if (null != tels && tels.length == 1) {
            return new JSONObject()
                    .put("nationcode", nationCode)
                    .put("mobile", tels[0]);
        } else {
            throw new SmsException("手机号参数不符规范");
        }
    }
}
