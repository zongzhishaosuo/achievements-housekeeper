package com.anyi.achievements.util.qcloudsms;

import com.alibaba.fastjson.JSON;
import com.anyi.achievements.util.httpclient.HttpRequest;
import com.anyi.achievements.util.httpclient.HttpResponse;
import com.anyi.achievements.util.httpclient.HttpStatus;
import com.anyi.achievements.util.qcloudsms.result.SmsMultiResult;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;


@Slf4j
@Component
public class SmsMultiSender extends SmsSenderWrapper {

    @Override
    void init() {
        this.url = "https://yun.tim.qq.com/v5/tlssmssvr/sendmultisms2";
    }

    /**
     * 群发模板短信
     *
     * @return
     */
    public SmsMultiResult send(SmsParam smsParam) {
        HttpRequest request = createHttpRequest(smsParam);
        SmsMultiResult smr = null;
        try {
            HttpResponse response = sender.fetch(request);
            log.info("【群发模板短信】短信发送结果：{}", response);
            if (response.getStatusCode() == HttpStatus.SC_OK) {
                smr = JSON.parseObject(response.body, SmsMultiResult.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("【群发模板短信】短信发送失败");
        }
        if (null == smr) {
            smr = new SmsMultiResult();
        }
        return smr;
    }

    /**
     * @param nationCode
     * @param tels
     * @return
     */
    @Override
    protected Object toTel(String nationCode, String... tels) throws SmsException {
        if (null != tels && tels.length > 0) {
            JSONArray array = new JSONArray();
            JSONObject object;
            for (String tel : tels) {
                object = new JSONObject()
                        .put("nationcode", nationCode)
                        .put("mobile", tel);
                array.put(object);
            }
            return array;
        } else {
            throw new SmsException("手机号参数不符规范");
        }
    }

}
