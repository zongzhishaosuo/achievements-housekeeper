package com.anyi.achievements.util.httpclient;

import java.io.IOException;

/**
 * @author yanhu
 * @version 2019/6/14 下午 06:38
 */
public interface HttpClientBase {

    HttpResponse fetch(HttpRequest request) throws IOException;

    void close() throws IOException;
}
