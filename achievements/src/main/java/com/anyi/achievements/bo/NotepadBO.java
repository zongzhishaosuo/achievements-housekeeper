package com.anyi.achievements.bo;

import com.anyi.achievements.entity.common.CommonResource;
import com.anyi.achievements.entity.notepad.Notepad;
import lombok.Data;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/13 下午 08:38
 */
@Data
public class NotepadBO {

    private Notepad notepad;

    private List<CommonResource> commonResourceList;
}
