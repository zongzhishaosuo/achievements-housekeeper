package com.anyi.achievements.bo;

import com.anyi.achievements.entity.archives.ArchivesProcess;
import com.anyi.achievements.entity.archives.ArchivesProcessRecord;
import com.anyi.achievements.entity.archives.ArchivesUser;
import com.anyi.achievements.entity.archives.PerformanceArchives;
import com.anyi.achievements.entity.common.CommonResource;
import com.anyi.achievements.entity.task.Task;
import com.anyi.achievements.entity.user.UserInfo;
import lombok.Data;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/29 下午 02:33
 */
@Data
public class PerformanceArchivesBO {

    private PerformanceArchives performanceArchives;

    private ArchivesProcess archivesProcess;

    private List<ArchivesProcess> archivesProcessList;

    private List<ArchivesProcessRecord> archivesProcessRecordList;

    private List<CommonResource> commonResourceList;

    private Task task;

    private List<ArchivesUser> archivesUserList;

    private List<UserInfo> userInfoList;

    private List<UserBO> userBOList;

    private List<ArchivesProcessBO> archivesProcessBOList;

}

