package com.anyi.achievements.bo;

import com.anyi.achievements.entity.task.Task;
import com.anyi.achievements.entity.task.TaskScope;
import com.anyi.achievements.entity.user.UserInfo;
import lombok.Data;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/16 下午 12:57
 */
@Data
public class TaskBO {

    private Task task;

    private List<TaskScope> taskScopeList;

    private UserInfo author;
}
