package com.anyi.achievements.bo.constant;

/**
 * @author yanhu
 * @version 2019/6/8 下午 11:59
 */
public class UserConst {

    public static final int LOGIN_WAY_PHONE_PASSWORD = 0;

    public static final int LOGIN_WAY_PHONE_CODE = 1;


    /**
     * 超级管理员编号
     */
    public static final String ROLE_CODE_001_SUPER_ADMIN = "0010";

    /**
     * 一级审核员
     */
    public static final String ROLE_CODE_002_FIRST_LEVEL_VERIFIER = "0020";

    /**
     * 二级审核员
     */
    public static final String ROLE_CODE_003_SECOND_LEVEL_VERIFIER = "0030";

    /**
     * 普通政法干警
     */
    public static final String ROLE_CODE_004_COMMON_POLICEMEN = "0040";


    public static final int SCORE_START_YEAR = 2016;
}
