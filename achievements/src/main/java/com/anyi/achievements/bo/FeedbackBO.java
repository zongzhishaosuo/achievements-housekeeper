package com.anyi.achievements.bo;

import com.anyi.achievements.entity.common.CommonResource;
import com.anyi.achievements.entity.feedback.Feedback;
import com.anyi.achievements.entity.user.UserInfo;
import lombok.Data;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/7/16 下午 10:55
 */
@Data
public class FeedbackBO {

    private Feedback feedback;

    private UserInfo userInfo;

    private List<CommonResource> commonResourceList;

}
