package com.anyi.achievements.bo.constant;

/**
 * @author yanhu
 * @version 2019/6/16 下午 01:56
 */
public class TaskConst {

    public static final int SCOPE_TYPE_10_UNIT = 10;

    public static final int SCOPE_TYPE_20_DEPT = 20;

    public static final int SCOPE_TYPE_30_USER = 30;

}
