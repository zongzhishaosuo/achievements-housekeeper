package com.anyi.achievements.bo;

import com.anyi.achievements.entity.score.UserScore;
import lombok.Data;

/**
 * @author yanhu
 * @version 2019/6/12 下午 10:10
 */
@Data
public class UserScoreBO {

    // 所有排名
    private Integer allRanking;

    // 单位内排名
    private Integer unitRanking;

    // 部门内排名
    private Integer deptRanking;

    private UserScore userScore;

}
