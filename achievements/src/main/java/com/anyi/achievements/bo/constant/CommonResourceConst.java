package com.anyi.achievements.bo.constant;

/**
 * 附件资源静态变量
 *
 * @author yanhu
 * @version 2019/6/9 下午 01:39
 */
public class CommonResourceConst {

    /**
     * 公告类型资源
     */
    public static final int COMMON_CLASS_10_NOTICE = 10;

    /**
     * 记事本类型资源
     */
    public static final int COMMON_CLASS_20_NOTEPAD = 20;

    public static final int COMMON_CLASS_30_TASK = 30;

    /**
     * 个人档案
     */
    public static final int COMMON_CLASS_40_PERSONAL_ARCHIVES = 40;
    /**
     * 政绩档案
     */
    public static final int COMMON_CLASS_50_PERFORMANCE_ARCHIVES = 50;


    public static final int TYPE_10_IMAGE = 10;

    public static final int TYPE_20_AUDIO = 20;

    public static final int TYPE_30_VIDEO = 30;

    public static final int TYPE_40_APPLICATION = 40;

    public static final int TYPE_50_UNDERFINED = 50;
}
