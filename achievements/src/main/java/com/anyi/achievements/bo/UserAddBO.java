package com.anyi.achievements.bo;

import com.anyi.achievements.dto.DTO;
import lombok.Data;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/7/30 下午 10:33
 */
@Data
public class UserAddBO {

    private List<DTO> okUserBOList;

    private List<DTO> failUserBOList;

}
