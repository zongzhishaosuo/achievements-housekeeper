package com.anyi.achievements.bo.constant;

/**
 * @author yanhu
 * @version 2019/6/9 下午 01:55
 */
public class NoticeConst {

    public static final int SCOPE_TYPE_10_UNIT = 10;

    public static final int SCOPE_TYPE_20_DEPT = 20;

    public static final int SCOPE_TYPE_30_USER = 30;

}
