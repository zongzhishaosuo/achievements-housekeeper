package com.anyi.achievements.bo;

import com.anyi.achievements.entity.score.UserScore;
import com.anyi.achievements.entity.user.UserAccount;
import com.anyi.achievements.entity.user.UserAuth;
import com.anyi.achievements.entity.user.UserInfo;
import lombok.Data;

/**
 * @author yanhu
 * @version 2019/6/8 下午 06:00
 */
@Data
public class UserBO {

    private UserInfo userInfo;

    private UserAuth userAuth;

    private UserAccount userAccount;

    private UserScore userScore;
}
