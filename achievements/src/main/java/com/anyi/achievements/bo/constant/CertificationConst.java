package com.anyi.achievements.bo.constant;

/**
 * @author yanhu
 * @version 2019/6/16 下午 04:15
 */
public class CertificationConst {

    public static final int STATUS_0_NOT_SUBMITTED = 0;

    public static final int STATUS_10_UNTREATED = 10;

    public static final int STATUS_20_PASS = 20;

    public static final int STATUS_30_NOT_PASS = 30;

}
