package com.anyi.achievements.bo;

import com.anyi.achievements.entity.unit.Dept;
import com.anyi.achievements.entity.unit.Unit;
import com.anyi.achievements.entity.unit.UnitRole;
import lombok.Data;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/11 下午 07:54
 */
@Data
public class UnitBO {

    private Unit unit;

    private List<Dept> deptList;

    private List<UnitRole> unitRoleList;

    private String superUnitCode;

    private String superRoleCode;
}
