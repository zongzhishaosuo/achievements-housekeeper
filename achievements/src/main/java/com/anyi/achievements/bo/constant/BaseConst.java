package com.anyi.achievements.bo.constant;

/**
 * 基础常量
 *
 * @author yanhu
 * @version 2019/6/11 下午 08:00
 */
public class BaseConst {

    /**
     * 正常记录
     */
    public static final int DELETED_0_NORMAL = 0;

    /**
     * 已删除记录
     */
    public static final int DELETED_1_DELETED = 1;

    public static final String SUCCESS = "success";

    public static final String FAIL = "fail";

    public static final String ERROR = "error";

    public static final String NONE = "0";
}
