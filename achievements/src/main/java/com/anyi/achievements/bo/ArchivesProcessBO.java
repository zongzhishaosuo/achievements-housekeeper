package com.anyi.achievements.bo;

import com.anyi.achievements.entity.archives.ArchivesProcess;
import com.anyi.achievements.entity.user.UserAccount;
import com.anyi.achievements.entity.user.UserInfo;
import lombok.Data;

/**
 * @author yanhu
 * @version 2019/7/27 上午 11:38
 */
@Data
public class ArchivesProcessBO {

    private ArchivesProcess archivesProcess;

    private UserInfo userInfo;

    private UserAccount userAccount;

}
