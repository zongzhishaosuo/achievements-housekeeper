package com.anyi.achievements.bo.em;

/**
 * @author yanhu
 * @version 2019/6/16 下午 04:51
 */
public enum UnitEnum {



    ;



    private String level;

    private String name;

    UnitEnum(String level, String name) {
        this.level = level;
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }
}
