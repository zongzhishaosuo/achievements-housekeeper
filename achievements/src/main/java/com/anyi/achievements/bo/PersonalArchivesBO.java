package com.anyi.achievements.bo;

import com.anyi.achievements.entity.archives.personal.PersonalArchives;
import com.anyi.achievements.entity.archives.personal.PersonalArchivesProcess;
import com.anyi.achievements.entity.common.CommonResource;
import com.anyi.achievements.entity.user.UserInfo;
import lombok.Data;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/19 下午 09:53
 */
@Data
public class PersonalArchivesBO {

    private UserInfo userInfo;

    private PersonalArchives personalArchives;

    private List<PersonalArchivesProcess> personalArchivesProcessList;

    private List<CommonResource> commonResourceList;

}
