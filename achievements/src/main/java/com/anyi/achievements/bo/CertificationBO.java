package com.anyi.achievements.bo;

import com.anyi.achievements.entity.certification.Certification;
import com.anyi.achievements.entity.certification.CertificationRecord;
import com.anyi.achievements.entity.user.UserInfo;
import lombok.Data;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/16 下午 04:30
 */
@Data
public class CertificationBO {

    private UserInfo userInfo;

    private Certification certification;

    private CertificationRecord certificationRecord;

    private List<CertificationRecord> certificationRecordList;

}
