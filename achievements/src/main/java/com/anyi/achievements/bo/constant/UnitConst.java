package com.anyi.achievements.bo.constant;

/**
 * @author yanhu
 * @version 2019/6/16 下午 05:04
 */
public class UnitConst {

    public static final String GENUS_001_ZFW = "001";

    public static final String GENUS_002_GJSF = "002";

    public static final String GENUS_003_XZC = "003";

    public static final String NO_SUPER_CODE = "0";
}
