package com.anyi.achievements.bo;

import com.anyi.achievements.entity.common.CommonResource;
import com.anyi.achievements.entity.notice.Notice;
import com.anyi.achievements.entity.notice.NoticeScope;
import com.anyi.achievements.entity.user.UserInfo;
import lombok.Data;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/9 下午 01:37
 */
@Data
public class NoticeBO {

    private Notice notice;

    private List<CommonResource> commonResourceList;

    private List<NoticeScope> noticeScopeList;

    private UserInfo author;
}
