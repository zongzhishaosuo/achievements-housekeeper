package com.anyi.achievements.repository.unit;

import com.anyi.achievements.entity.unit.UnitRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/16 下午 05:18
 */
public interface UnitRoleRepository extends JpaRepository<UnitRole, Long> {

    List<UnitRole> findByUnitCode(String unitCode);

    UnitRole findByCode(String code);
}
