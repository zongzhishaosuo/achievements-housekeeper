package com.anyi.achievements.repository.user;

import com.anyi.achievements.entity.user.UserInform;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author yh
 * @version 2019/7/11 上午 11:44
 */
public interface UserInformRepository extends JpaRepository<UserInform, Long>, JpaSpecificationExecutor<UserInform> {


}
