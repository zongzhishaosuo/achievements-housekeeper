package com.anyi.achievements.repository.user;

import com.anyi.achievements.entity.user.UserAuth;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yanhu
 * @version 2019/6/8 下午 05:54
 */
public interface UserAuthRepository extends JpaRepository<UserAuth,Long> {

    UserAuth findByUserId(String userId);

}
