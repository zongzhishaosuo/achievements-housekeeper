package com.anyi.achievements.repository.score;

import com.anyi.achievements.entity.score.UserScoreRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author yanhu
 * @version 2019/6/8 下午 05:54
 */
public interface UserScoreRecordRepository extends JpaRepository<UserScoreRecord, Long>, JpaSpecificationExecutor<UserScoreRecord> {

}
