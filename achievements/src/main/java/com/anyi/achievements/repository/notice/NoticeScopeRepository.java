package com.anyi.achievements.repository.notice;

import com.anyi.achievements.entity.notice.NoticeScope;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/9 下午 07:42
 */
public interface NoticeScopeRepository extends JpaRepository<NoticeScope, Long> {

    void deleteByNoticeId(Long noticeId);

    List<NoticeScope> findByNoticeId(Long noticeId);
}
