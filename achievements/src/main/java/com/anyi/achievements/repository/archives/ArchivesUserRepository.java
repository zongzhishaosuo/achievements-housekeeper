package com.anyi.achievements.repository.archives;


import com.anyi.achievements.entity.archives.ArchivesUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/7/16 下午 09:34
 */
public interface ArchivesUserRepository extends JpaRepository<ArchivesUser, Long> {

    List<ArchivesUser> findByArchivesIdAndArchivesType(Long archivesId, Integer archivesType);

    void deleteByArchivesIdAndArchivesType(Long archivesId, Integer archivesType);

}
