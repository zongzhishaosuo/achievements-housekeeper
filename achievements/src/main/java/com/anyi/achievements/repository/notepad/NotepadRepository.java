package com.anyi.achievements.repository.notepad;

import com.anyi.achievements.entity.notepad.Notepad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author yanhu
 * @version 2019/6/13 下午 08:31
 */
public interface NotepadRepository extends JpaRepository<Notepad, Long>, JpaSpecificationExecutor<Notepad> {

}
