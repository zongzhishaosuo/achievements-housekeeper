package com.anyi.achievements.repository.user;

import com.anyi.achievements.entity.user.UserExperience;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yh
 * @version 2019/7/11 上午 11:30
 */
public interface UserExperienceRepository extends JpaRepository<UserExperience, Long> {

    UserExperience findByUserId(String userId);

}
