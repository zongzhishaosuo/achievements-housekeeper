package com.anyi.achievements.repository.task;

import com.anyi.achievements.entity.task.TaskScope;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/10 下午 11:19
 */
public interface TaskScopeRepository extends JpaRepository<TaskScope, Long> {

    void deleteByTaskId(Long taskId);

    List<TaskScope> findByTaskId(Long taskId);
}
