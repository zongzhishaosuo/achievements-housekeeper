package com.anyi.achievements.repository.archives.personal;

import com.anyi.achievements.entity.archives.personal.PersonalArchives;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/19 下午 09:43
 */
public interface PersonalArchivesRepository extends JpaRepository<PersonalArchives, Long>, JpaSpecificationExecutor<PersonalArchives> {

    List<PersonalArchives> findByUserIdAndDeleted(String userId, Integer deleted);

}
