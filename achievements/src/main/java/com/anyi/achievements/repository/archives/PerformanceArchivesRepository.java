package com.anyi.achievements.repository.archives;

import com.anyi.achievements.entity.archives.PerformanceArchives;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/29 下午 02:29
 */
public interface PerformanceArchivesRepository extends JpaRepository<PerformanceArchives, Long>, JpaSpecificationExecutor<PerformanceArchives> {

    @Query(value = "select distinct a.* from tab_performance_archives a " +
            " inner join tab_archives_process b on a.id = b.archives_id " +
            " where processor = ?1 " +
            " and processed = ?2 " +
            " and location_main like ?3 " +
            " and belong like ?4 " +
            " and 'year' like ?5 order by modify_time desc limit ?6,?7", nativeQuery = true)
    List<PerformanceArchives> processPage(String processor, Boolean processed, String locationMain,
                                          String belong, String year, Integer number, Integer size);

    @Query(value = "select count(1) from tab_performance_archives a " +
            " inner join tab_archives_process b on a.id = b.archives_id " +
            " where processor = ?1 " +
            " and processed = ?2 " +
            " and location_main like ?3 " +
            " and belong like ?4 " +
            " and 'year' like ?5 ", nativeQuery = true)
    Long processCount(String processor, Boolean processed, String locationMain,
                      String belong, String year);

    @Query(value = "select distinct a.* from tab_performance_archives a " +
            " inner join tab_archives_user b on a.id = b.archives_id " +
            " where user_id = ?1 " +
            " and location_main like ?2 " +
            " and belong like ?3 " +
            " and 'year' like ?4 " +
            " order by modify_time desc " +
            " limit ?5,?6", nativeQuery = true)
    List<PerformanceArchives> ownPage(String userId, String locationMain, String belong,
                                      String year, Integer number, Integer size);

    @Query(value = "select count(1) from tab_performance_archives a " +
            " inner join tab_archives_user b on a.id = b.archives_id " +
            " where user_id = ?1 " +
            " and location_main like ?2 " +
            " and belong like ?3 " +
            " and 'year' like ?4 ", nativeQuery = true)
    Long ownCount(String userId, String locationMain, String belong, String year);

}
