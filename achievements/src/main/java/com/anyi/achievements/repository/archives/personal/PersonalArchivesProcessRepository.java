package com.anyi.achievements.repository.archives.personal;

import com.anyi.achievements.entity.archives.personal.PersonalArchivesProcess;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/19 下午 09:44
 */
public interface PersonalArchivesProcessRepository extends JpaRepository<PersonalArchivesProcess, Long> {

    List<PersonalArchivesProcess> findByArchivesId(Long archivesId);

}
