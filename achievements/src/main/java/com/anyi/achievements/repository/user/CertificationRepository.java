package com.anyi.achievements.repository.user;

import com.anyi.achievements.entity.certification.Certification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author yanhu
 * @version 2019/6/13 下午 08:29
 */
public interface CertificationRepository extends JpaRepository<Certification, Long>, JpaSpecificationExecutor<Certification> {

    Certification findByUserId(String userId);

}
