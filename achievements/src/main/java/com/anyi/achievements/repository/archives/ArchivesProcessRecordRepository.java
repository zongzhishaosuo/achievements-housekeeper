package com.anyi.achievements.repository.archives;

import com.anyi.achievements.entity.archives.ArchivesProcessRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/29 下午 02:30
 */
public interface ArchivesProcessRecordRepository extends JpaRepository<ArchivesProcessRecord, Long> {

    List<ArchivesProcessRecord> findByArchivesIdOrderByTimestamp(Long archivesId);

}
