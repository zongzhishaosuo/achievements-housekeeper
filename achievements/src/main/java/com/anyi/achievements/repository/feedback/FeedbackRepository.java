package com.anyi.achievements.repository.feedback;

import com.anyi.achievements.entity.feedback.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yh
 * @version 2019/7/15 上午 11:57
 */
public interface FeedbackRepository extends JpaRepository<Feedback, Long> {
    
}
