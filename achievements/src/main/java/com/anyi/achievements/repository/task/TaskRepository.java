package com.anyi.achievements.repository.task;

import com.anyi.achievements.entity.task.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/10 下午 11:18
 */
public interface TaskRepository extends JpaRepository<Task, Long>, JpaSpecificationExecutor<Task> {

    /**
     * 获取可见的任务列表
     *
     * @param userId
     * @param unitCode
     * @param number
     * @param size
     * @return
     */
    @Query(value = "SELECT * from tab_task " +
            " where deleted = 0 " +
            " and revocation = false " +
            " and status like ?6 " +
            " and id in ( " +
            "   select distinct task_id from tab_task_scope " +
            "   where user_id = ?1 or dept_code = ?2 or unit_code = ?3 " +
            " ) order by sort desc, id desc limit ?4,?5 ", nativeQuery = true)
    List<Task> getVisibleScopePage(String userId, String deptCode, String unitCode,
                                   Integer number, Integer size, String status);

    /**
     * 可见任务总数
     *
     * @param userId
     * @param unitCode
     * @return
     */
    @Query(value = "SELECT count(1) from tab_task " +
            " where deleted = 0 " +
            " and revocation = false " +
            " and status like ?4 " +
            " and id in ( " +
            "   select distinct task_id from tab_task_scope " +
            "   where user_id = ?1 or dept_code = ?2 or unit_code = ?3 " +
            " ) ", nativeQuery = true)
    Long getVisibleScopeCount(String userId, String deptCode, String unitCode, String status);

    /**
     * 获取可见的任务列表
     *
     * @param userId
     * @param unitCode
     * @return
     */
    @Query(value = "SELECT * from tab_task " +
            " where deleted = 0 " +
            " and revocation = false " +
            " and status like ?4 " +
            " and id in ( " +
            "   select distinct task_id from tab_task_scope " +
            "   where user_id = ?1 or dept_code = ?2 or unit_code = ?3 " +
            " ) order by sort desc, id desc ", nativeQuery = true)
    List<Task> getVisibleScopeList(String userId, String deptCode, String unitCode, String status);

    @Modifying
    @Transactional
    @Query(value = "UPDATE tab_task set status = 20 where start_time < ?1 and end_time > ?1", nativeQuery = true)
    void setStart(LocalDateTime time);

    @Modifying
    @Transactional
    @Query(value = "UPDATE tab_task set status = 30 where end_time < ?1", nativeQuery = true)
    void setEnd(LocalDateTime time);

}
