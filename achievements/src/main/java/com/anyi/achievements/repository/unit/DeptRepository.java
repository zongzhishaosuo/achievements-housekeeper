package com.anyi.achievements.repository.unit;

import com.anyi.achievements.entity.unit.Dept;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/10 下午 10:02
 */
public interface DeptRepository extends JpaRepository<Dept, Long> {

    List<Dept> findByUnitCodeOrderBySort(String unitCode);

}
