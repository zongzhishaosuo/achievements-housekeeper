package com.anyi.achievements.repository.inform;

import com.anyi.achievements.entity.inform.Inform;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author yh
 * @version 2019/7/15 上午 10:57
 */
public interface InformRepository extends JpaRepository<Inform, Long>, JpaSpecificationExecutor<Inform> {

}
