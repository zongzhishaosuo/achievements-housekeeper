package com.anyi.achievements.repository.unit;

import com.anyi.achievements.entity.unit.Unit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/8 下午 06:08
 */
public interface UnitRepository extends JpaRepository<Unit, Long> {

    /**
     * 所有未被删除的单位记录
     *
     * @param deleted
     * @return
     */
    List<Unit> findByDeletedOrderBySort(Integer deleted);

    Unit findByCodeAndDeleted(String code, Integer deleted);
}
