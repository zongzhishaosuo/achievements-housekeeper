package com.anyi.achievements.repository.log;

import com.anyi.achievements.entity.log.OperateLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yh
 * @version 2019/7/15 上午 10:48
 */
public interface OperateLogRepository extends JpaRepository<OperateLog, Long> {

}