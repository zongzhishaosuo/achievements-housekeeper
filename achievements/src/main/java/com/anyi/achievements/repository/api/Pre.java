package com.anyi.achievements.repository.api;

import lombok.Data;

/**
 * @author yanhu
 * @date 2018/8/9
 */
@Data
public class Pre {

    private OP operator;
    private String name;
    private Object value;

    private Pre(OP operator, String name, Object value) {
        this.operator = operator;
        this.name = name;
        this.value = value;
    }

    public static Pre get(OP operator, String name, Object value) {
        return new Pre(operator, name, value);
    }

    public static Pre of(OP operator, String name, Object value) {
        if (null == value || "".equals(value))
            return null;
        return new Pre(operator, name, value);
    }
}
