package com.anyi.achievements.repository.score;

import com.anyi.achievements.entity.score.UserScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author yanhu
 * @version 2019/6/8 下午 05:55
 */
public interface UserScoreRepository extends JpaRepository<UserScore, Long> {

    UserScore findByUserIdAndYear(String userId, Integer year);

    @Query(value = "SELECT " +
            "  t.rn  " +
            "FROM " +
            "  ( " +
            "  SELECT " +
            "    user_id, " +
            "    total_score, " +
            "    ( SELECT @rn \\:= @rn + 1 FROM ( SELECT @rn \\:= 0 ) r ) AS rn  " +
            "  FROM " +
            "    ( SELECT user_id, sum( total_score ) AS total_score FROM tab_user_score GROUP BY user_id ) t1  " +
            "  WHERE " +
            "    user_id IN ( SELECT user_id FROM tab_user_info WHERE unit_code LIKE ?1 AND dept_code LIKE ?2 )  " +
            "  ORDER BY " +
            "    total_score  " +
            "  ) t  " +
            "WHERE " +
            "  t.user_id = ?3 ", nativeQuery = true)
    Integer getAllRanking(String unitCode, String deptCode, String userId);

    @Query(value = "SELECT " +
            "  t.rn  " +
            " FROM " +
            "  ( " +
            "  SELECT " +
            "    user_id, " +
            "    ( SELECT @rn \\:= @rn + 1 FROM ( SELECT @rn \\:= 0 ) r ) AS rn  " +
            "  FROM " +
            "    tab_user_score  " +
            "  WHERE " +
            "    year = ?1  " +
            "    and user_id in (select user_id from tab_user_info " +
            "      where " +
            "      unit_code like ?1 " +
            "      and dept_code like ?2 " +
            "    ) " +
            "  ORDER BY " +
            "    total_score  " +
            "  ) t  " +
            " WHERE " +
            "  t.user_id = ?4 ", nativeQuery = true)
    Integer getThisYearRanking(Integer year, String unitCode, String deptCode, String userId);
}
