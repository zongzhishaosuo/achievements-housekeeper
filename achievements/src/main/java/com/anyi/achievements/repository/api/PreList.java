package com.anyi.achievements.repository.api;


import java.util.ArrayList;
import java.util.List;

/**
 * @author yanhu
 * @version 2018/12/28 10:23
 */
public class PreList {

    private List<Pre> ps;

    private PreList() {
        this.ps = new ArrayList<>();
    }

    public static Bui bui() {
        return new Bui();
    }

    public static class Bui {

        private PreList pl;

        private Bui() {
            pl = new PreList();
        }

        public Bui add(Pre pre) {
            if (null != pre)
                this.pl.ps.add(pre);
            return this;
        }

        public List<Pre> get() {
            return this.pl.ps;
        }
    }
}
