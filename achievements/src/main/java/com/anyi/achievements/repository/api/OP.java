package com.anyi.achievements.repository.api;

public enum OP {
    // like
    LIKE,
    // like %n%
    LIKE_A,
    // like %n
    LIKE_L,
    // like n%
    LIKE_R,
    // =
    EQ,
    // !=
    NOTEQ,
    // >
    GT,
    // >=
    GTEQ,
    // <
    LT,
    // <=
    LTEQ,
    // is null
    NULL,
    // is not null
    NOTNULL,
    // in
    IN,
    // not in
    NOTIN,

    AND,

    OR,

    NOT
}