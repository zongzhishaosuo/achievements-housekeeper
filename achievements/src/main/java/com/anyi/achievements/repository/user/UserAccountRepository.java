package com.anyi.achievements.repository.user;

import com.anyi.achievements.entity.user.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yanhu
 * @version 2019/6/8 下午 05:54
 */
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

    UserAccount findByUserId(String userId);

}
