package com.anyi.achievements.repository.phone;

import com.anyi.achievements.entity.phone.PhoneCode;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yanhu
 * @version 2019/6/8 下午 09:22
 */
public interface PhoneCodeRepository extends JpaRepository<PhoneCode, Long> {

    PhoneCode findByPhone(String phone);

}
