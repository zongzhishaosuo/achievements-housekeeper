package com.anyi.achievements.repository.user;

import com.anyi.achievements.entity.user.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/8 下午 05:53
 */
public interface UserInfoRepository extends JpaRepository<UserInfo, Long>, JpaSpecificationExecutor<UserInfo> {

    UserInfo findByUserId(String userId);

    UserInfo findByPhoneAndDeleted(String phone, Integer deleted);

    UserInfo findByIdNumberAndDeleted(String idNumber, Integer deleted);

    List<UserInfo> findByUserIdIn(List<String> userIdList);

}
