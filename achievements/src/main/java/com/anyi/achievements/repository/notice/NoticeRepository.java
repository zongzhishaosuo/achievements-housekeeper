package com.anyi.achievements.repository.notice;

import com.anyi.achievements.entity.notice.Notice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/8 下午 05:56
 */
public interface NoticeRepository extends JpaRepository<Notice, Long>, JpaSpecificationExecutor<Notice> {

    /**
     * 获取可见的公告列表
     *
     * @param userId
     * @param unitCode
     * @param number
     * @param size
     * @return
     */
    @Query(value = "SELECT * from tab_notice " +
            " where deleted = 0 " +
            " and revocation = false " +
            " and status like ?1 " +
            " and id in ( " +
            "   select distinct notice_id from tab_notice_scope " +
            "   where user_id = ?2 or dept_code = ?3 or unit_code = ?4 " +
            " ) order by overhead desc, sort desc limit ?5,?6 ", nativeQuery = true)
    List<Notice> getVisibleScopePage(String status, String userId, String deptCode,
                                     String unitCode, Integer number, Integer size);

    /**
     * 可见公告总数
     *
     * @param userId
     * @param unitCode
     * @return
     */
    @Query(value = "SELECT count(1) from tab_notice " +
            " where deleted = 0 " +
            " and revocation = false " +
            " and status like ?1 " +
            " and id in ( " +
            "   select distinct notice_id from tab_notice_scope " +
            "   where user_id = ?2 or dept_code = ?3 or unit_code = ?4 " +
            " ) ", nativeQuery = true)
    Long getVisibleScopeCount(String status, String userId, String deptCode, String unitCode);

    @Query(value = "SELECT * from tab_notice " +
            " where deleted = 0 " +
            " and revocation = false " +
            " and status like ?1 " +
            " and id in ( " +
            "   select distinct notice_id from tab_notice_scope " +
            "   where user_id = ?2 or dept_code = ?3 or unit_code = ?4 " +
            " ) order by overhead desc, sort desc ", nativeQuery = true)
    List<Notice> getVisibleScopeList(String status, String userId, String deptCode, String unitCode);

    @Modifying
    @Transactional
    @Query(value = "UPDATE tab_notice set status = 20 where start_time < ?1 and end_time > ?1 ", nativeQuery = true)
    void setStart(LocalDateTime time);

    @Modifying
    @Transactional
    @Query(value = "UPDATE tab_notice set status = 30 where end_time < ?1", nativeQuery = true)
    void setEnd(LocalDateTime time);

}
