package com.anyi.achievements.repository.certification;

import com.anyi.achievements.entity.certification.CertificationRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/16 下午 04:24
 */
public interface CertificationRecordRepository extends JpaRepository<CertificationRecord, Long> {

    List<CertificationRecord> findByCertificationId(Long certificationId);

}
