package com.anyi.achievements.repository.common;

import com.anyi.achievements.entity.common.CommonResource;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/8 下午 05:57
 */
public interface CommonResourceRepository extends JpaRepository<CommonResource, Long> {

    List<CommonResource> findByCommonIdAndCommonClassAndDeleted(Long commonId, Integer commonClass, Integer deleted);

    void deleteByCommonIdAndCommonClass(Long commonId, Integer commonClass);
    
}
