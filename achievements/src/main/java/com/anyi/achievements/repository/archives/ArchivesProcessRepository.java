package com.anyi.achievements.repository.archives;

import com.anyi.achievements.entity.archives.ArchivesProcess;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/29 下午 02:30
 */
public interface ArchivesProcessRepository extends JpaRepository<ArchivesProcess, Long> {

    List<ArchivesProcess> findByArchivesIdOrderByAtPresent(Long archivesId);

}
