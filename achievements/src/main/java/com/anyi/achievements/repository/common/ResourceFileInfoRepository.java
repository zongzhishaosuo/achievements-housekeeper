package com.anyi.achievements.repository.common;

import com.anyi.achievements.entity.common.ResourceFileInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/19 下午 06:29
 */
public interface ResourceFileInfoRepository extends JpaRepository<ResourceFileInfo, Long> {

    @Query(value = "SELECT " +
            "  *  " +
            "FROM " +
            "  tab_resource_file_info a " +
            "  LEFT JOIN tab_common_resource b ON a.unique_id = b.unique_id  " +
            "WHERE " +
            "  b.unique_id IS NULL AND a.head_img = 0 ", nativeQuery = true)
    List<ResourceFileInfo> uselessFiles();

}
