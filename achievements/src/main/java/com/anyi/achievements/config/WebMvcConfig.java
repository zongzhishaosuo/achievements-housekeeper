package com.anyi.achievements.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author yanhu
 * @version 2019/6/7 下午 04:08
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Value("${mew.file.upload.root-path}")
    private String fileUploadRootPath;

    @Value("${mew.file.request-path}")
    private String fileRequestPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 文件上传路径
        registry.addResourceHandler(fileRequestPath + "**").addResourceLocations("file:" + fileUploadRootPath);

    }

    /**
     * 全局跨域配置
     */
    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration config = new CorsConfiguration();
        // 设置允许跨域请求的域名
        config.addAllowedOrigin("*");
        // 是否允许证书 不再默认开启
        config.setAllowCredentials(true);
        // 设置允许的方法
        config.addAllowedMethod("*");
        // 允许任何头
        config.addAllowedHeader("*");
        // 运行跨域的header值
        config.addExposedHeader("token");
        // 跨域允许时间
        config.setMaxAge(3600L);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        // 拦截所有路径
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

    @Value("${mew.use.token}")
    private Boolean useToken;

    @Autowired
    private TokenInterceptor tokenInterceptor;

    /**
     * 拦截器配置
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // 需要排除的地址
        String[] excludePath = {
                "/app/user/register",
                "/app/user/login",
                "/app/unit/**",
                "/app/phone/**",
                "/admin/**",
                "/test/**",
                "/files/**",
                // 错误页面
                "/error/**",
                // 带后缀的静态资源
                "/**/**.*",
        };
        if (null != useToken && useToken) {
            registry.addInterceptor(tokenInterceptor)
                    .addPathPatterns("/**")
                    .excludePathPatterns(excludePath)
                    .excludePathPatterns();
        }
    }

}
