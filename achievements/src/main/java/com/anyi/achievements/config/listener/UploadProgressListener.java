package com.anyi.achievements.config.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.ProgressListener;
import org.springframework.stereotype.Component;

/**
 * @author yanhu
 * @version 2019/6/15 下午 01:34
 */
@Slf4j
@Component
public class UploadProgressListener implements ProgressListener {

    @Override
    public void update(long pBytesRead, long pContentLength, int pItems) {
        log.info("第{}个文件，上传进度：{}%", pItems, (long) ((double) pBytesRead * 100 / (double) pContentLength));
    }

}
