package com.anyi.achievements.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 异步线程配置
 *
 * @author yanhu
 * @date 2018/9/4
 */
@Slf4j
@Configuration
public class ExecutorConfig {

    @Bean("asyncServiceExecutor")
    public Executor asyncSaveChatExecutor() {
        log.info("start asyncServiceExecutor");
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //配置核心线程数
        executor.setCorePoolSize(4);
        //配置最大线程数
        executor.setMaxPoolSize(8);
        //配置队列大小
        executor.setQueueCapacity(500);
        //配置线程池中的线程的名称前缀
        executor.setThreadNamePrefix("async-service-");
        // rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务，而是有调用者所在的线程来执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        //执行初始化
        executor.initialize();
        return executor;
    }

    /**
     * 定时任务线程池
     *
     * @return
     */
//    @Bean("scheduledThreadPoolExecutor")
//    public Executor scheduledThreadPoolExecutor() {
//        log.info("start scheduledThreadPoolExecutor");
//        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
//        // 配置核心线程数
//        scheduler.setPoolSize(10);
//        scheduler.setThreadNamePrefix("user-defined-scheduled-");
//        return scheduler;
//    }
}
