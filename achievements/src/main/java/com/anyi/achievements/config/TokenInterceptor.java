package com.anyi.achievements.config;

import com.alibaba.fastjson.JSON;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.util.JwtTokenUtils;
import com.anyi.achievements.util.ObjUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 接口调用拦截器
 *
 * @author yanhu
 * @date 2018/9/14
 */
@Slf4j
@Component
public class TokenInterceptor implements HandlerInterceptor {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        String token = request.getHeader("token");
        ErrorCode errorCode;
        if (ObjUtils.notEmpty(token)) {
            // token验证
            String userId = JwtTokenUtils.getUserId(token);
            if (ObjUtils.notEmpty(userId)) {
                String tokenRedis = redisTemplate.opsForValue().get(userId);
                if (token.equals(tokenRedis)) {
                    return true;
                } else {
                    errorCode = ErrorCode.USER_100_TOKEN_INVALID;
                    log.info("token已失效");
                }
            } else {
                errorCode = ErrorCode.USER_100_TOKEN_ERROR;
                log.info("token错误");
            }
        } else {
            errorCode = ErrorCode.USER_100_TOKEN_NOT_EMPTY;
            log.info("token校验不通过");
        }
        printError(response, errorCode);
        return false;
    }

    private void printError(ServletResponse response, ErrorCode errorCode) {
        response.setContentType("application/json; charset=utf-8");
        ServletOutputStream pw = null;
        try {
            pw = response.getOutputStream();
            PrintWriter p = new PrintWriter(pw);
            p.print(JSON.toJSONString(DTO.error(errorCode)));
            p.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != pw) {
                try {
                    pw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView mv) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) throws Exception {

    }

}
