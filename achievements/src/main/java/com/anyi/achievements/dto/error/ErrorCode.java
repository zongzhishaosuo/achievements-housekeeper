package com.anyi.achievements.dto.error;

/**
 * @author yanhu
 * @version 2019/3/15 下午 03:39
 */
public enum ErrorCode {

    /**
     * 人员注册错误码
     */
    USER_100_PHONE_ALREADY_REGISTERED(100_000, "该手机号已被注册"),
    USER_100_PHONE_CAN_NOT_EMPTY(100_001, "手机号不能为空"),
    USER_100_ID_NUMBER_ALREADY_REGISTERED(100_002, "该身份证号已被注册"),
    USER_100_ID_NUMBER_CAN_NOT_EMPTY(100_003, "身份证号不能为空"),

    user_5_user_not_exist(100_005, "人员不存在"),
    user_100_incomplete_user_information(100_007, "用户信息未完善"),

    USER_100_TOKEN_INVALID(100_010, "token已失效，请重新登录获取！"),
    USER_100_TOKEN_ERROR(100_011, "token错误"),
    USER_100_TOKEN_NOT_EMPTY(100_012, "token不能为空"),

    user_login_50_password_error(100_050, "密码错误"),
    user_login_51_no_user_by_phone(100_051, "该手机号未注册"),
    user_login_52_id_number_error(100_052, "身份证号错误"),

    file_101_upload_000_file_can_not_empty(101_000, "上传文件不能为空"),
    file_101_upload_001_file_too_large(101_001, "上传文件过大"),
    file_101_upload_002_upload_fail(101_002, "文件上传失败"),

    phone_102_code_send_fail(102_000, "短信验证码发送失败"),
    phone_102_code_code_error(102_001, "短信验证码错误"),
    phone_102_code_code_expiry(102_002, "短信验证码已过期，请重新获取"),
    phone_102_note_send_fail(102_003, "短信发送失败"),


    notice_103_record_not_exist(103_000, "公告记录不存在"),


    task_104_record_not_exist(104_000, "任务记录不存在"),
    task_104_task_already_selected(1004_001, "任务已被选择执行，无法再修改"),

    CERTIFICATION_105_SUBMITTED_CAN_NOT_MODIFY(105000, "实名认证已提交，不允许再修改"),
    CERTIFICATION_105_PASSED_CAN_NOT_MODIFY(105001, "实名认证已通过，不允许再修改"),
    CERTIFICATION_105_RECORD_NOT_EXIST(105002, "实名认证记录不存在"),

    UNIT_106_NO_HAVE_SUPER_UNIT(106000, "没有上级单位"),
    UNIT_106_NO_HAVE_SUPER_ROLE(106001, "没有上级角色"),

    PERSONAL_ARCHIVES_107_SUBMITTED_CAN_NOT_MODIFY(107000, "个人档案已提交，在未处理前不允许再修改"),
    PERSONAL_ARCHIVES_107_RECORD_NOT_EXIST(107_001, "个人档案记录不存在"),
    PERSONAL_ARCHIVES_107_PASSED_CAN_NOT_MODIFY(107_002, "个人档案已通过，不允许再修改"),

    server_0_params_can_not_null(900000, "必传参数不能为null"),
    server_1_params_can_not_empty(900001, "必传参数不能为空"),
    server_2_record_not_exist(900002, "记录不存在"),
    server_3_params_error(900003, "请求参数错误"),
    SERVER_999_NO_SERVER(900004, "不存在相应服务"),
    SERVER_999_ILLEGAL_OPERATION(900005, "非法操作"),

    server_object_params_inconformity(999996, "所传对象或其字段类型与后台不一致"),
    server_an_undefined_exception(999997, "未定义的异常"),
    server_request_url_not_found(999998, "请求地址不存在"),
    server_error(999999, "服务器异常");

    private int code;
    private String error;

    ErrorCode(int code, String msg) {
        this.error = msg;
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public int getCode() {
        return code;
    }

}
