package com.anyi.achievements.dto;

import com.anyi.achievements.dto.error.ErrorCode;
import lombok.Data;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 接口传输基本结构对象
 *
 * @author yanhu
 * @version 2019/3/15 下午 03:37
 */
@Data
public class DTO<T> {

    private Integer code;

    private String error;

    private T data;

    private String time;

    private DTO(Integer code, String error, T data) {
        this.code = code;
        this.error = error;
        this.data = data;
        this.time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss SSS"));
    }

    /**
     * 成功信息
     *
     * @param data
     * @return
     */
    public static <T> DTO<T> ok(T data) {
        return new DTO<>(0, "", data);
    }

    /**
     * 自定义错误信息
     *
     * @param msg
     * @return
     */
    public static <T> DTO<T> fail(String msg) {
        return new DTO<>(-1, msg, null);
    }

    /**
     * 规定错误信息
     *
     * @param errorCode
     * @return
     */
    public static <T> DTO<T> error(ErrorCode errorCode) {
        return new DTO<>(errorCode.getCode(), errorCode.getError(), null);
    }

    public static <T> DTO<T> error(ErrorCode errorCode, T data) {
        return new DTO<>(errorCode.getCode(), errorCode.getError(), data);
    }

    /**
     * 错误信息返回 + 事务回滚
     *
     * @param errorCode
     * @return
     */
    public static <T> DTO<T> errorRollback(ErrorCode errorCode) {
        // 手动回滚事务
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        return new DTO<>(errorCode.getCode(), errorCode.getError(), null);
    }

    public boolean isSuccess() {
        return 0 == this.code;
    }

    public boolean isFail() {
        return 0 != this.code;
    }
}
