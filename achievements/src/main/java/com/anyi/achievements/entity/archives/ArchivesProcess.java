package com.anyi.achievements.entity.archives;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 档案处理过程
 *
 * @author yanhu
 * @version 2019/6/20 下午 08:47
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_archives_process")
public class ArchivesProcess extends BaseEntity {

    // 档案id
    private Long archivesId;

    // 类型（0审核，1审签）
    private Integer type;

    // 是否已处理
    private Boolean processed;

    // 是否通过
    private Boolean passed;

    // 处理人人员编号
    private String processor;

    // 处理人姓名
    private String processorName;

    // 是否帮忙修改过
    private Boolean modified;

    // 处理过程时间戳
    private Long timestamp;

    // 过程是否处于当前阶段
    private Boolean atPresent;

    // 意见说明
    private String comment;

    public void init() {
        this.processed = false;
        this.passed = false;
        this.modified = false;
        this.atPresent = true;
        this.timestamp = System.currentTimeMillis();
    }
}
