package com.anyi.achievements.entity.certification;

import com.anyi.achievements.bo.constant.CertificationConst;
import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 人员实名认证
 *
 * @author yanhu
 * @version 2019/6/12 下午 11:54
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_certification")
public class Certification extends BaseEntity {

    private String userId;

    // 个人照片
    private String personalPic;

    // 身份证正面照片
    private String idCardFrontPic;

    // 身份证反面照片
    private String idCardReversePic;

    // 审核状态（0未提交，10已提交未审核，20审核通过，30审核不通过）
    private Integer status;

    // 撤销标志（true已撤销，false正常）
    private Boolean revocation;

    // 是否已提交审核（提交后且未审核前不允许修改）
    private Boolean submitted;

    // 审核人人员编号
    private String verifier;

    // 审核人姓名
    private String verifierName;

    // 备注说明
    private String remark;

    public void init() {
        if (null == this.revocation) {
            this.revocation = false;
        }
        if (null == this.status) {
            this.status = CertificationConst.STATUS_0_NOT_SUBMITTED;
        }
        if (null == this.submitted) {
            this.submitted = false;
        }
    }
}
