package com.anyi.achievements.entity.user;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 人员权限
 *
 * @author yanhu
 * @version 2019/6/8 下午 04:51
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_user_auth")
public class UserAuth extends BaseEntity {

    // 人员编号
    @Column(unique = true, nullable = false)
    private String userId;
    // 修改其他下级人员角色权限
    private Boolean modifyRoleAuth;
    // 发布公告权限
    private Boolean noticeReleaseAuth;
    // 能否分配发布公告权限
    private Boolean allotNoticeReleaseAuth;
    // 发布任务权限（超级管理员默认拥有）
    private Boolean taskReleaseAuth;
    // 能否分配发布任务权限（超级管理员）
    private Boolean allotTaskReleaseAuth;

    public void init() {
        if (null == this.modifyRoleAuth) {
            this.modifyRoleAuth = false;
        }
        if (null == this.noticeReleaseAuth) {
            this.noticeReleaseAuth = false;
        }
        if (null == this.allotNoticeReleaseAuth) {
            this.allotNoticeReleaseAuth = false;
        }
        if (null == this.taskReleaseAuth) {
            this.taskReleaseAuth = false;
        }
        if (null == this.allotTaskReleaseAuth) {
            this.allotTaskReleaseAuth = false;
        }
    }
}
