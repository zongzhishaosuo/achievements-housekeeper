package com.anyi.achievements.entity.user;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 个人通知
 *
 * @author yh
 * @version 2019/7/11 上午 11:41
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_user_inform")
public class UserInform extends BaseEntity {

    private String userId;

    private Boolean unread;

    public void init() {
        this.unread = true;
    }
}
