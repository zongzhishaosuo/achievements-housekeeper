package com.anyi.achievements.entity.user;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 个人经历
 *
 * @author yh
 * @version 2019/7/11 上午 11:12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_user_experience")
public class UserExperience extends BaseEntity {

    @Column(unique = true, nullable = false)
    private String userId;

    // 教育经历
    @Column(columnDefinition = "text")
    private String educationExp;

    // 工作经历
    @Column(columnDefinition = "text")
    private String workExp;

    // 家庭信息
    @Column(columnDefinition = "text")
    private String familyInfo;

    // 奖惩信息
    @Column(columnDefinition = "text")
    private String rewardsAndPunishment;

    // 考核信息
    @Column(columnDefinition = "text")
    private String assessmentInfo;

    // 借调信息
    @Column(columnDefinition = "text")
    private String secondmentsInfo;

    // 聘用信息
    @Column(columnDefinition = "text")
    private String employmentInfo;

    // 挂职信息
    @Column(columnDefinition = "text")
    private String temporaryInfo;

    // 出国出境
    @Column(columnDefinition = "text")
    private String leaveCountry;

    // 考察信息
    @Column(columnDefinition = "text")
    private String reviewInfo;

    // 培训信息
    @Column(columnDefinition = "text")
    private String trainingInfo;

    // 其他信息
    @Column(columnDefinition = "text")
    private String otherInfo;

}
