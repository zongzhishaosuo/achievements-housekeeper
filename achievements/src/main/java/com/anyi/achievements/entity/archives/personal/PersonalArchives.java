package com.anyi.achievements.entity.archives.personal;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 个人档案
 *
 * @author yanhu
 * @version 2019/6/19 下午 09:27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_personal_archives")
public class PersonalArchives extends BaseEntity {

    private String userId;

    // 处理人人员编号
    private String processor;

    // 处理人姓名
    private String processorName;

    // 是否已提交
    private Boolean submitted;

    // 是否已处理
    private Boolean processed;

    // 是否通过
    private Boolean passed;

    // 备注说明
    private String remark;

    private Long taskId;
    
    private String taskTitle;

    public void init() {
        this.submitted = false;
        this.processed = false;
        this.passed = false;
    }
}
