package com.anyi.achievements.entity.common;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 通用资源附件
 *
 * @author yanhu
 * @version 2019/6/8 下午 04:37
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_common_resource")
public class CommonResource extends BaseEntity {

    // 附件对应记录id
    private Long commonId;
    // id类别（10公告，20记事本，30任务，40个人档案，50政绩档案，120意见反馈）
    private Integer commonClass;
    // 资源类型（10图片，20音频，30视频，40其他）
    private Integer type;
    // 资源url
    private String url;
    // 资源唯一标识
    private String uniqueId;
    // 名称
    private String name;

}
