package com.anyi.achievements.entity.archives.personal;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 个人档案处理过程
 *
 * @author yanhu
 * @version 2019/6/19 下午 09:36
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_personal_archives_process")
public class PersonalArchivesProcess extends BaseEntity {

    // 个人档案id
    private Long archivesId;

    // 是否通过
    private Boolean passed;

    // 处理人人员编号
    private String processor;

    // 处理人姓名
    private String processorName;

    // 备注说明
    private String remark;

}
