package com.anyi.achievements.entity.feedback;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 意见反馈
 *
 * @author yh
 * @version 2019/7/15 上午 11:53
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_feedback")
public class Feedback extends BaseEntity {

    // 反馈者
    private String feedbackRater;

    // 反馈者姓名
    private String raterName;

    private String phone;

    private String content;

    private Boolean haveAttach;

    public void init() {
        if (null == this.haveAttach) {
            this.haveAttach = false;
        }
    }
}
