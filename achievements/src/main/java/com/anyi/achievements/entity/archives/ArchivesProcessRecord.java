package com.anyi.achievements.entity.archives;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 档案处理过程记录
 *
 * @author yanhu
 * @version 2019/6/29 下午 01:55
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_archives_process_record")
public class ArchivesProcessRecord extends BaseEntity {

    // 档案id
    private Long archivesId;

    // 是否通过
    private Boolean passed;

    // 处理人人员编号
    private String processor;

    // 处理人姓名
    private String processorName;

    // 处理过程记录时间戳
    private Long timestamp;

    // 意见说明
    private String comment;

}
