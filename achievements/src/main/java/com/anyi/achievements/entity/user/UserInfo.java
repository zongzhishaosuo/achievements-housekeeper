package com.anyi.achievements.entity.user;

import com.anyi.achievements.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * 干警人员信息表
 *
 * @author yanhu
 * @version 2019/6/8 下午 03:57
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_user_info")
public class UserInfo extends BaseEntity {

    // 人员编号
    @Column(unique = true, nullable = false)
    private String userId;
    // 姓名
    private String name;
    // 性别（0男，1女，2未知）
    private Integer gender;
    // 身份证号
    private String idNumber;
    // 职位
    private String position;
    // 手机号
    private String phone;
    // 单位编号
    private String unitCode;
    // 单位名称
    private String unitName;
    // 部门编号
    private String deptCode;
    // 部门名称
    private String deptName;
    // 出生日期
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
    // 住址
    private String address;
    // 备注
    private String remark;
    // 是否已完善必填信息
    private Boolean completeInfo;
    // 实名认证状态（0未认证，1认证中，2已认证，3已驳回）
    private Integer certification;

    // 角色编号
    private String roleCode;
    // 系统角色（超级管理员 > 一级审核员 > 二级审核员 > 普通政法干警）
    private String role;

    public void init() {
        if (null == this.gender) {
            this.gender = 2;
        }
        if (null == this.idNumber) {
            this.idNumber = "";
        }
        if (null == this.position) {
            this.position = "";
        }
        if (null == this.phone) {
            this.phone = "";
        }
        if (null == this.unitCode) {
            this.unitCode = "";
        }
        if (null == this.unitName) {
            this.unitName = "";
        }
        if (null == this.deptCode) {
            this.deptCode = "";
        }
        if (null == this.deptName) {
            this.deptName = "";
        }
        if (null == this.address) {
            this.address = "";
        }
        if (null == this.remark) {
            this.remark = "";
        }
        if (null == this.completeInfo) {
            this.completeInfo = false;
        }
        if (null == this.certification) {
            this.certification = 0;
        }
        if (null == this.roleCode) {
            this.roleCode = "";
        }
        if (null == this.role) {
            this.role = "";
        }
    }
}
