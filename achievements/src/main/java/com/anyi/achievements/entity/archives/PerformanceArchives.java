package com.anyi.achievements.entity.archives;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author yanhu
 * @version 2019/6/29 下午 01:58
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_performance_archives")
public class PerformanceArchives extends BaseEntity {

    // 任务id
    private Long taskId;

    // 任务标题
    private String taskTitle;

    // 档案本人人员编号
    private String owner;

    // 申请人姓名
    private String ownerName;

    // 档案定位
    @Column(columnDefinition = "text")
    private String location;

    // 档案主定位（正面清单、中性清单、负面清单）
    private String locationMain;

    // 档案年度
    private Integer year;

    // 档案描述
    @Column(columnDefinition = "text")
    private String description;

    // 是否已提交
    private Boolean submitted;

    // 是否被驳回
    private Boolean reject;

    // 是否已完成（通过最终审签）
    private Boolean finished;

    // 是否拥有附件
    private Boolean haveAttach;

    // 积分
    private Integer score;

    // 所属（个人、集体）
    private String belong;


    public void init() {
        if (null == this.submitted) {
            this.submitted = false;
        }
        if (null == this.reject) {
            this.reject = false;
        }
        if (null == this.finished) {
            this.finished = false;
        }
        if (null == this.haveAttach) {
            this.haveAttach = false;
        }
    }

}
