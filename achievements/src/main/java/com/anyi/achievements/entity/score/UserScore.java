package com.anyi.achievements.entity.score;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * 人员积分
 *
 * @author yanhu
 * @version 2019/6/8 下午 04:28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_user_score")
public class UserScore extends BaseEntity {

    // 人员编号
    private String userId;
    // 年份
    private Integer year;
    // 总积分
    private Integer totalScore;
    // 可用积分
    private Integer usableScore;

    public void init() {
        if (null == year) {
            year = LocalDate.now().getYear();
        }
        if (null == totalScore) {
            totalScore = 0;
        }
        if (null == usableScore) {
            usableScore = 0;
        }
    }
}
