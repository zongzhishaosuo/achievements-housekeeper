package com.anyi.achievements.entity.archives;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 通用档案可见范围
 *
 * @author yanhu
 * @version 2019/6/19 下午 11:13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_archives_scope")
public class ArchivesScope extends BaseEntity {

    private Long archivesId;

    // 档案类型（0个人档案，1政绩档案）
    private Integer archivesType;

    // 档案可见范围类型（10单位，20部门，30人员）
    private Integer scopeType;

    // 单位编号
    private String unitCode;

    // 部门编号
    private String deptCode;

    // 人员编号
    private String userId;
}
