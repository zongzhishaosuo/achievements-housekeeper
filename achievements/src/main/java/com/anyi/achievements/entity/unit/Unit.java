package com.anyi.achievements.entity.unit;

import com.anyi.achievements.bo.constant.UnitConst;
import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 单位
 *
 * @author yanhu
 * @version 2019/6/8 下午 06:05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_unit")
public class Unit extends BaseEntity {

    // 编号（0001政法委，0002公安局，0003检察院，0004法院，0005司，0006乡镇场）
    @Column(unique = true, nullable = false)
    private String code;

    // 名称
    private String name;

    // 上级单位编号（无上级时字段值为"0"）
    private String superCode;

    // 所属类别（001政法委，002公检司法，003乡镇场）
    private String genus;

    // 简介
    private String intro;

    // 备注
    private String remark;

    // 人数
    private Integer peopleNum;

    public void init() {
        if (null == this.superCode) {
            this.superCode = UnitConst.NO_SUPER_CODE;
        }
        if (null == this.intro) {
            this.intro = "";
        }
        if (null == this.remark) {
            this.remark = "";
        }
        if (null == this.peopleNum) {
            this.peopleNum = 0;
        }
    }
}
