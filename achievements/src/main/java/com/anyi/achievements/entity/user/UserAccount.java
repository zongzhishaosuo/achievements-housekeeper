package com.anyi.achievements.entity.user;

import com.anyi.achievements.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 人员账号
 *
 * @author yanhu
 * @version 2019/6/8 下午 03:59
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_user_account")
public class UserAccount extends BaseEntity {

    // 人员编号
    @Column(unique = true, nullable = false)
    private String userId;
    // 密码（MD5加密）
    @JsonIgnore
    private String password;
    // 账号是否可用
    private Boolean usable;
    // 昵称
    private String nickName;
    // 头像url
    private String headImage;

    public void init() {
        if (null == this.usable) {
            this.usable = true;
        }
        if (null == this.password) {
            this.password = "";
        }
        if (null == this.nickName) {
            this.nickName = "";
        }
        if (null == this.headImage) {
            this.headImage = "";
        }
    }
}
