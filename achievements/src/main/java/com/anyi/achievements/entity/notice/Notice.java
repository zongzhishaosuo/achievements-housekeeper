package com.anyi.achievements.entity.notice;

import com.anyi.achievements.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 公告
 *
 * @author yanhu
 * @version 2019/6/8 下午 04:34
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_notice")
public class Notice extends BaseEntity {

    // 发布者人员编号
    private String author;

    // 公告类型，默认0（0通用）
    private Integer type;

    // 标题
    private String title;

    // 副标题
    private String subtitle;

    // 公告内容
    @Column(columnDefinition = "text")
    private String content;

    // 开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    // 结束时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    // 公告状态（10未开始，20进行中，30已结束）
    private Integer status;

    // 撤销标志（true已撤销，false正常）
    private Boolean revocation;

    // 备注
    private String remark;

    // 是否顶置
    private Boolean overhead;

    // 是否拥有附件
    private Boolean haveAttach;

    private String rangeName;

    public void init() {
        if (null == type) {
            this.type = 0;
        }
        if (null == subtitle) {
            this.subtitle = "";
        }
        if (null == remark) {
            this.remark = "";
        }
        if (null == overhead) {
            this.overhead = false;
        }
        if (null == status) {
            status = 10;
        }
        if (null == revocation) {
            revocation = false;
        }
        if (null == haveAttach) {
            haveAttach = false;
        }
    }
}
