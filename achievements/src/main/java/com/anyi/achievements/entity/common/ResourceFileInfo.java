package com.anyi.achievements.entity.common;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author yanhu
 * @version 2019/6/13 下午 06:10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_resource_file_info")
public class ResourceFileInfo extends BaseEntity {

    // 唯一标识
    @Column(unique = true)
    private String uniqueId;
    // MIME类型
    private String mime;
    // 文件大小
    private Long size;
    // 原始名称
    private String originalName;
    // 现名称
    private String nowName;

    private String path;

    private Boolean headImg;
}
