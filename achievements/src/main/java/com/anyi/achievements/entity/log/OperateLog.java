package com.anyi.achievements.entity.log;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 操作日志
 *
 * @author yh
 * @version 2019/7/11 上午 11:48
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_operation_log")
public class OperateLog extends BaseEntity {

    // 操作者
    private String operator;

    // 操作者姓名
    private String operatorName;

    // 外部id
    private Long foreignId;

    // 外部id类型
    private Integer foreignType;

    // 操作类型
    private Integer operateType;

    // 操作内容
    private String content;

}
