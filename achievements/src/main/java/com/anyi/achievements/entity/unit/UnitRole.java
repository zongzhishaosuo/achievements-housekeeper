package com.anyi.achievements.entity.unit;

import com.anyi.achievements.bo.constant.UnitConst;
import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 单位内角色
 *
 * @author yanhu
 * @version 2019/6/16 下午 05:15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_unit_role")
public class UnitRole extends BaseEntity {

    // 单位编号
    private String unitCode;

    // 角色编号
    private String code;

    // 角色
    private String role;

    // 上级角色编号，无上级时字段值为"0"
    private String superCode;

    // 人数
    private Integer peopleNum;

    public void init() {
        if (null == this.superCode) {
            this.superCode = UnitConst.NO_SUPER_CODE;
        }
        this.peopleNum = 0;
    }
}
