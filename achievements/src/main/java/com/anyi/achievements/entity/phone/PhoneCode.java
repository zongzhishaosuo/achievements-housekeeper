package com.anyi.achievements.entity.phone;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 手机验证码
 *
 * @author yanhu
 * @version 2019/6/8 下午 09:17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_phone_code")
public class PhoneCode extends BaseEntity {

    // 手机号
    private String phone;
    // 验证码
    private String code;
    // 发送时间戳
    private Long timestamp;
}
