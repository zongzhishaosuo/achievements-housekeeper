package com.anyi.achievements.entity.task;

import com.anyi.achievements.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 任务
 *
 * @author yanhu
 * @version 2019/6/10 下午 10:19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_task")
public class Task extends BaseEntity {

    // 发布者人员编号
    private String author;

    // 任务类型（0通用任务，10档案任务）
    private Integer type;

    // 状态（10未开始，20进行中，30已过期）
    private Integer status;

    // 标题
    private String title;

    // 副标题
    private String subtitle;

    // 内容
    @Column(columnDefinition = "text")
    private String content;

    // 开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    // 结束时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    // 撤销标志（true已撤销，false正常）
    private Boolean revocation;

    // 备注说明
    private String remark;

    // 是否拥有附件
    private Boolean haveAttach;

    // 任务是否已被选择执行，被选择后无法再修改任务
    private Boolean selected;

    private String rangeName;

    public void init() {
        if (null == this.type) {
            this.type = 0;
        }
        if (null == this.subtitle) {
            this.subtitle = "";
        }
        if (null == this.status) {
            this.status = 10;
        }
        if (null == this.revocation) {
            this.revocation = false;
        }
        if (null == this.haveAttach) {
            this.haveAttach = false;
        }
        if (null == this.selected) {
            this.selected = false;
        }
    }
}
