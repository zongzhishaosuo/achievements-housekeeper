package com.anyi.achievements.entity.notice;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 公告可见范围
 *
 * @author yanhu
 * @version 2019/6/9 下午 02:26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_notice_scope")
public class NoticeScope extends BaseEntity {

    // 公告id
    private Long noticeId;
    // 公告可见范围类型（10单位，20部门，30人员）
    private Integer type;
    // 单位编号
    private String unitCode;
    // 部门编号
    private String deptCode;
    // 人员编号
    private String userId;
}
