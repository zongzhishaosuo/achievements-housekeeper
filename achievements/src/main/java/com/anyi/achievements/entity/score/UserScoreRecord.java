package com.anyi.achievements.entity.score;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 积分记录
 *
 * @author yanhu
 * @version 2019/6/8 下午 04:31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_user_score_record")
public class UserScoreRecord extends BaseEntity {

    // 人员编号
    private String userId;
    // 方式（true奖励，false扣除）
    private Boolean award;
    // 积分数
    private Integer score;
    // 途径（0政绩档案，1初始化）
    private Integer way;

}
