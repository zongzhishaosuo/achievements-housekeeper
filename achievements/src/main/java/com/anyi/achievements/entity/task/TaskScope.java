package com.anyi.achievements.entity.task;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 任务可见范围
 *
 * @author yanhu
 * @version 2019/6/10 下午 10:32
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_task_scope")
public class TaskScope extends BaseEntity {

    // 任务id
    private Long taskId;
    // 任务可见范围类型（10单位，20部门，30个人）
    private Integer type;
    // 单位编号
    private String unitCode;
    // 部门编号
    private String deptCode;
    // 个人编号
    private String userId;

}
