package com.anyi.achievements.entity.inform;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 通知
 *
 * @author yh
 * @version 2019/7/15 上午 10:51
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_inform")
public class Inform extends BaseEntity {

    // 通知者
    private String informer;

    // 外部id
    private Long foreignId;

    // 外部id类型
    private Integer foreignType;

    // 通知类型
    private Integer informType;

    // 内容
    private String content;

    // 是否未读
    private Boolean unread;

    public void init() {
        this.unread = true;
    }
    
}
