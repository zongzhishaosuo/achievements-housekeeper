package com.anyi.achievements.entity.archives;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author yanhu
 * @version 2019/7/16 下午 09:30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_archives_user")
public class ArchivesUser extends BaseEntity {

    private Long archivesId;

    // 档案类型（0政绩档案）
    private Integer archivesType;

    private String userId;

    private String userName;

}
