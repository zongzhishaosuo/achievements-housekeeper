package com.anyi.achievements.entity.certification;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 实名认证审核记录
 *
 * @author yanhu
 * @version 2019/6/16 下午 04:02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_certification_record")
public class CertificationRecord extends BaseEntity {

    // 实名认证记录id
    private Long certificationId;

    // 审核结果（true通过，false不通过）
    private Boolean passed;

    // 说明
    private String remark;

    private String userId;

    // 审核人人员编号
    private String verifier;

    // 审核人姓名
    private String verifierName;
}
