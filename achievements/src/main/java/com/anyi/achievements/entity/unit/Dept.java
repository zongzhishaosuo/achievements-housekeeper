package com.anyi.achievements.entity.unit;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 部门
 *
 * @author yanhu
 * @version 2019/6/10 下午 10:01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_dept")
public class Dept extends BaseEntity {

    // 单位编号
    private String unitCode;

    // 编号
    @Column(unique = true)
    private String code;

    // 名称
    private String name;

    // 简介
    private String intro;

    // 备注
    private String remark;

    // 人数
    private Integer peopleNum;
}
