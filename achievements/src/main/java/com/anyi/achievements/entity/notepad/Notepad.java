package com.anyi.achievements.entity.notepad;

import com.anyi.achievements.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 记事本
 *
 * @author yanhu
 * @version 2019/6/12 下午 11:50
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tab_notepad")
public class Notepad extends BaseEntity {

    // 所属人员编号
    private String userId;

    // 标题
    private String title;

    // 内容
    @Column(columnDefinition = "text")
    private String content;

    // 是否拥有附件
    private Boolean haveAttach;

}
