package com.anyi.achievements.service;

import com.anyi.achievements.entity.common.ResourceFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 异步方法
 *
 * @author yanhu
 * @version 2019/6/13 下午 06:12
 */
@Slf4j
@Service
public class AsyncServiceImpl extends BaseServiceImpl {

    /**
     * 异步保存已上传文件
     */
    @Async("asyncServiceExecutor")
    public void asyncSaveUploadedFileInfo(String path, String uniqueId, String name, boolean headImg) {
        ResourceFileInfo rfi = new ResourceFileInfo();
        rfi.setPath(path);
        rfi.setUniqueId(uniqueId);
        rfi.setNowName(name);
        rfi.setHeadImg(headImg);
        resourceFileInfoRepository.save(rfi);
        log.info("异步保存已上传文件");
    }

}
