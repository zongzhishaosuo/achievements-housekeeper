package com.anyi.achievements.service;

import com.anyi.achievements.util.qcloudsms.SmsSingleSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Random;
import java.util.UUID;

/**
 * @author yanhu
 * @version 2019/6/7 下午 04:07
 */
@Slf4j
public class BaseServiceImpl extends BaseService {


    @Autowired
    protected SmsSingleSender smsSingleSender;

    @Value("${mew.server.host}")
    protected String serverHost;

    @Value("${mew.phone.code.valid-time}")
    protected Long phoneCodeVailTime;

    @Value("${mew.file.upload.root-path}")
    protected String uploadRootPath;

    @Value("${mew.file.upload.max-file-size}")
    protected Long uploadFileMaxSize;

    @Value("${mew.file.request-path}")
    protected String fileRequestPath;

    /**
     * get uuid
     *
     * @return
     */
    protected synchronized static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }


    /**
     * 随机字符串 0-9a-zA-Z
     *
     * @param len 字符串长度
     * @return
     */
    public static String randomStr(int len) {

        StringBuilder sb = new StringBuilder();
        char ch;

        Random random = new Random();
        for (int i = 0; i < len; i++) {
            switch (random.nextInt(3)) {
                case 0:
                    ch = (char) (random.nextInt(10) + 48);
                    break;
                case 1:
                    ch = (char) (random.nextInt(26) + 65);
                    break;
                default:
                    ch = (char) (random.nextInt(26) + 97);
            }
            sb.append(ch);
        }
        return sb.toString();
    }

    /**
     * 随机数字字符串
     *
     * @param len 字符串长度
     * @return
     */
    public static String randomNum(int len) {

        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < len; i++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    protected boolean noEmpty(String str) {
        return null != str && str.length() > 0;
    }

    protected boolean empty(String str) {
        return null == str || str.length() == 0;
    }

}
