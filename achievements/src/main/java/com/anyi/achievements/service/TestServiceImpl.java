package com.anyi.achievements.service;

import com.alibaba.excel.metadata.Sheet;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.entity.score.UserScore;
import com.anyi.achievements.entity.user.UserAccount;
import com.anyi.achievements.entity.user.UserAuth;
import com.anyi.achievements.entity.user.UserInfo;
import com.anyi.achievements.util.ExcelUtils;
import com.anyi.achievements.util.MD5Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/10 上午 10:16
 */
@Slf4j
@Service
public class TestServiceImpl extends BaseServiceImpl {

    public DTO<?> addUser() {

        String path = "D:\\test\\政绩管家人员信息表.xlsx";

        Sheet sheet = new Sheet(3, 4);

        List<Object> list = ExcelUtils.readMoreThan1000RowBySheet(path, sheet);

        List<UserInfo> userInfoList = new ArrayList<>();
        List<UserAccount> accountList = new ArrayList<>();
        List<UserAuth> authList = new ArrayList<>();
        List<UserScore> scoreList = new ArrayList<>();
        if (null != list) {
            for (Object o : list) {
                ArrayList<String> as = (ArrayList<String>) o;
                if (null == as.get(0) || !as.get(0).matches("[0-9]+")) {
                    continue;
                }
                UserInfo info = getInfo(as, getUUID());
                UserAccount account = getAccount(info);
                UserAuth auth = getAuth(info);
                List<UserScore> scores = getScore(info);
                userInfoList.add(info);
                accountList.add(account);
                authList.add(auth);
                scoreList.addAll(scores);
            }
        }

        userInfoRepository.saveAll(userInfoList);
        userScoreRepository.saveAll(scoreList);
        userAccountRepository.saveAll(accountList);
        userAuthRepository.saveAll(authList);

        return DTO.ok("OK");
    }


    private static UserInfo getInfo(ArrayList<String> array, String userId) {
        UserInfo info = new UserInfo();
        info.setUserId(userId);
        info.setName(array.get(1));
        info.setGender(null == array.get(2) ? 2 : ("男".equals(array.get(2)) ? 0 : 1));
        info.setIdNumber(array.get(3));
        info.setPosition(array.get(4));
        info.setPhone(array.get(5));
        info.setUnitName(array.get(7));
        info.setDeptName(array.get(8));
        info.setRole(array.get(9));
        info.init();
        return info;
    }

    private static UserAuth getAuth(UserInfo userInfo) {
        String role = userInfo.getRole();
        boolean superAdmin = "超级管理员".equals(role.trim());
        UserAuth auth = new UserAuth();
        auth.setUserId(userInfo.getUserId());
        auth.setTaskReleaseAuth(superAdmin);
        auth.setNoticeReleaseAuth(superAdmin);
        auth.setModifyRoleAuth(superAdmin);
        auth.setAllotTaskReleaseAuth(superAdmin);
        auth.setAllotNoticeReleaseAuth(superAdmin);
        return auth;
    }

    private static UserAccount getAccount(UserInfo userInfo) {
        String idNumber = userInfo.getIdNumber();
        UserAccount account = new UserAccount();
        account.setUserId(userInfo.getUserId());
        account.setNickName(userInfo.getName());
        account.setHeadImage("");
        account.setUsable(true);
        account.setPassword(idNumber.substring(idNumber.length() - 6));
        return account;
    }

    private static List<UserScore> getScore(UserInfo userInfo) {
        List<UserScore> scoreList = new ArrayList<>(4);
        for (int i = 0; i < 4; i++) {
            UserScore score = new UserScore();
            score.setUserId(userInfo.getUserId());
            score.setYear(2016 + i);
            score.setTotalScore(60);
            score.setUsableScore(60);
            scoreList.add(score);
        }
        return scoreList;
    }

    public static void main(String[] args) {
        String[] ss = {"180036",
                "120018",
                "241313",
                "031512",
                "170011",
                "170054",
                "12005x",
                "030058",
                "140057",
                "090015",
                "211919",
                "111932",
                "030018",
                "030016",
                "040234",
                "060095",
                "040016",
                "270013" };
        for (String s : ss) {
            System.out.println(MD5Utils.md5(s));
        }
    }

}
