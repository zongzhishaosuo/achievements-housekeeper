package com.anyi.achievements.service.app.score;

import com.anyi.achievements.bo.UserScoreBO;
import com.anyi.achievements.bo.constant.UserConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.score.UserScore;
import com.anyi.achievements.entity.score.UserScoreRecord;
import com.anyi.achievements.entity.user.UserInfo;
import com.anyi.achievements.repository.api.OP;
import com.anyi.achievements.repository.api.Pre;
import com.anyi.achievements.repository.api.PreList;
import com.anyi.achievements.repository.api.SpecFactory;
import com.anyi.achievements.service.BaseServiceImpl;
import com.anyi.achievements.util.ObjUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 积分服务
 *
 * @author yanhu
 * @version 2019/6/12 下午 10:05
 */
@Service
public class ScoreServiceImpl extends BaseServiceImpl {


    /**
     * 获取积分排名
     *
     * @param userId
     * @return
     */
    public DTO<?> getScoreRanking(String userId, Integer rule) {
        if (ObjUtils.notEmpty(userId, rule)) {
            UserInfo userInfo = userInfoRepository.findByUserId(userId);
            String unitCode = userInfo.getUnitCode();
            String deptCode = userInfo.getDeptCode();
            Integer allRanking;
            Integer unitRanking;
            Integer deptRanking;
            int year = LocalDate.now().getYear();
            switch (rule) {
                case 0:
                    // 历年总积分
                case 1:
                    // 实时总积分
                    allRanking = userScoreRepository.getAllRanking("%", "%", userId);
                    unitRanking = userScoreRepository.getAllRanking(unitCode, "%", userId);
                    deptRanking = userScoreRepository.getAllRanking(unitCode, deptCode, userId);
                    break;
                default:
                    // 当年总积分
                    allRanking = userScoreRepository.getThisYearRanking(year, "%", "%", userId);
                    unitRanking = userScoreRepository.getThisYearRanking(year, unitCode, "%", userId);
                    deptRanking = userScoreRepository.getThisYearRanking(year, unitCode, deptCode, userId);
            }
            UserScoreBO userScoreBO = new UserScoreBO();
            userScoreBO.setAllRanking(allRanking);
            userScoreBO.setUnitRanking(unitRanking);
            userScoreBO.setDeptRanking(deptRanking);
            return DTO.ok(userScoreBO);
        }
        return DTO.error(ErrorCode.server_1_params_can_not_empty);
    }

    /**
     * 获取历年积分
     *
     * @param userId
     * @return
     */
    public DTO<?> getOverTheYearsScore(String userId) {
        if (ObjUtils.notEmpty(userId)) {
            List<UserScore> userScoreList = new ArrayList<>();
            int year = LocalDate.now().getYear();
            for (int i = UserConst.SCORE_START_YEAR; i <= year; i++) {
                UserScore userScore = userScoreRepository.findByUserIdAndYear(userId, i);
                userScoreList.add(userScore);
            }
            return DTO.ok(userScoreList);
        }
        return DTO.error(ErrorCode.server_1_params_can_not_empty);
    }

    /**
     * 获取积分明细记录
     *
     * @param userId
     * @param award
     * @param way
     * @param number
     * @param size
     * @return
     */
    public DTO<?> getScoreRecordPage(String userId, Boolean award, Integer way, Integer number, Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        Pageable pageable = PageRequest.of(number, size, sort);
        List<Pre> ps = PreList.bui()
                .add(Pre.of(OP.EQ, "userId", userId))
                .add(Pre.of(OP.EQ, "award", award))
                .add(Pre.of(OP.EQ, "way", way))
                .get();
        Page<UserScoreRecord> recordPage = userScoreRecordRepository.findAll(SpecFactory.where(ps), pageable);
        return DTO.ok(recordPage);
    }

}
