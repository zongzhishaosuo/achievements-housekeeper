package com.anyi.achievements.service.app.notepad;

import com.anyi.achievements.bo.NotepadBO;
import com.anyi.achievements.bo.constant.BaseConst;
import com.anyi.achievements.bo.constant.CommonResourceConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.PageBean;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.common.CommonResource;
import com.anyi.achievements.entity.notepad.Notepad;
import com.anyi.achievements.repository.api.OP;
import com.anyi.achievements.repository.api.Pre;
import com.anyi.achievements.repository.api.PreList;
import com.anyi.achievements.repository.api.SpecFactory;
import com.anyi.achievements.service.BaseServiceImpl;
import com.anyi.achievements.util.BeanUtils;
import com.anyi.achievements.util.ObjUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 记事本服务
 *
 * @author yanhu
 * @version 2019/6/13 下午 08:37
 */
@Slf4j
@Service
public class NotepadServiceImpl extends BaseServiceImpl {


    /**
     * 新增记事本记录
     *
     * @param notepadBO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> addNotepad(NotepadBO notepadBO) {
        Notepad notepad = notepadBO.getNotepad();
        if (ObjUtils.notEmpty(notepad)) {
            notepad = notepadRepository.save(notepad);
            notepadBO.setNotepad(notepad);
            // 保存记事本附件资源
            saveNotepadResource(notepadBO);
            return DTO.ok(notepadBO);
        }
        return DTO.error(ErrorCode.server_1_params_can_not_empty);
    }

    /**
     * 保存记事本附件资源
     *
     * @param notepadBO
     * @return
     */
    private void saveNotepadResource(NotepadBO notepadBO) {
        Notepad notepad = notepadBO.getNotepad();
        List<CommonResource> commonResourceList = notepadBO.getCommonResourceList();
        if (null != commonResourceList && commonResourceList.size() > 0) {
            notepad.setHaveAttach(true);
            long notepadId = notepad.getId();
            for (CommonResource commonResource : commonResourceList) {
                commonResource.setCommonId(notepadId);
                commonResource.setCommonClass(CommonResourceConst.COMMON_CLASS_20_NOTEPAD);
            }
            commonResourceList = commonResourceRepository.saveAll(commonResourceList);
            notepadBO.setCommonResourceList(commonResourceList);
        } else {
            notepad.setHaveAttach(false);
        }
    }

    /**
     * 更新记事本
     *
     * @param notepadBO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> updateNotepad(NotepadBO notepadBO) {
        Notepad notepad = notepadBO.getNotepad();
        if (ObjUtils.notEmpty(notepad)) {
            long notepadId = notepad.getId();
            Optional<Notepad> notepadOptional = notepadRepository.findById(notepadId);
            if (notepadOptional.isPresent()) {
                Notepad notepadDB = notepadOptional.get();
                BeanUtils.copyProperties(notepad, notepadDB);
                notepadRepository.save(notepadDB);
                notepadBO.setNotepad(notepadDB);

                // 删除原有附件
                deleteResource(notepadId);

                // 保存记事本附件资源
                saveNotepadResource(notepadBO);
                return DTO.ok(notepadBO);
            }
            return DTO.error(ErrorCode.server_2_record_not_exist);
        }
        return DTO.error(ErrorCode.server_1_params_can_not_empty);
    }

    /**
     * @param notepadId
     * @return
     */
    private List<CommonResource> getResourceList(Long notepadId) {
        return commonResourceRepository
                .findByCommonIdAndCommonClassAndDeleted(notepadId, CommonResourceConst.COMMON_CLASS_20_NOTEPAD, BaseConst.DELETED_0_NORMAL);
    }

    private void deleteResource(Long notepadId) {
        commonResourceRepository.deleteByCommonIdAndCommonClass(notepadId, CommonResourceConst.COMMON_CLASS_20_NOTEPAD);
    }

    /**
     * 删除记事本
     *
     * @param ids
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> deleteNotepad(List<Long> ids) {
        int count = 0;
        if (null != ids) {
            for (Long id : ids) {
                Optional<Notepad> optional = notepadRepository.findById(id);
                if (optional.isPresent()) {
                    Notepad notepad = optional.get();
                    if (notepad.getHaveAttach()) {
                        // 删除附件信息
                        deleteResource(id);
                    }
                    notepadRepository.deleteById(id);
                    count++;
                }
            }
        }
        return DTO.ok(count);
    }

    /**
     * 获取记事本列表
     *
     * @param userId
     * @param number
     * @param size
     * @return
     */
    public DTO<?> notepadPage(String userId, Integer number, Integer size) {
        if (ObjUtils.notEmpty(userId, number, size)) {
            Pageable pageable = PageRequest.of(number, size, Sort.by(Sort.Direction.DESC, "modifyTime"));
            List<Pre> pres = PreList.bui()
                    .add(Pre.get(OP.EQ, "userId", userId))
                    .get();
            Page<Notepad> notepadPage = notepadRepository.findAll(SpecFactory.where(pres), pageable);
            List<Notepad> notepadList = notepadPage.getContent();
            List<NotepadBO> notepadBOList = new ArrayList<>(size);
            NotepadBO notepadBO;
            for (Notepad notepad : notepadList) {
                notepadBO = new NotepadBO();
                notepadBO.setNotepad(notepad);
                if (notepad.getHaveAttach()) {
                    notepadBO.setCommonResourceList(getResourceList(notepad.getId()));
                }
                notepadBOList.add(notepadBO);
            }
            return DTO.ok(PageBean.of(number, size, notepadPage.getTotalElements(), notepadBOList));
        }
        return DTO.error(ErrorCode.server_1_params_can_not_empty);
    }
}
