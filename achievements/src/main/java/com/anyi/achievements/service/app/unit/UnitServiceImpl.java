package com.anyi.achievements.service.app.unit;

import com.anyi.achievements.bo.UnitBO;
import com.anyi.achievements.bo.constant.BaseConst;
import com.anyi.achievements.bo.constant.UnitConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.unit.Dept;
import com.anyi.achievements.entity.unit.Unit;
import com.anyi.achievements.entity.unit.UnitRole;
import com.anyi.achievements.service.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 单位服务
 *
 * @author yanhu
 * @version 2019/6/9 下午 02:35
 */
@Slf4j
@Service
public class UnitServiceImpl extends BaseServiceImpl {

    /**
     * 获取所有单位及其下所有部门
     *
     * @return
     */
    public DTO<?> getAllUnitAndDept() {
        List<UnitBO> unitBOList = new ArrayList<>();
        List<Unit> unitList = unitRepository.findByDeletedOrderBySort(BaseConst.DELETED_0_NORMAL);
        if (null != unitList) {
            UnitBO unitBO;
            String unitCode;
            for (Unit unit : unitList) {
                unitCode = unit.getCode();
                List<Dept> deptList = deptRepository
                        .findByUnitCodeOrderBySort(unitCode);
                List<UnitRole> unitRoleList = unitRoleRepository.findByUnitCode(unitCode);
                unitBO = new UnitBO();
                unitBO.setUnit(unit);
                unitBO.setDeptList(deptList);
                unitBO.setUnitRoleList(unitRoleList);
                unitBOList.add(unitBO);
            }
        }
        return DTO.ok(unitBOList);
    }


    public DTO<?> getSuperUnitAndRole(String roleCode) {
        UnitRole unitRole = unitRoleRepository.findByCode(roleCode);
        if (null != unitRole) {
            if (UnitConst.NO_SUPER_CODE.equals(unitRole.getSuperCode())) {
                return DTO.ok(unitRole);
            } else {
                unitRole = unitRoleRepository.findByCode(unitRole.getSuperCode());
                if (null != unitRole) {
                    return DTO.ok(unitRole);
                }
            }
        }
        return DTO.error(ErrorCode.server_2_record_not_exist);
    }
}
