package com.anyi.achievements.service.app.archives;

import com.anyi.achievements.entity.task.Task;
import com.anyi.achievements.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author yh
 * @version 2019/7/11 下午 06:28
 */
@Service
public class ArchivesServiceImpl extends BaseServiceImpl {

    void selectTask(Long taskId) {
        if (null == taskId) {
            return;
        }
        Optional<Task> optional = taskRepository.findById(taskId);
        if (optional.isPresent()) {
            Task task = optional.get();
            if (null == task.getSelected() || !task.getSelected()) {
                task.setSelected(true);
                taskRepository.save(task);
            }
        }
    }

}
