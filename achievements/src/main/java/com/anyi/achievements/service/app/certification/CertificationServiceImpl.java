package com.anyi.achievements.service.app.certification;

import com.anyi.achievements.bo.CertificationBO;
import com.anyi.achievements.bo.constant.CertificationConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.PageBean;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.certification.Certification;
import com.anyi.achievements.entity.certification.CertificationRecord;
import com.anyi.achievements.entity.user.UserInfo;
import com.anyi.achievements.repository.api.OP;
import com.anyi.achievements.repository.api.Pre;
import com.anyi.achievements.repository.api.PreList;
import com.anyi.achievements.repository.api.SpecFactory;
import com.anyi.achievements.service.BaseServiceImpl;
import com.anyi.achievements.util.BeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 人员实名认证
 *
 * @author yanhu
 * @version 2019/6/16 下午 03:44
 */
@Slf4j
@Service
public class CertificationServiceImpl extends BaseServiceImpl {


    /**
     * 获取自身实名认证记录
     *
     * @param userId
     * @return
     */
    public DTO<?> getOneself(String userId) {
        CertificationBO certificationBO = new CertificationBO();
        Certification certification = certificationRepository.findByUserId(userId);
        if (null != certification) {
            List<CertificationRecord> recordList = certificationRecordRepository.findByCertificationId(certification.getId());
            certificationBO.setCertification(certification);
            certificationBO.setCertificationRecordList(recordList);
        }
        return DTO.ok(certificationBO);
    }


    public DTO<?> getById(Long id) {
        Optional<Certification> optional = certificationRepository.findById(id);
        if (optional.isPresent()) {
            Certification certification = optional.get();
            List<CertificationRecord> recordList = certificationRecordRepository.findByCertificationId(certification.getId());
            UserInfo userInfo = userInfoRepository.findByUserId(certification.getUserId());
            CertificationBO certificationBO = new CertificationBO();
            certificationBO.setUserInfo(userInfo);
            certificationBO.setCertification(certification);
            certificationBO.setCertificationRecordList(recordList);
            return DTO.ok(certificationBO);
        }
        return DTO.error(ErrorCode.CERTIFICATION_105_RECORD_NOT_EXIST);
    }

    /**
     * 新增或修改实名认证
     *
     * @param certification
     * @return
     */
    public DTO<?> addOrUpdate(Certification certification) {
        Certification certificationDB = certificationRepository.findByUserId(certification.getUserId());
        if (null != certificationDB) {
            int status = certificationDB.getStatus();
            if (certificationDB.getSubmitted() && CertificationConst.STATUS_10_UNTREATED == status) {
                return DTO.error(ErrorCode.CERTIFICATION_105_SUBMITTED_CAN_NOT_MODIFY);
            }
            if (CertificationConst.STATUS_20_PASS == status) {
                return DTO.error(ErrorCode.CERTIFICATION_105_PASSED_CAN_NOT_MODIFY);
            }
            Boolean submitted = certification.getSubmitted();
            if (null != submitted && submitted && CertificationConst.STATUS_10_UNTREATED != status) {
                certification.setStatus(CertificationConst.STATUS_10_UNTREATED);
            }
            BeanUtils.copyProperties(certification, certificationDB);
            certification = certificationDB;
        } else {
            certification.init();
        }
        certification = certificationRepository.save(certification);
        UserInfo userInfo = userInfoRepository.findByUserId(certification.getUserId());
        userInfo.setCertification(1);
        userInfoRepository.save(userInfo);
        return DTO.ok(certification);
    }

    /**
     * 获取下级提交的实名认证记录
     *
     * @param verifier
     * @return
     */
    public DTO<?> getSubordinateCertificationPage(String verifier, List<Integer> statusList, Integer number, Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC, "modifyTime");
        Pageable pageable = PageRequest.of(number, size, sort);
        List<Pre> pres = PreList.bui()
                .add(Pre.get(OP.EQ, "revocation", Boolean.FALSE))
                .add(Pre.get(OP.EQ, "submitted", Boolean.TRUE))
                .add(Pre.get(OP.EQ, "verifier", verifier))
                .add(Pre.of(OP.NOTEQ, "status", CertificationConst.STATUS_0_NOT_SUBMITTED))
                .add(Pre.of(OP.IN, "status", statusList))
                .get();
        Page<Certification> certificationPage = certificationRepository.findAll(SpecFactory.where(pres), pageable);
        List<Certification> certificationList = certificationPage.getContent();
        List<CertificationBO> certificationBOList = new ArrayList<>(size);
        if (certificationList.size() > 0) {
            CertificationBO bo;
            UserInfo userInfo;
            for (Certification certification : certificationList) {
                userInfo = userInfoRepository.findByUserId(certification.getUserId());
                List<CertificationRecord> recordList = certificationRecordRepository.findByCertificationId(certification.getId());
                bo = new CertificationBO();
                bo.setUserInfo(userInfo);
                bo.setCertification(certification);
                bo.setCertificationRecordList(recordList);
                certificationBOList.add(bo);
            }
        }
        return DTO.ok(PageBean.of(number, size, certificationPage.getTotalElements(), certificationBOList));
    }

    /**
     * 上级审核人处理实名认证
     *
     * @param userId
     * @param status
     * @param remark
     * @return
     */
    public DTO<?> verifyHandle(String userId, Integer status, String remark) {
        Certification certification = certificationRepository.findByUserId(userId);
        if (null != certification) {
            certification.setStatus(status);
            certification = certificationRepository.save(certification);

            boolean pass = CertificationConst.STATUS_20_PASS == status;

            UserInfo userInfo = userInfoRepository.findByUserId(userId);
            if (pass) {
                userInfo.setCertification(2);
            } else {
                userInfo.setCertification(3);
            }
            userInfo = userInfoRepository.save(userInfo);

            CertificationRecord record = new CertificationRecord();
            record.setCertificationId(certification.getId());
            record.setPassed(pass);
            record.setRemark(remark);
            record.setUserId(userId);
            record.setVerifier(certification.getVerifier());
            record.setVerifierName(certification.getVerifierName());
            record = certificationRecordRepository.save(record);

            CertificationBO bo = new CertificationBO();
            bo.setCertification(certification);
            bo.setCertificationRecord(record);
            bo.setUserInfo(userInfo);
            return DTO.ok(bo);
        }
        return DTO.error(ErrorCode.CERTIFICATION_105_RECORD_NOT_EXIST);
    }
}
