package com.anyi.achievements.service.app.inform;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.entity.inform.Inform;
import com.anyi.achievements.repository.api.OP;
import com.anyi.achievements.repository.api.Pre;
import com.anyi.achievements.repository.api.PreList;
import com.anyi.achievements.repository.api.SpecFactory;
import com.anyi.achievements.service.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yh
 * @version 2019/7/15 上午 11:23
 */
@Slf4j
@Service
public class InformServiceImpl extends BaseServiceImpl {

    public DTO<?> page(String informer, Integer number, Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        Pageable pageable = PageRequest.of(number, size, sort);
        PreList.Bui bui = PreList.bui().add(Pre.of(OP.EQ, "informer", informer));
        Page<Inform> informPage = informRepository.findAll(SpecFactory.where(bui.get()), pageable);
        return DTO.ok(informPage);
    }

    public DTO<?> read(List<Long> ids) {
        List<Inform> informList = informRepository.findAllById(ids);
        if (informList.size() > 0) {
            for (Inform inform : informList) {
                if (inform.getUnread()) {
                    inform.setUnread(false);
                }
            }
            informRepository.saveAll(informList);
        }
        return DTO.ok("ok");
    }

}
