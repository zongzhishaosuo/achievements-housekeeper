package com.anyi.achievements.service.app.log;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.entity.log.OperateLog;
import com.anyi.achievements.service.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * @author yh
 * @version 2019/7/15 上午 11:28
 */
@Slf4j
@Service
public class OperateLogServiceImpl extends BaseServiceImpl {

    public DTO<?> page(Integer number, Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        Pageable pageable = PageRequest.of(number, size, sort);
        Page<OperateLog> logPage = operateLogRepository.findAll(pageable);
        return DTO.ok(logPage);
    }

}
