package com.anyi.achievements.service.base;

import com.anyi.achievements.bo.UserBO;
import com.anyi.achievements.bo.constant.BaseConst;
import com.anyi.achievements.bo.constant.UserConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.phone.PhoneCode;
import com.anyi.achievements.entity.score.UserScore;
import com.anyi.achievements.entity.user.UserAccount;
import com.anyi.achievements.entity.user.UserAuth;
import com.anyi.achievements.entity.user.UserInfo;
import com.anyi.achievements.service.BaseServiceImpl;
import com.anyi.achievements.util.MD5Utils;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;

/**
 * @author yh
 * @version 2019/7/23 下午 07:02
 */
@Slf4j
public class BaseUserServiceImpl extends BaseServiceImpl {

    /**
     * 新增人员信息
     *
     * @param userBO
     * @param userId
     * @return
     */
    protected DTO<UserBO> addUserInfo(UserBO userBO, String userId) {
        UserInfo userInfo = userBO.getUserInfo();
        if (null != userInfo) {
            String phone = userInfo.getPhone();
            if (null != phone) {
                UserInfo userInfoByPhone = userInfoRepository
                        .findByPhoneAndDeleted(phone, BaseConst.DELETED_0_NORMAL);
                if (null != userInfoByPhone) {
                    log.info("手机号重复：" + phone);
                    return DTO.error(ErrorCode.USER_100_PHONE_ALREADY_REGISTERED, userBO);
                }
            } else {
                return DTO.error(ErrorCode.USER_100_PHONE_CAN_NOT_EMPTY, userBO);
            }
            String idNumber = userInfo.getIdNumber();
            if (null != idNumber) {
                UserInfo userInfoByIdNumber = userInfoRepository
                        .findByIdNumberAndDeleted(idNumber, BaseConst.DELETED_0_NORMAL);
                if (null != userInfoByIdNumber) {
                    log.info("身份证号重复：" + idNumber);
                    return DTO.error(ErrorCode.USER_100_ID_NUMBER_ALREADY_REGISTERED, userBO);
                }
            } else {
                return DTO.error(ErrorCode.USER_100_ID_NUMBER_CAN_NOT_EMPTY, userBO);
            }
            userInfo.setUserId(userId);
            userInfo.init();
            userInfo = userInfoRepository.save(userInfo);
            userBO.setUserInfo(userInfo);
            return DTO.ok(userBO);
        }
        return DTO.error(ErrorCode.server_0_params_can_not_null, userBO);
    }

    /**
     * 添加人员权限
     *
     * @param userBO
     * @param userId
     * @return
     */
    protected void addUserAuth(UserBO userBO, String userId) {
        UserAuth userAuth = userBO.getUserAuth();
        if (null == userAuth) {
            userAuth = new UserAuth();
        }
        userAuth.setUserId(userId);
        userAuth.init();
        userAuth = userAuthRepository.save(userAuth);
        userBO.setUserAuth(userAuth);
    }

    /**
     * 添加人员账号
     *
     * @param userBO
     * @param userId
     * @return
     */
    protected void addUserAccount(UserBO userBO, String userId) {
        UserAccount userAccount = userBO.getUserAccount();
        if (null == userAccount) {
            userAccount = new UserAccount();
        }
        if (null == userAccount.getNickName()) {
            userAccount.setNickName("干警" + System.currentTimeMillis());
        }
        if (null == userAccount.getPassword()) {
            userAccount.setPassword(MD5Utils.md5("123456"));
        }
        userAccount.setUserId(userId);
        userAccount.init();
        userAccount = userAccountRepository.save(userAccount);
        userBO.setUserAccount(userAccount);
    }

    /**
     * 添加人员积分
     *
     * @param userId
     * @return
     */
    protected void addUserScore(UserBO userBO, String userId) {
        UserScore userScore = new UserScore();
        userScore.setUserId(userId);
        userScore.init();
        userScore.setTotalScore(60);
        userScore.setUsableScore(60);
        userScore = userScoreRepository.save(userScore);
        userBO.setUserScore(userScore);
        // 增加往年用户积分
        addFormerYearsUserScore(userId);
    }

    /**
     * 增加往年用户积分
     *
     * @param userId
     */
    private void addFormerYearsUserScore(String userId) {
        UserScore userScore;
        int year = LocalDate.now().getYear();
        for (int i = UserConst.SCORE_START_YEAR; i < year; i++) {
            userScore = new UserScore();
            userScore.setUserId(userId);
            userScore.setYear(i);
            userScore.init();
            userScoreRepository.save(userScore);
        }
    }

    /**
     * 验证短信验证码
     *
     * @param phone
     * @param code
     * @return
     */
    protected DTO<UserBO> verifyPhoneCode(String phone, String code) {
        PhoneCode phoneCode = phoneCodeRepository.findByPhone(phone);
        if (null != phoneCode && code.equals(phoneCode.getCode())) {
            long now = System.currentTimeMillis() / 1000;
            if (now - phoneCode.getTimestamp() <= phoneCodeVailTime) {
                return DTO.ok(null);
            }
            return DTO.error(ErrorCode.phone_102_code_code_expiry);
        }
        return DTO.error(ErrorCode.phone_102_code_code_error);
    }

}
