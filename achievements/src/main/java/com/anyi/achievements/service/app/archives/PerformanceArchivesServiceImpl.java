package com.anyi.achievements.service.app.archives;

import com.anyi.achievements.bo.ArchivesProcessBO;
import com.anyi.achievements.bo.PerformanceArchivesBO;
import com.anyi.achievements.bo.UserBO;
import com.anyi.achievements.bo.constant.BaseConst;
import com.anyi.achievements.bo.constant.CommonResourceConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.PageBean;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.archives.ArchivesProcess;
import com.anyi.achievements.entity.archives.ArchivesProcessRecord;
import com.anyi.achievements.entity.archives.ArchivesUser;
import com.anyi.achievements.entity.archives.PerformanceArchives;
import com.anyi.achievements.entity.common.CommonResource;
import com.anyi.achievements.entity.score.UserScore;
import com.anyi.achievements.entity.score.UserScoreRecord;
import com.anyi.achievements.entity.task.Task;
import com.anyi.achievements.entity.user.UserAccount;
import com.anyi.achievements.entity.user.UserInfo;
import com.anyi.achievements.util.BeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 政绩档案
 *
 * @author yanhu
 * @version 2019/6/29 下午 02:32
 */
@Slf4j
@Service
public class PerformanceArchivesServiceImpl extends ArchivesServiceImpl {

    /**
     * 新增或修改
     *
     * @param bo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> save(PerformanceArchivesBO bo) {
        DTO<?> dto = addArchives(bo);
        if (dto.isFail()) {
            return dto;
        }
        addArchivesUser(bo);
        // 附件
        addCommonResource(bo);
        return DTO.ok(bo);
    }

    /**
     * 新增或修改
     *
     * @param bo
     * @return
     */
    private DTO<?> addArchives(PerformanceArchivesBO bo) {
        PerformanceArchives archives = bo.getPerformanceArchives();
        if (null == archives) {
            return DTO.error(ErrorCode.server_0_params_can_not_null);
        }
        Long id = archives.getId();
        if (null != id) {
            Optional<PerformanceArchives> op = performanceArchivesRepository.findById(id);
            if (op.isPresent()) {
                PerformanceArchives archivesDB = op.get();
                BeanUtils.copyProperties(archives, archivesDB);
                archives = archivesDB;
                archivesUserRepository.deleteByArchivesIdAndArchivesType(id, 0);
                // 删除原有附件
                commonResourceRepository.deleteByCommonIdAndCommonClass(id, CommonResourceConst.COMMON_CLASS_50_PERFORMANCE_ARCHIVES);
            } else {
                return DTO.error(ErrorCode.server_2_record_not_exist);
            }
        } else {
            archives.init();
        }
        // 设置任务为已选择
        selectTask(archives.getTaskId());

        archives = performanceArchivesRepository.save(archives);
        bo.setPerformanceArchives(archives);
        return DTO.ok(bo);
    }

    private void addArchivesUser(PerformanceArchivesBO bo) {
        List<ArchivesUser> userList = bo.getArchivesUserList();
        if (null != userList && userList.size() > 0) {
            long archivesId = bo.getPerformanceArchives().getId();
            for (ArchivesUser archivesUser : userList) {
                archivesUser.setArchivesId(archivesId);
                archivesUser.setArchivesType(0);
            }
            userList = archivesUserRepository.saveAll(userList);
            bo.setArchivesUserList(userList);
        }
    }

    /**
     * 新增附件列表
     *
     * @param bo
     */
    private void addCommonResource(PerformanceArchivesBO bo) {
        List<CommonResource> resourceList = bo.getCommonResourceList();
        PerformanceArchives archives = bo.getPerformanceArchives();
        if (null != resourceList && resourceList.size() > 0) {
            archives.setHaveAttach(true);
            long archivesId = archives.getId();
            for (CommonResource commonResource : resourceList) {
                commonResource.setCommonId(archivesId);
                commonResource.setCommonClass(CommonResourceConst.COMMON_CLASS_50_PERFORMANCE_ARCHIVES);
            }
            resourceList = commonResourceRepository.saveAll(resourceList);
            bo.setCommonResourceList(resourceList);
        } else {
            archives.setHaveAttach(false);
        }
    }


    /**
     * 指定并提交
     *
     * @param process
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> submit(ArchivesProcess process) {
        Optional<PerformanceArchives> op = performanceArchivesRepository.findById(process.getArchivesId());
        if (op.isPresent()) {
            PerformanceArchivesBO bo = new PerformanceArchivesBO();
            PerformanceArchives archives = op.get();
            if (!archives.getSubmitted()) {
                Long id = process.getId();
                if (null != id) {
                    // 驳回后再提交
                    Optional<ArchivesProcess> optional = archivesProcessRepository.findById(id);
                    if (optional.isPresent()) {
                        ArchivesProcess processDB = optional.get();
                        BeanUtils.copyProperties(process, processDB);
                        process = processDB;
                    } else {
                        return DTO.error(ErrorCode.server_2_record_not_exist);
                    }
                } else {
                    // 新提交
                    process.init();
                }

                // 提交标志
                archives.setSubmitted(true);
                archives.setReject(false);
                archives = performanceArchivesRepository.save(archives);
                // 修改为未处理状态
                process.setProcessed(false);
                process.setPassed(false);
                process = archivesProcessRepository.save(process);
            }
            bo.setArchivesProcess(process);
            bo.setPerformanceArchives(archives);
            return DTO.ok(bo);
        } else {
            return DTO.errorRollback(ErrorCode.server_2_record_not_exist);
        }
    }

    /**
     * 获取附件列表
     *
     * @param archivesId
     * @return
     */
    private List<CommonResource> getCommonResource(Long archivesId) {
        return commonResourceRepository
                .findByCommonIdAndCommonClassAndDeleted
                        (archivesId, CommonResourceConst.COMMON_CLASS_50_PERFORMANCE_ARCHIVES, BaseConst.DELETED_0_NORMAL);
    }

    /**
     * 获取自身的
     *
     * @param userId
     * @return
     */
    public DTO<?> ownPage(String userId, String locationMain, String belong,
                          String year, Integer number, Integer size) {

        locationMain = noEmpty(locationMain) ? locationMain : "%";
        belong = noEmpty(belong) ? belong : "%";
        year = noEmpty(year) ? year : "%";

        List<PerformanceArchives> archivesList = performanceArchivesRepository
                .ownPage(userId, locationMain, belong, year, number * size, size);
        if (null == archivesList) {
            archivesList = new ArrayList<>();
        }
        Long count = performanceArchivesRepository.ownCount(userId, locationMain, belong, year);
        List<PerformanceArchivesBO> boList = new ArrayList<>();
        for (PerformanceArchives archives : archivesList) {
            List<ArchivesUser> archivesUsers = archivesUserRepository
                    .findByArchivesIdAndArchivesType(archives.getId(), 0);
            List<ArchivesProcess> processList = archivesProcessRepository.findByArchivesIdOrderByAtPresent(archives.getId());

            PerformanceArchivesBO bo = new PerformanceArchivesBO();
            if (null != processList && processList.size() > 0) {
                bo.setArchivesProcessList(processList);
                bo.setArchivesProcess(processList.get(processList.size() - 1));
            }
            bo.setArchivesUserList(archivesUsers);
            bo.setUserInfoList(getUserInfo(archivesUsers));
            bo.setPerformanceArchives(archives);
            boList.add(bo);
        }
        return DTO.ok(PageBean.of(number, size, count, boList));
    }

    /**
     * 获取详情
     *
     * @param id
     * @return
     */
    public DTO<?> archivesDetails(Long id) {
        Optional<PerformanceArchives> op = performanceArchivesRepository.findById(id);
        if (op.isPresent()) {
            PerformanceArchivesBO bo = new PerformanceArchivesBO();

            PerformanceArchives archives = op.get();
            bo.setPerformanceArchives(archives);

            // 上传人信息
            List<ArchivesUser> archivesUsers = archivesUserRepository.findByArchivesIdAndArchivesType(id, 0);
            bo.setArchivesUserList(archivesUsers);
            List<UserInfo> userInfoList = getUserInfo(archivesUsers);
            List<UserBO> userBOList = new ArrayList<>();
            if (null != userInfoList) {
                for (UserInfo userInfo : userInfoList) {
                    UserAccount userAccount = userAccountRepository.findByUserId(userInfo.getUserId());
                    UserBO userBO = new UserBO();
                    userBO.setUserInfo(userInfo);
                    userBO.setUserAccount(userAccount);
                    userBOList.add(userBO);
                }
            }
            bo.setUserBOList(userBOList);

            // 审核过程及审核人信息
            List<ArchivesProcess> processList = archivesProcessRepository.findByArchivesIdOrderByAtPresent(id);
            if (null != processList && processList.size() > 0) {
                bo.setArchivesProcessList(processList);
                bo.setArchivesProcess(processList.get(processList.size() - 1));
                List<ArchivesProcessBO> archivesProcessBOList = new ArrayList<>();
                for (ArchivesProcess archivesProcess : processList) {
                    String userId = archivesProcess.getProcessor();
                    UserInfo userInfo = userInfoRepository.findByUserId(userId);
                    UserAccount userAccount = userAccountRepository.findByUserId(userId);
                    ArchivesProcessBO archivesProcessBO = new ArchivesProcessBO();
                    archivesProcessBO.setArchivesProcess(archivesProcess);
                    archivesProcessBO.setUserInfo(userInfo);
                    archivesProcessBO.setUserAccount(userAccount);
                    archivesProcessBOList.add(archivesProcessBO);
                }
                bo.setArchivesProcessBOList(archivesProcessBOList);
            }

            // 审核过程记录
            List<ArchivesProcessRecord> recordList =
                    archivesProcessRecordRepository.findByArchivesIdOrderByTimestamp(id);
            bo.setArchivesProcessRecordList(recordList);

            if (archives.getHaveAttach()) {
                bo.setCommonResourceList(getCommonResource(id));
            }

            // 所选任务
            Optional<Task> opTask = taskRepository.findById(archives.getTaskId());
            if (opTask.isPresent()) {
                Task task = opTask.get();
                bo.setTask(task);
            }
            return DTO.ok(bo);
        }
        return DTO.error(ErrorCode.server_2_record_not_exist);
    }

    /**
     * 获取列表
     *
     * @param number
     * @param size
     * @return
     */
    public DTO<?> processPage(String processor, Boolean processed, String locationMain, String belong, String year, Integer number, Integer size) {

        locationMain = noEmpty(locationMain) ? locationMain : "%";
        belong = noEmpty(belong) ? belong : "%";
        year = noEmpty(year) ? year : "%";

        List<PerformanceArchives> archivesList =
                performanceArchivesRepository.processPage(processor, processed, locationMain, belong, year, number * size, size);
        if (null == archivesList) {
            archivesList = new ArrayList<>();
        }
        Long count = performanceArchivesRepository.processCount(processor, processed, locationMain, belong, year);
        count = null != count ? count : 0;
        List<PerformanceArchivesBO> boList = new ArrayList<>();
        for (PerformanceArchives archives : archivesList) {
            List<ArchivesUser> archivesUsers = archivesUserRepository
                    .findByArchivesIdAndArchivesType(archives.getId(), 0);

            PerformanceArchivesBO bo = new PerformanceArchivesBO();
            bo.setArchivesUserList(archivesUsers);
            bo.setUserInfoList(getUserInfo(archivesUsers));
            bo.setPerformanceArchives(archives);
            boList.add(bo);
        }
        return DTO.ok(PageBean.of(number, size, count, boList));
    }


    private List<UserInfo> getUserInfo(List<ArchivesUser> archivesUsers) {
        List<UserInfo> userInfoList = new ArrayList<>();
        if (null != archivesUsers) {
            List<String> userIdList = new ArrayList<>(archivesUsers.size());
            for (ArchivesUser archivesUser : archivesUsers) {
                userIdList.add(archivesUser.getUserId());
            }
            if (userIdList.size() > 0) {
                userInfoList = userInfoRepository.findByUserIdIn(userIdList);
            }
        }
        return userInfoList;
    }

    /**
     * 处理
     *
     * @param process
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> processHandle(ArchivesProcess process) {
        long id = process.getId();
        Optional<ArchivesProcess> optional = archivesProcessRepository.findById(id);
        if (optional.isPresent()) {
            PerformanceArchivesBO bo = new PerformanceArchivesBO();
            ArchivesProcess processDB = optional.get();

            if (!process.getPassed()) {
                Optional<PerformanceArchives> opArchives = performanceArchivesRepository.findById(processDB.getArchivesId());
                if (opArchives.isPresent()) {
                    PerformanceArchives archives = opArchives.get();
                    archives.setSubmitted(false);
                    archives.setReject(true);
                    performanceArchivesRepository.save(archives);
                    bo.setPerformanceArchives(archives);
                } else {
                    DTO.error(ErrorCode.server_2_record_not_exist);
                }
            }

            processDB.setPassed(process.getPassed());
            processDB.setProcessed(true);
            processDB.setTimestamp(System.currentTimeMillis());
            archivesProcessRepository.save(processDB);
            bo.setArchivesProcess(processDB);

            addProcessRecord(processDB);
            return DTO.ok(bo);
        }
        return DTO.error(ErrorCode.server_2_record_not_exist);
    }

    /**
     * 最终处理
     *
     * @param processIds
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> finalHandle(List<Long> processIds, String comment) {
        for (Long processId : processIds) {
            Optional<ArchivesProcess> optional = archivesProcessRepository.findById(processId);
            if (optional.isPresent()) {
                PerformanceArchivesBO bo = new PerformanceArchivesBO();
                ArchivesProcess processDB = optional.get();

                Optional<PerformanceArchives> opArchives = performanceArchivesRepository.findById(processDB.getArchivesId());
                if (opArchives.isPresent()) {
                    PerformanceArchives archives = opArchives.get();
                    archives.setFinished(true);
                    performanceArchivesRepository.save(archives);
                    bo.setPerformanceArchives(archives);

                    processDB.setProcessed(true);
                    processDB.setComment(comment);
                    archivesProcessRepository.save(processDB);
                    bo.setArchivesProcess(processDB);

                    addScore(archives.getId(), archives.getScore());
                    addProcessRecord(processDB);
                } else {
                    DTO.error(ErrorCode.server_2_record_not_exist);
                }
            } else {
                return DTO.error(ErrorCode.server_2_record_not_exist);
            }
        }
        return DTO.ok(processIds.size());
    }

    private void addProcessRecord(ArchivesProcess process) {
        ArchivesProcessRecord record = new ArchivesProcessRecord();
        record.setArchivesId(process.getArchivesId());
        record.setPassed(process.getPassed());
        record.setProcessor(process.getProcessor());
        record.setProcessorName(process.getProcessorName());
        record.setTimestamp(System.currentTimeMillis());
        archivesProcessRecordRepository.save(record);
    }

    private void addScore(Long archivesId, Integer score) {
        List<ArchivesUser> archivesUsers = archivesUserRepository.findByArchivesIdAndArchivesType(archivesId, 0);
        if (null != archivesUsers) {
            int abs = (score < 0) ? -score : score;
            boolean award = score >= 0;
            int year = LocalDate.now().getYear();

            for (ArchivesUser archivesUser : archivesUsers) {
                String userId = archivesUser.getUserId();
                UserScore userScore = userScoreRepository.findByUserIdAndYear(userId, year);
                if (null != userScore) {
                    userScore.setTotalScore(userScore.getTotalScore() + score);
                    userScore.setUsableScore(userScore.getUsableScore() + score);
                    userScoreRepository.save(userScore);

                    UserScoreRecord usr = new UserScoreRecord();
                    usr.setWay(0);
                    usr.setUserId(userId);
                    usr.setScore(abs);
                    usr.setAward(award);
                    userScoreRecordRepository.save(usr);
                }
            }
        }
    }
}
