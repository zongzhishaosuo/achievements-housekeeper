package com.anyi.achievements.service.app.phone;

import com.anyi.achievements.bo.constant.BaseConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.phone.PhoneCode;
import com.anyi.achievements.service.base.BaseUserServiceImpl;
import com.anyi.achievements.util.ObjUtils;
import com.anyi.achievements.util.qcloudsms.SmsParam;
import com.anyi.achievements.util.qcloudsms.SmsSenderBase;
import com.anyi.achievements.util.qcloudsms.result.SmsSingleResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author yanhu
 * @version 2019/6/15 下午 08:32
 */
@Slf4j
@Service
public class PhoneServiceImpl extends BaseUserServiceImpl {

    /**
     * 发送短信验证码
     *
     * @param phone
     * @return
     */
    public DTO<?> sendCode(String phone) {
        if (null == phone || phone.length() == 0) {
            return DTO.error(ErrorCode.server_1_params_can_not_empty);
        }
        String code = randomNum(4);
        SmsSingleResult result = smsSingleSender.send(buildSmsParam(phone, code));
        if (result.success()) {
            PhoneCode phoneCode = phoneCodeRepository.findByPhone(phone);
            if (null == phoneCode) {
                phoneCode = new PhoneCode();
                phoneCode.setPhone(phone);
            }
            phoneCode.setCode(code);
            phoneCode.setTimestamp(System.currentTimeMillis() / 1000);
            phoneCodeRepository.save(phoneCode);
            return DTO.ok(BaseConst.SUCCESS);
        }
        log.error(result.getErrmsg());
        return DTO.error(ErrorCode.phone_102_code_send_fail);
    }

    private SmsParam buildSmsParam(String phone, String code) {
        SmsParam ssp = new SmsParam();
        ssp.setTels(new String[]{phone});
        String[] params = {code, (phoneCodeVailTime / 60) + ""};
        ssp.setParams(params);
        ssp.setTplId(SmsSenderBase.verifyCodeTplId);
        return ssp;
    }

    /**
     * 验证短信验证码
     *
     * @param phone
     * @param code
     * @return
     */
    public DTO<?> verifyCode(String phone, String code) {
        if (ObjUtils.notEmpty(phone, code)) {
            return verifyPhoneCode(phone, code);
        }
        return DTO.error(ErrorCode.server_1_params_can_not_empty);
    }

}
