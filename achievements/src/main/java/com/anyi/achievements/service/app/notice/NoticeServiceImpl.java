package com.anyi.achievements.service.app.notice;

import com.anyi.achievements.bo.NoticeBO;
import com.anyi.achievements.bo.constant.BaseConst;
import com.anyi.achievements.bo.constant.CommonResourceConst;
import com.anyi.achievements.bo.constant.NoticeConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.PageBean;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.common.CommonResource;
import com.anyi.achievements.entity.notice.Notice;
import com.anyi.achievements.entity.notice.NoticeScope;
import com.anyi.achievements.entity.user.UserInfo;
import com.anyi.achievements.repository.api.OP;
import com.anyi.achievements.repository.api.Pre;
import com.anyi.achievements.repository.api.PreList;
import com.anyi.achievements.repository.api.SpecFactory;
import com.anyi.achievements.service.BaseServiceImpl;
import com.anyi.achievements.util.BeanUtils;
import com.anyi.achievements.util.ObjUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * 公告服务
 *
 * @author yanhu
 * @version 2019/6/9 下午 01:36
 */
@Slf4j
@Service
public class NoticeServiceImpl extends BaseServiceImpl {


    /**
     * 发布公告
     *
     * @param noticeBO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<NoticeBO> releaseNotice(NoticeBO noticeBO) {
        return addOrUpdateNoticeAndOther(noticeBO, Boolean.FALSE);
    }

    /**
     * 修改公告
     *
     * @param noticeBO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<NoticeBO> updateNotice(NoticeBO noticeBO) {
        return addOrUpdateNoticeAndOther(noticeBO, Boolean.TRUE);
    }

    /**
     * 保存或修改公告及其附件等信息
     *
     * @param noticeBO
     * @param isUpdate
     * @return
     */
    private DTO<NoticeBO> addOrUpdateNoticeAndOther(NoticeBO noticeBO, boolean isUpdate) {

        // todo 判断是否拥有发布公告权限
        DTO<NoticeBO> noticeBODTO = addOrUpdateNotice(noticeBO, isUpdate);
        if (noticeBODTO.isFail()) {
            return noticeBODTO;
        }
        noticeBO = noticeBODTO.getData();
        // 公告附件
        addNoticeResource(noticeBO);
        // 公告可见权限
        addVisibleScope(noticeBO);
        return DTO.ok(noticeBO);
    }

    /**
     * 新增或修改公告
     *
     * @param noticeBO
     * @param isUpdate
     * @return
     */
    private DTO<NoticeBO> addOrUpdateNotice(NoticeBO noticeBO, boolean isUpdate) {
        Notice notice = noticeBO.getNotice();
        if (null == notice) {
            return DTO.error(ErrorCode.server_0_params_can_not_null);
        }
        if (isUpdate) {
            long noticeId = notice.getId();
            Optional<Notice> optionalNotice = noticeRepository.findById(noticeId);
            if (optionalNotice.isPresent()) {
                Notice noticeDB = optionalNotice.get();
                BeanUtils.copyProperties(notice, noticeDB);
                notice = noticeDB;
                // 删除原有相关附件
                deleteResource(noticeId);
                noticeScopeRepository.deleteByNoticeId(noticeId);
            } else {
                return DTO.error(ErrorCode.notice_103_record_not_exist);
            }
        } else {
            notice.init();
        }

        if (started(notice)) {
            notice.setStatus(20);
        } else if (ended(notice)) {
            notice.setStatus(30);
        } else {
            notice.setStatus(10);
        }

        notice = noticeRepository.save(notice);
        noticeBO.setNotice(notice);
        return DTO.ok(noticeBO);
    }

    private boolean started(Notice notice) {
        LocalDateTime time = LocalDateTime.now();
        LocalDateTime startTime = notice.getStartTime();
        LocalDateTime endTime = notice.getEndTime();
        return null != startTime && null != endTime && !time.isBefore(startTime) && !time.isAfter(endTime);
    }

    private boolean ended(Notice notice) {
        LocalDateTime time = LocalDateTime.now();
        LocalDateTime endTime = notice.getEndTime();
        return null != endTime && time.isAfter(endTime);
    }

    /**
     * @param noticeId
     * @return
     */
    private List<CommonResource> getResourceList(Long noticeId) {
        return commonResourceRepository
                .findByCommonIdAndCommonClassAndDeleted(noticeId, CommonResourceConst.COMMON_CLASS_10_NOTICE, BaseConst.DELETED_0_NORMAL);
    }

    private void deleteResource(Long noticeId) {
        commonResourceRepository
                .deleteByCommonIdAndCommonClass(noticeId, CommonResourceConst.COMMON_CLASS_10_NOTICE);
    }

    /**
     * 保存公告附件
     *
     * @param noticeBO
     */
    private void addNoticeResource(NoticeBO noticeBO) {
        List<CommonResource> commonResourceList = noticeBO.getCommonResourceList();
        // 附件资源
        Notice notice = noticeBO.getNotice();
        if (null != commonResourceList && commonResourceList.size() > 0) {
            notice.setHaveAttach(true);
            long noticeId = notice.getId();
            for (CommonResource commonResource : commonResourceList) {
                commonResource.setCommonId(noticeId);
                commonResource.setCommonClass(CommonResourceConst.COMMON_CLASS_10_NOTICE);
            }
            commonResourceList = commonResourceRepository.saveAll(commonResourceList);
            noticeBO.setCommonResourceList(commonResourceList);
        } else {
            notice.setHaveAttach(false);
        }
    }

    /**
     * 增加公告可见范围
     *
     * @param noticeBO
     * @return
     */
    private void addVisibleScope(NoticeBO noticeBO) {
        List<NoticeScope> noticeScopeList = noticeBO.getNoticeScopeList();
        Notice notice = noticeBO.getNotice();
        long noticeId = notice.getId();
        String author = notice.getAuthor();
        if (null == noticeScopeList) {
            noticeScopeList = new ArrayList<>();
        }
        // 自己可见
        NoticeScope noticeScope = new NoticeScope();
        noticeScope.setType(NoticeConst.SCOPE_TYPE_30_USER);
        noticeScope.setUserId(author);
        noticeScopeList.add(noticeScope);
        for (NoticeScope ns : noticeScopeList) {
            ns.setNoticeId(noticeId);
        }
        noticeScopeList = noticeScopeRepository.saveAll(noticeScopeList);
        noticeBO.setNoticeScopeList(noticeScopeList);
    }

    /**
     * 删除公告
     *
     * @param ids
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> deleteNotice(List<Long> ids) {
        int count = 0;
        if (null != ids && ids.size() > 0) {
            Optional<Notice> optionalNotice;
            Notice notice;
            List<Notice> noticeList = new ArrayList<>(ids.size());
            for (Long id : ids) {
                optionalNotice = noticeRepository.findById(id);
                if (optionalNotice.isPresent()) {
                    notice = optionalNotice.get();
                    notice.setDeleted(BaseConst.DELETED_1_DELETED);
                    notice.setDeleteTime(LocalDateTime.now());
                    noticeList.add(notice);
                    count++;
                }
            }
            noticeRepository.saveAll(noticeList);
        }
        return DTO.ok(count);
    }

    /**
     * 获取公告列表
     *
     * @param userId
     * @param number
     * @param size
     * @return
     */
    public DTO<?> getNoticePage(String userId, String status, Integer number, Integer size) {
        UserInfo userInfo = userInfoRepository.findByUserId(userId);
        if (userInfo != null) {
            String deptCode = userInfo.getDeptCode();
            String unitCode = userInfo.getUnitCode();
            status = status == null || "".equals(status) ? "%" : status;
            if (ObjUtils.notEmpty(deptCode, unitCode)) {
                List<Notice> noticeList = noticeRepository.getVisibleScopePage(status, userId, deptCode, unitCode, number * size, size);
                Long total = noticeRepository.getVisibleScopeCount(status, userId, deptCode, unitCode);
                return DTO.ok(PageBean.of(number, size, total, noticeList));
            }
            return DTO.error(ErrorCode.user_100_incomplete_user_information);
        }
        return DTO.error(ErrorCode.user_5_user_not_exist);
    }

    public DTO<?> getNoticeList(String userId, String status) {
        UserInfo userInfo = userInfoRepository.findByUserId(userId);
        if (userInfo != null) {
            String deptCode = userInfo.getDeptCode();
            String unitCode = userInfo.getUnitCode();
            if (ObjUtils.notEmpty(deptCode, unitCode)) {
                status = status == null || "".equals(status) ? "%" : status;
                List<Notice> noticeList = noticeRepository
                        .getVisibleScopeList(status, userId, deptCode, unitCode);
                return DTO.ok(noticeList);
            }
            return DTO.error(ErrorCode.user_100_incomplete_user_information);
        }
        return DTO.error(ErrorCode.user_5_user_not_exist);
    }

    public DTO<?> getOwnPage(String author, Integer status, Integer number, Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC, "modifyTime");
        Pageable pageable = PageRequest.of(number, size, sort);
        PreList.Bui bui = PreList.bui()
                .add(Pre.of(OP.EQ, "author", author))
                .add(Pre.of(OP.EQ, "status", status));
        Page<Notice> page = noticeRepository.findAll(SpecFactory.where(bui.get()), pageable);
        return DTO.ok(page);
    }

    /**
     * 获取公告详情
     *
     * @param noticeId
     * @return
     */
    public DTO<NoticeBO> getNoticeDetails(Long noticeId) {
        Optional<Notice> optionalNotice = noticeRepository.findById(noticeId);
        if (optionalNotice.isPresent()) {
            Notice notice = optionalNotice.get();
            List<CommonResource> commonResourceList = getResourceList(noticeId);
            List<NoticeScope> noticeScopeList
                    = noticeScopeRepository.findByNoticeId(noticeId);
            UserInfo author = userInfoRepository.findByUserId(notice.getAuthor());
            NoticeBO noticeBO = new NoticeBO();
            noticeBO.setNotice(notice);
            noticeBO.setCommonResourceList(commonResourceList);
            noticeBO.setNoticeScopeList(noticeScopeList);
            noticeBO.setAuthor(author);
            return DTO.ok(noticeBO);
        }
        return DTO.error(ErrorCode.notice_103_record_not_exist);
    }

}
