package com.anyi.achievements.service.app.user;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.entity.user.UserExperience;
import com.anyi.achievements.service.BaseServiceImpl;
import com.anyi.achievements.util.BeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author yh
 * @version 2019/7/11 上午 11:31
 */
@Slf4j
@Service
public class ExperienceServiceImpl extends BaseServiceImpl {

    public DTO<?> save(UserExperience experience) {
        UserExperience experienceDB = userExperienceRepository.findByUserId(experience.getUserId());
        if (null != experienceDB) {
            BeanUtils.copyProperties(experience, experienceDB);
            experience = experienceDB;
        }
        experience = userExperienceRepository.save(experience);
        return DTO.ok(experience);
    }

    public DTO<?> get(String userId) {
        UserExperience userExperience = userExperienceRepository.findByUserId(userId);
        return DTO.ok(userExperience);
    }

}
