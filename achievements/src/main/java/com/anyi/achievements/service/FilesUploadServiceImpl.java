package com.anyi.achievements.service;

import com.anyi.achievements.bo.constant.CommonResourceConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.common.CommonResource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author yanhu
 * @version 2019/6/13 上午 11:19
 */
@Slf4j
@Service
public class FilesUploadServiceImpl extends BaseServiceImpl {

    @Autowired
    private AsyncServiceImpl asyncService;

    private static final String POINT = ".";

    private static Map<String, Integer> fileTypeMap;

    static {
        fileTypeMap = new HashMap<>();
        fileTypeMap.put("image", CommonResourceConst.TYPE_10_IMAGE);
        fileTypeMap.put("audio", CommonResourceConst.TYPE_20_AUDIO);
        fileTypeMap.put("video", CommonResourceConst.TYPE_30_VIDEO);
        fileTypeMap.put("application", CommonResourceConst.TYPE_40_APPLICATION);
    }

    public DTO<?> upload(MultipartFile file, boolean headImg) {
        if (null == file || file.isEmpty()) {
            return DTO.error(ErrorCode.file_101_upload_000_file_can_not_empty);
        }
        if (file.getSize() > uploadFileMaxSize) {
            return DTO.error(ErrorCode.file_101_upload_001_file_too_large);
        }
        String type = getFileType(file.getContentType());
        File path = getFilePath(uploadRootPath, type);
        String suffix = getFilenameSuffix(file.getOriginalFilename());
        String uniqueId = getUUID() + System.currentTimeMillis();
        String newFilename = uniqueId + suffix;
        try {
            File newFile = new File(path, newFilename);
            file.transferTo(newFile);
            String url = serverHost +
                    fileRequestPath +
                    type + "/" +
                    newFilename;
            CommonResource commonResource = getCommonResource(uniqueId, type, url);
            asyncService.asyncSaveUploadedFileInfo(newFile.getAbsolutePath(), uniqueId, newFilename, headImg);
            return DTO.ok(commonResource);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("文件上传异常");
        }
        return DTO.error(ErrorCode.file_101_upload_002_upload_fail);
    }

    private String getFileType(String mime) {
        String path = null;
        Iterator<String> iterator = fileTypeMap.keySet().iterator();
        String key;
        String regex;
        boolean matches;
        while (iterator.hasNext()) {
            key = iterator.next();
            regex = "^(" + key + "/)[A-Za-z0-9_+-.]+$";
            matches = Pattern.compile(regex).matcher(mime).matches();
            if (matches) {
                path = key;
            }
        }
        if (null == path) {
            path = "undefined";
        }
        return path;
    }

    private File getFilePath(String root, String type) {
        File filePath = new File(root + File.separator + type);
        if (!filePath.exists()) {
            boolean mkdirs = filePath.mkdirs();
            log.info("创建文件上传目录：" + mkdirs);
        }
        return filePath;
    }

    /**
     * 获取文件新名称
     *
     * @param originalFilename
     * @return
     */
    private String getFilenameSuffix(String originalFilename) {
        String suffix = "";
        if (null != originalFilename && originalFilename.contains(POINT)) {
            suffix = originalFilename.substring(originalFilename.lastIndexOf(POINT));
        }
        return suffix;
    }

    /**
     * 封装结果集
     *
     * @param uniqueId
     * @param type
     * @param url
     * @return
     */
    private CommonResource getCommonResource(String uniqueId, String type, String url) {
        CommonResource commonResource = new CommonResource();
        commonResource.setUniqueId(uniqueId);
        Integer typeInt = fileTypeMap.get(type);
        if (null == typeInt) {
            typeInt = CommonResourceConst.TYPE_50_UNDERFINED;
        }
        commonResource.setType(typeInt);
        commonResource.setUrl(url);
        return commonResource;
    }
}
