package com.anyi.achievements.service.app.user;

import com.anyi.achievements.bo.UserBO;
import com.anyi.achievements.bo.constant.BaseConst;
import com.anyi.achievements.bo.constant.UserConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.PageBean;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.score.UserScore;
import com.anyi.achievements.entity.user.UserAccount;
import com.anyi.achievements.entity.user.UserAuth;
import com.anyi.achievements.entity.user.UserInfo;
import com.anyi.achievements.repository.api.OP;
import com.anyi.achievements.repository.api.Pre;
import com.anyi.achievements.repository.api.PreList;
import com.anyi.achievements.repository.api.SpecFactory;
import com.anyi.achievements.service.base.BaseUserServiceImpl;
import com.anyi.achievements.util.BeanUtils;
import com.anyi.achievements.util.ObjUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 人员操作
 *
 * @author yanhu
 * @version 2019/6/8 下午 07:34
 */
@Slf4j
@Service
public class UserServiceImpl extends BaseUserServiceImpl {

    /**
     * 添加新人员
     *
     * @param userBO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<UserBO> register(UserBO userBO) {
        UserInfo userInfo = userBO.getUserInfo();
        if (null != userInfo) {
            String userId = getUUID();
            // 添加人员基本信息
            DTO<UserBO> userBODTO = addUserInfo(userBO, userId);
            if (userBODTO.isFail()) {
                return userBODTO;
            }
            // 添加人员权限、账号、积分信息，保证事务操作
            addUserAuth(userBO, userId);
            addUserAccount(userBO, userId);
            addUserScore(userBO, userId);
            return DTO.ok(userBO);
        }
        return DTO.error(ErrorCode.server_0_params_can_not_null);
    }

    /**
     * 多种登陆方式
     *
     * @param way
     * @param phone
     * @param password
     * @param code
     * @return
     */
    public DTO<UserBO> login(int way, String phone, String password, String code) {
        switch (way) {
            case UserConst.LOGIN_WAY_PHONE_PASSWORD:
                return loginByPhoneAndPassword(phone, password);
            case UserConst.LOGIN_WAY_PHONE_CODE:
                return loginByPhoneAndCode(phone, code);
            default:
                return DTO.error(ErrorCode.server_3_params_error);
        }
    }

    /**
     * 手机号+密码登录
     *
     * @param phone
     * @param password
     * @return
     */
    private DTO<UserBO> loginByPhoneAndPassword(String phone, String password) {
        if (ObjUtils.notEmpty(phone, password)) {
            UserInfo userInfo = userInfoRepository.findByPhoneAndDeleted(phone, BaseConst.DELETED_0_NORMAL);
            if (null != userInfo) {
                UserAccount userAccount = userAccountRepository.findByUserId(userInfo.getUserId());
                if (userAccount.getPassword().toUpperCase().equals(password.toUpperCase())) {
                    return getUserAllInfo(userInfo);
                }
                return DTO.error(ErrorCode.user_login_50_password_error);
            }
            return DTO.error(ErrorCode.user_login_51_no_user_by_phone);
        }
        return DTO.error(ErrorCode.server_1_params_can_not_empty);
    }


    /**
     * 手机号+短信验证码登录
     *
     * @param phone
     * @param code
     * @return
     */
    private DTO<UserBO> loginByPhoneAndCode(String phone, String code) {
        if (ObjUtils.notEmpty(phone, code)) {
            DTO<UserBO> dto = verifyPhoneCode(phone, code);
            if (dto.isFail()) {
                return dto;
            }
            UserInfo userInfo = userInfoRepository.findByPhoneAndDeleted(phone, BaseConst.DELETED_0_NORMAL);
            if (null != userInfo) {
                return getUserAllInfo(userInfo);
            }
            return DTO.error(ErrorCode.user_login_51_no_user_by_phone);
        }
        return DTO.error(ErrorCode.server_0_params_can_not_null);
    }

    /**
     * 获取人员所有信息
     *
     * @param userInfo
     * @return
     */
    private DTO<UserBO> getUserAllInfo(UserInfo userInfo) {
        if (null != userInfo) {
            String userId = userInfo.getUserId();
            UserAuth userAuth = userAuthRepository.findByUserId(userId);
            UserAccount userAccount = userAccountRepository.findByUserId(userId);
            int year = LocalDate.now().getYear();
            UserScore userScore = userScoreRepository.findByUserIdAndYear(userId, year);
            UserBO userBO = new UserBO();
            userBO.setUserInfo(userInfo);
            userBO.setUserAuth(userAuth);
            userBO.setUserScore(userScore);
            userBO.setUserAccount(userAccount);
            return DTO.ok(userBO);
        }
        return DTO.error(ErrorCode.user_5_user_not_exist);
    }


    /**
     * 更新人员信息
     *
     * @param userBO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<UserBO> updateUser(UserBO userBO) {
        UserInfo userInfo = userBO.getUserInfo();
        UserAuth userAuth = userBO.getUserAuth();
        UserAccount userAccount = userBO.getUserAccount();
        // 修改人员基本信息
        if (null != userInfo) {
            UserInfo userInfoDB = userInfoRepository.findByUserId(userInfo.getUserId());
            if (null != userInfoDB) {
                BeanUtils.copyProperties(userInfo, userInfoDB);
                userInfoRepository.save(userInfoDB);
                userBO.setUserInfo(userInfoDB);
            }
        }
        // 修改人员权限信息
        if (null != userAuth) {
            UserAuth userAuthDB = userAuthRepository.findByUserId(userAuth.getUserId());
            if (null != userAuthDB) {
                BeanUtils.copyProperties(userAuth, userAuthDB);
                userAuthRepository.save(userAuth);
                userBO.setUserAuth(userAuthDB);
            }
        }
        // 修改人员账号信息，不允许修改密码
        if (null != userAccount) {
            UserAccount userAccountDB = userAccountRepository.findByUserId(userAccount.getUserId());
            if (null != userAccountDB) {
                userAccount.setPassword(null);
                BeanUtils.copyProperties(userAccount, userAccountDB);
                userAccountRepository.save(userAccountDB);
                userBO.setUserAccount(userAccountDB);
            }
        }
        return DTO.ok(userBO);
    }

    /**
     * 获取单位内人员信息
     *
     * @param unitCode
     * @param number
     * @param size
     * @return
     */
    public DTO<?>
    getUserInfoPage(String unitCode, String deptCode, String roleCode,
                    Integer number, Integer size) {
        Pageable pageable = PageRequest.of(number, size, Sort.by("sort"));
        List<Pre> preList = PreList.bui()
                .add(Pre.get(OP.EQ, "deleted", 0))
                .add(Pre.get(OP.EQ, "completeInfo", true))
                .add(Pre.of(OP.EQ, "unitCode", unitCode))
                .add(Pre.of(OP.EQ, "deptCode", deptCode))
                .add(Pre.of(OP.EQ, "roleCode", roleCode))
                .get();
        Page<UserInfo> userInfoPage = userInfoRepository.findAll(SpecFactory.where(preList), pageable);
        List<UserBO> boList = new ArrayList<>(size);
        List<UserInfo> infoList = userInfoPage.getContent();
        for (UserInfo info : infoList) {
            UserAccount account = userAccountRepository.findByUserId(info.getUserId());
            UserBO bo = new UserBO();
            bo.setUserInfo(info);
            bo.setUserAccount(account);
            boList.add(bo);
        }
        return DTO.ok(PageBean.of(number, size, userInfoPage.getTotalElements(), boList));
    }

    /**
     * 通过手机号获取人员信息
     *
     * @param phone
     * @return
     */
    public DTO<?> getByPhone(String phone) {
        UserBO bo = new UserBO();
        UserInfo userInfo = userInfoRepository.findByPhoneAndDeleted(phone, BaseConst.DELETED_0_NORMAL);
        if (null != userInfo) {
            String userId = userInfo.getUserId();
            UserAuth userAuth = userAuthRepository.findByUserId(userId);
            UserAccount userAccount = userAccountRepository.findByUserId(userId);
            int year = LocalDate.now().getYear();
            UserScore userScore = userScoreRepository.findByUserIdAndYear(userId, year);
            bo.setUserInfo(userInfo);
            bo.setUserAuth(userAuth);
            bo.setUserAccount(userAccount);
            bo.setUserScore(userScore);
        }
        return DTO.ok(bo);
    }

}
