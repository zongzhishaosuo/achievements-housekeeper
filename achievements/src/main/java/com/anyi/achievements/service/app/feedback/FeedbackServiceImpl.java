package com.anyi.achievements.service.app.feedback;

import com.anyi.achievements.bo.FeedbackBO;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.entity.common.CommonResource;
import com.anyi.achievements.entity.feedback.Feedback;
import com.anyi.achievements.service.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yh
 * @version 2019/7/15 上午 11:57
 */
@Slf4j
@Service
public class FeedbackServiceImpl extends BaseServiceImpl {

    public DTO<?> add(FeedbackBO bo) {
        Feedback feedback = bo.getFeedback();
        feedback = feedbackRepository.save(feedback);
        List<CommonResource> resources = bo.getCommonResourceList();
        if (null != resources && resources.size() > 0) {
            for (CommonResource resource : resources) {
                resource.setCommonId(feedback.getId());
                resource.setCommonClass(120);
            }
            resources = commonResourceRepository.saveAll(resources);
            bo.setCommonResourceList(resources);
            feedback.setHaveAttach(true);
        } else {
            feedback.init();
        }
        return DTO.ok(bo);
    }

}
