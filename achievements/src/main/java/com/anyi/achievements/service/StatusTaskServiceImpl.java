package com.anyi.achievements.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * @author yanhu
 * @version 2019/7/8 下午 09:52
 */
@Slf4j
@Service
public class StatusTaskServiceImpl extends BaseServiceImpl {

    
    @Scheduled(cron = "0 0 0 * * ?")
    public void noticeStatus() {
        LocalDateTime time = LocalDateTime.now();
        log.info("设置公告状态 time:{}", time);
        noticeRepository.setStart(time);
        noticeRepository.setEnd(time);
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void taskStatus() {
        LocalDateTime time = LocalDateTime.now();
        log.info("设置任务状态 time:{}", time);
        taskRepository.setStart(time);
        taskRepository.setEnd(time);
    }

}
