package com.anyi.achievements.service.app.task;

import com.anyi.achievements.bo.TaskBO;
import com.anyi.achievements.bo.constant.BaseConst;
import com.anyi.achievements.bo.constant.TaskConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.PageBean;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.task.Task;
import com.anyi.achievements.entity.task.TaskScope;
import com.anyi.achievements.entity.user.UserInfo;
import com.anyi.achievements.repository.api.OP;
import com.anyi.achievements.repository.api.Pre;
import com.anyi.achievements.repository.api.PreList;
import com.anyi.achievements.repository.api.SpecFactory;
import com.anyi.achievements.service.BaseServiceImpl;
import com.anyi.achievements.util.BeanUtils;
import com.anyi.achievements.util.ObjUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author yanhu
 * @version 2019/6/16 下午 12:55
 */
@Slf4j
@Service
public class TaskServiceImpl extends BaseServiceImpl {

    /**
     * 发布任务
     *
     * @param taskBO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> releaseTask(TaskBO taskBO) {
        DTO<TaskBO> taskBODTO = addOrUpdateTask(taskBO, Boolean.FALSE);
        if (taskBODTO.isFail()) {
            return taskBODTO;
        }
        addTaskScope(taskBO);
        return DTO.ok(taskBO);
    }

    /**
     * 更新任务
     *
     * @param taskBO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> updateTask(TaskBO taskBO) {
        DTO<TaskBO> taskBODTO = addOrUpdateTask(taskBO, Boolean.TRUE);
        if (taskBODTO.isFail()) {
            return taskBODTO;
        }
        addTaskScope(taskBO);
        return DTO.ok(taskBO);
    }

    /**
     * 新增或修改任务
     *
     * @param taskBO
     * @param isUpdate
     * @return
     */
    private DTO<TaskBO> addOrUpdateTask(TaskBO taskBO, boolean isUpdate) {
        Task task = taskBO.getTask();
        if (null == task) {
            return DTO.error(ErrorCode.server_0_params_can_not_null);
        }
        if (isUpdate) {
            long taskId = task.getId();
            Optional<Task> optionalTask = taskRepository.findById(taskId);
            if (optionalTask.isPresent()) {
                Task taskDB = optionalTask.get();
                if (taskDB.getSelected()) {
                    return DTO.error(ErrorCode.task_104_task_already_selected);
                }
                BeanUtils.copyProperties(task, taskDB);
                task = taskDB;
                taskScopeRepository.deleteByTaskId(taskId);
            } else {
                return DTO.error(ErrorCode.task_104_record_not_exist);
            }
        } else {
            task.init();
        }

        if (started(task)) {
            task.setStatus(20);
        } else if (ended(task)) {
            task.setStatus(30);
        } else {
            task.setStatus(10);
        }

        task = taskRepository.save(task);
        taskBO.setTask(task);
        return DTO.ok(taskBO);
    }

    private boolean started(Task task) {
        LocalDateTime time = LocalDateTime.now();
        LocalDateTime startTime = task.getStartTime();
        LocalDateTime endTime = task.getEndTime();
        return null != startTime && null != endTime && !time.isBefore(startTime) && !time.isAfter(endTime);
    }

    private boolean ended(Task task) {
        LocalDateTime time = LocalDateTime.now();
        LocalDateTime endTime = task.getEndTime();
        return null != endTime && time.isAfter(endTime);
    }

    /**
     * 增加任务可见范围
     *
     * @param taskBO
     * @return
     */
    private void addTaskScope(TaskBO taskBO) {
        List<TaskScope> taskScopeList = taskBO.getTaskScopeList();
        if (null == taskScopeList) {
            taskScopeList = new ArrayList<>();
        }
        Task task = taskBO.getTask();
        long taskId = task.getId();
        String userId = task.getAuthor();
        TaskScope taskScope = new TaskScope();
        taskScope.setUserId(userId);
        taskScope.setType(TaskConst.SCOPE_TYPE_30_USER);
        taskScopeList.add(taskScope);
        for (TaskScope scope : taskScopeList) {
            scope.setTaskId(taskId);
        }
        taskScopeList = taskScopeRepository.saveAll(taskScopeList);
        taskBO.setTaskScopeList(taskScopeList);
    }

    /**
     * 删除任务
     *
     * @param ids
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> deleteTask(List<Long> ids) {
        int count = 0;
        if (null != ids && ids.size() > 0) {
            List<Task> taskList = new ArrayList<>(ids.size());
            Task task;
            Optional<Task> optionalTask;
            for (Long id : ids) {
                optionalTask = taskRepository.findById(id);
                if (optionalTask.isPresent()) {
                    task = optionalTask.get();
                    if (task.getSelected()) {
                        return DTO.error(ErrorCode.task_104_task_already_selected);
                    }
                    task.setDeleted(BaseConst.DELETED_1_DELETED);
                    task.setDeleteTime(LocalDateTime.now());
                    taskList.add(task);
                    count++;
                }
            }
            taskRepository.saveAll(taskList);
        }
        return DTO.ok(count);
    }

    /**
     * 获取任务列表
     *
     * @param userId
     * @param number
     * @param size
     * @return
     */
    public DTO<?> page(String userId, String status, Integer number, Integer size) {
        UserInfo userInfo = userInfoRepository.findByUserId(userId);
        if (userInfo != null) {
            String deptCode = userInfo.getDeptCode();
            String unitCode = userInfo.getUnitCode();
            status = status == null || "".equals(status) ? "%" : status;
            if (ObjUtils.notEmpty(deptCode, unitCode)) {
                List<Task> taskList = taskRepository
                        .getVisibleScopePage(userId, deptCode, unitCode, number * size, size, status);
                Long total = taskRepository.getVisibleScopeCount(userId, deptCode, unitCode, status);
                return DTO.ok(PageBean.of(number, size, total, taskList));
            }
            return DTO.error(ErrorCode.user_100_incomplete_user_information);
        }
        return DTO.error(ErrorCode.user_5_user_not_exist);
    }

    public DTO<?> list(String userId, String status) {
        UserInfo userInfo = userInfoRepository.findByUserId(userId);
        if (userInfo != null) {
            String deptCode = userInfo.getDeptCode();
            String unitCode = userInfo.getUnitCode();
            if (ObjUtils.notEmpty(deptCode, unitCode)) {
                status = status == null || "".equals(status) ? "%" : status;
                List<Task> taskList = taskRepository
                        .getVisibleScopeList(userId, deptCode, unitCode, status);
                return DTO.ok(taskList);
            }
            return DTO.error(ErrorCode.user_100_incomplete_user_information);
        }
        return DTO.error(ErrorCode.user_5_user_not_exist);
    }

    public DTO<?> ownPage(String author, Integer status, Integer number, Integer size) {
        Sort sort = Sort.by(Sort.Direction.DESC, "modifyTime");
        Pageable pageable = PageRequest.of(number, size, sort);
        List<Pre> pres = PreList.bui()
                .add(Pre.of(OP.EQ, "author", author))
                .add(Pre.of(OP.EQ, "status", status))
                .get();
        Page<Task> page = taskRepository.findAll(SpecFactory.where(pres), pageable);
        return DTO.ok(page);
    }

    /**
     * 获取任务详情
     *
     * @param taskId
     * @return
     */
    public DTO<TaskBO> getTaskDetails(Long taskId) {
        Optional<Task> optionalTask = taskRepository.findById(taskId);
        if (optionalTask.isPresent()) {
            Task task = optionalTask.get();
            List<TaskScope> taskScopeList
                    = taskScopeRepository.findByTaskId(taskId);
            UserInfo author = userInfoRepository.findByUserId(task.getAuthor());
            TaskBO taskBO = new TaskBO();
            taskBO.setTask(task);
            taskBO.setTaskScopeList(taskScopeList);
            taskBO.setAuthor(author);
            return DTO.ok(taskBO);
        }
        return DTO.error(ErrorCode.notice_103_record_not_exist);
    }
}
