package com.anyi.achievements.service;

import com.anyi.achievements.entity.common.ResourceFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yanhu
 * @version 2019/6/19 下午 06:48
 */
@Slf4j
@Service
public class ClearFilesTaskServiceImpl extends BaseServiceImpl {

//    @Scheduled(cron = "0 0 2 * * ?")
    public void clearUselessFiles() {
        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss SSS"));
        log.info("{}: clean useless files", time);
        List<ResourceFileInfo> fileInfoList = resourceFileInfoRepository.uselessFiles();
        if (null != fileInfoList && fileInfoList.size() > 0) {
            List<ResourceFileInfo> deletedFiles = new ArrayList<>(fileInfoList.size());
            for (ResourceFileInfo resourceFileInfo : fileInfoList) {
                String path = resourceFileInfo.getPath();
                if (path != null) {
                    boolean de = deleteFiles(new File(path));
                    log.info("删除未使用文件，结果：{}\t文件：{}", de, path);
                    if (de) {
                        deletedFiles.add(resourceFileInfo);
                    }
                }
            }
            if (deletedFiles.size() > 0) {
                resourceFileInfoRepository.deleteAll(deletedFiles);
            }
        }
    }

    private boolean deleteFiles(File file) {
        boolean de = false;
        if (file.exists() && file.isFile()) {
            int n = 0;
            de = file.delete();
            while (!de) {
                de = file.delete();
                n++;
                if (n > 3) {
                    break;
                }
            }
        }
        return de;
    }

}
