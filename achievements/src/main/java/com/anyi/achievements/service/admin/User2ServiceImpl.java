package com.anyi.achievements.service.admin;

import com.anyi.achievements.bo.UserAddBO;
import com.anyi.achievements.bo.UserBO;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.entity.user.UserAccount;
import com.anyi.achievements.entity.user.UserAuth;
import com.anyi.achievements.entity.user.UserInfo;
import com.anyi.achievements.repository.api.OP;
import com.anyi.achievements.repository.api.Pre;
import com.anyi.achievements.repository.api.PreList;
import com.anyi.achievements.repository.api.SpecFactory;
import com.anyi.achievements.service.base.BaseUserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yh
 * @version 2019/7/23 下午 06:01
 */
@Slf4j
@Service
public class User2ServiceImpl extends BaseUserServiceImpl {

    public DTO<?>
    page(String unitCode, String deptCode, String role,
         String name, String idNumber, String phone,
         Integer number, Integer size) {
        Sort sort = Sort.by("sort");
        Pageable pageable = PageRequest.of(number, size, sort);
        List<Pre> ps = PreList.bui()
                .add(Pre.of(OP.EQ, "unitCode", unitCode))
                .add(Pre.of(OP.EQ, "deptCode", deptCode))
                .add(Pre.of(OP.EQ, "role", role))
                .add(Pre.of(OP.LIKE_A, "name", name))
                .add(Pre.of(OP.LIKE_A, "idNumber", idNumber))
                .add(Pre.of(OP.LIKE_A, "phone", phone))
                .get();
        Page<UserInfo> infoPage = userInfoRepository.findAll(SpecFactory.where(ps), pageable);
        return DTO.ok(infoPage);
    }

    /**
     * 获取人员详情
     *
     * @param userId
     * @return
     */
    public DTO<?> details(String userId) {
        UserBO userBO = new UserBO();
        UserInfo info = userInfoRepository.findByUserId(userId);
        UserAccount account = userAccountRepository.findByUserId(userId);
        UserAuth auth = userAuthRepository.findByUserId(userId);
        userBO.setUserInfo(info);
        userBO.setUserAccount(account);
        userBO.setUserAuth(auth);
        return DTO.ok(userBO);
    }

    /**
     * 添加成员
     *
     * @param userBO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<?> add(UserBO userBO) {
        String userId = getUUID();
        // 添加人员基本信息
        DTO<UserBO> userBODTO = addUserInfo(userBO, userId);
        if (userBODTO.isFail()) {
            return userBODTO;
        }
        // 添加人员权限、账号、积分信息，保证事务操作
        addUserAuth(userBO, userId);
        addUserAccount(userBO, userId);
        addUserScore(userBO, userId);
        return DTO.ok(userBO);
    }

    @Transactional(rollbackFor = Exception.class)
    public DTO<?> addBatch(List<UserBO> bos) {
        UserAddBO uab = new UserAddBO();
        if (null != bos && bos.size() > 0) {
            List<DTO> okList = new ArrayList<>();
            List<DTO> failList = new ArrayList<>();
            for (UserBO bo : bos) {
                DTO dto = add(bo);
                if (dto.isSuccess()) {
                    okList.add(dto);
                } else {
                    failList.add(dto);
                }
            }
            uab.setOkUserBOList(okList);
            uab.setFailUserBOList(failList);
        }
        return DTO.ok(uab);
    }
}
