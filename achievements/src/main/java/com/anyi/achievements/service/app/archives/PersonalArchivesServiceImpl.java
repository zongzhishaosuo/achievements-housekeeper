package com.anyi.achievements.service.app.archives;

import com.anyi.achievements.bo.PersonalArchivesBO;
import com.anyi.achievements.bo.constant.BaseConst;
import com.anyi.achievements.bo.constant.CommonResourceConst;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.PageBean;
import com.anyi.achievements.dto.error.ErrorCode;
import com.anyi.achievements.entity.archives.personal.PersonalArchives;
import com.anyi.achievements.entity.archives.personal.PersonalArchivesProcess;
import com.anyi.achievements.entity.common.CommonResource;
import com.anyi.achievements.entity.user.UserInfo;
import com.anyi.achievements.repository.api.OP;
import com.anyi.achievements.repository.api.Pre;
import com.anyi.achievements.repository.api.PreList;
import com.anyi.achievements.repository.api.SpecFactory;
import com.anyi.achievements.util.BeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author yanhu
 * @version 2019/6/19 下午 09:51
 */
@Slf4j
@Service
public class PersonalArchivesServiceImpl extends ArchivesServiceImpl {


    /**
     * 新增
     *
     * @param archivesBO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<PersonalArchivesBO> add(PersonalArchivesBO archivesBO) {
        DTO<PersonalArchivesBO> archivesBODTO = saveArchives(archivesBO, Boolean.FALSE);
        if (archivesBODTO.isFail()) {
            return archivesBODTO;
        }
        return saveResourceList(archivesBO);
    }

    /**
     * 修改
     *
     * @param archivesBO
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public DTO<PersonalArchivesBO> modify(PersonalArchivesBO archivesBO) {
        DTO<PersonalArchivesBO> archivesBODTO = saveArchives(archivesBO, Boolean.TRUE);
        if (archivesBODTO.isFail()) {
            return archivesBODTO;
        }
        return saveResourceList(archivesBO);
    }

    /**
     * @param archivesBO
     * @param modify
     * @return
     */
    private DTO<PersonalArchivesBO> saveArchives(PersonalArchivesBO archivesBO, boolean modify) {
        PersonalArchives archives = archivesBO.getPersonalArchives();
        if (null != archives) {
            if (modify) {
                Optional<PersonalArchives> optional = personalArchivesRepository.findById(archives.getId());
                if (optional.isPresent()) {
                    PersonalArchives archivesDB = optional.get();
                    if (archivesDB.getSubmitted()) {
                        if (!archivesDB.getProcessed()) {
                            return DTO.error(ErrorCode.PERSONAL_ARCHIVES_107_SUBMITTED_CAN_NOT_MODIFY);
                        }
                        if (archivesDB.getPassed()) {
                            return DTO.error(ErrorCode.PERSONAL_ARCHIVES_107_PASSED_CAN_NOT_MODIFY);
                        }
                    }
                    BeanUtils.copyProperties(archives, archivesDB);
                    archives = archivesDB;
                    // 删除附件记录
                    deleteResource(archives.getId());
                } else {
                    return DTO.error(ErrorCode.server_2_record_not_exist);
                }
            } else {
                archives.init();
            }
            // 设置任务为已选择
            selectTask(archives.getTaskId());

            archives = personalArchivesRepository.save(archives);
            archivesBO.setPersonalArchives(archives);
            return DTO.ok(archivesBO);
        }
        return DTO.error(ErrorCode.PERSONAL_ARCHIVES_107_RECORD_NOT_EXIST);
    }

    /**
     * @param archivesBO
     * @return
     */
    private DTO<PersonalArchivesBO> saveResourceList(PersonalArchivesBO archivesBO) {
        List<CommonResource> commonResourceList = archivesBO.getCommonResourceList();
        if (null != commonResourceList && commonResourceList.size() > 0) {
            long archivesId = archivesBO.getPersonalArchives().getId();
            for (CommonResource commonResource : commonResourceList) {
                commonResource.setCommonId(archivesId);
                commonResource.setCommonClass(CommonResourceConst.TYPE_40_APPLICATION);
            }
            commonResourceList = commonResourceRepository.saveAll(commonResourceList);
            archivesBO.setCommonResourceList(commonResourceList);
        }
        return DTO.ok(archivesBO);
    }

    /**
     * @param archivesId
     * @return
     */
    private List<CommonResource> getResourceList(Long archivesId) {
        return commonResourceRepository
                .findByCommonIdAndCommonClassAndDeleted(archivesId, CommonResourceConst.COMMON_CLASS_40_PERSONAL_ARCHIVES, BaseConst.DELETED_0_NORMAL);
    }

    private void deleteResource(Long archivesId) {
        commonResourceRepository.deleteByCommonIdAndCommonClass(archivesId, CommonResourceConst.COMMON_CLASS_40_PERSONAL_ARCHIVES);
    }

    /**
     * 提交
     *
     * @param id
     * @return
     */
    public DTO<?> submit(Long id) {
        Optional<PersonalArchives> optional = personalArchivesRepository.findById(id);
        if (optional.isPresent()) {
            PersonalArchives archives = optional.get();
            archives.setSubmitted(true);
            archives.setProcessed(false);
            archives = personalArchivesRepository.save(archives);
            return DTO.ok(archives);
        }
        return DTO.error(ErrorCode.PERSONAL_ARCHIVES_107_RECORD_NOT_EXIST);
    }

    /**
     * @param userId
     * @return
     */
    public DTO<List<PersonalArchivesBO>> getOwnArchives(String userId) {
        List<PersonalArchives> archivesList = personalArchivesRepository
                .findByUserIdAndDeleted(userId, BaseConst.DELETED_0_NORMAL);
        List<PersonalArchivesBO> archivesBOList = new ArrayList<>();
        if (null != archivesList && archivesList.size() > 0) {
            for (PersonalArchives personalArchives : archivesList) {
                archivesBOList.add(getArchivesBO(personalArchives));
            }
        }
        return DTO.ok(archivesBOList);
    }

    public DTO<?> getById(Long id) {
        Optional<PersonalArchives> optional = personalArchivesRepository.findById(id);
        if (optional.isPresent()) {
            PersonalArchives archives = optional.get();
            UserInfo userInfo = userInfoRepository.findByUserId(archives.getUserId());
            List<CommonResource> resourceList = getResourceList(id);
            List<PersonalArchivesProcess> processList = personalArchivesProcessRepository.findByArchivesId(id);
            PersonalArchivesBO archivesBO = new PersonalArchivesBO();
            archivesBO.setUserInfo(userInfo);
            archivesBO.setPersonalArchives(archives);
            archivesBO.setCommonResourceList(resourceList);
            archivesBO.setPersonalArchivesProcessList(processList);
            return DTO.ok(archivesBO);
        }
        return DTO.error(ErrorCode.server_2_record_not_exist);
    }

    /**
     * @param archives
     * @return
     */
    private PersonalArchivesBO getArchivesBO(PersonalArchives archives) {
        long archivesId = archives.getId();
        UserInfo userInfo = userInfoRepository.findByUserId(archives.getUserId());
        List<CommonResource> resourceList = getResourceList(archivesId);
        List<PersonalArchivesProcess> processList = personalArchivesProcessRepository.findByArchivesId(archivesId);
        PersonalArchivesBO archivesBO = new PersonalArchivesBO();
        archivesBO.setUserInfo(userInfo);
        archivesBO.setPersonalArchives(archives);
        archivesBO.setCommonResourceList(resourceList);
        archivesBO.setPersonalArchivesProcessList(processList);
        return archivesBO;
    }

    /**
     * 获取已提交
     *
     * @param processor
     * @param processed
     * @param passed
     * @param number
     * @param size
     * @return
     */
    public DTO<?> getSubmittedPage(String processor, Boolean processed, Boolean passed, Integer number, Integer size) {
        Pageable pageable = PageRequest.of(number, size, Sort.by(Sort.Direction.DESC, "modifyTime"));
        List<Pre> pres = PreList.bui()
                .add(Pre.get(OP.EQ, "processor", processor))
                .add(Pre.get(OP.EQ, "submitted", true))
                .add(Pre.of(OP.EQ, "processed", processed))
                .add(Pre.of(OP.EQ, "passed", passed))
                .get();
        Page<PersonalArchives> archivesPage = personalArchivesRepository.findAll(SpecFactory.where(pres), pageable);
        List<PersonalArchives> archivesList = archivesPage.getContent();
        List<PersonalArchivesBO> archivesBOList = new ArrayList<>(size);
        if (archivesList.size() > 0) {
            for (PersonalArchives archives : archivesList) {
                PersonalArchivesBO archivesBO = getArchivesBO(archives);
                UserInfo userInfo = userInfoRepository.findByUserId(archives.getUserId());
                archivesBO.setUserInfo(userInfo);
                archivesBOList.add(archivesBO);
            }
        }
        return DTO.ok(PageBean.of(number, size, archivesPage.getTotalElements(), archivesBOList));
    }

    /**
     * 处理
     *
     * @param archivesId
     * @param passed
     * @return
     */
    public DTO<?> process(Long archivesId, Boolean passed, String remark) {
        Optional<PersonalArchives> optional = personalArchivesRepository.findById(archivesId);
        if (optional.isPresent()) {
            PersonalArchives archives = optional.get();
            if (!archives.getPassed()) {
                archives.setPassed(passed);
                archives.setProcessed(true);
                archives = personalArchivesRepository.save(archives);
                saveArchivesProcess(archives, remark);
            }
            return DTO.ok(archives);
        }
        return DTO.error(ErrorCode.server_2_record_not_exist);
    }

    /**
     * 处理记录
     *
     * @param archives
     * @return
     */
    private void saveArchivesProcess(PersonalArchives archives, String remark) {
        PersonalArchivesProcess process = new PersonalArchivesProcess();
        process.setArchivesId(archives.getId());
        process.setPassed(archives.getPassed());
        process.setProcessor(archives.getProcessor());
        process.setProcessorName(archives.getProcessorName());
        process.setRemark(remark);
        personalArchivesProcessRepository.save(process);
    }

}
