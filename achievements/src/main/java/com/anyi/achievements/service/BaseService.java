package com.anyi.achievements.service;

import com.anyi.achievements.repository.archives.ArchivesProcessRecordRepository;
import com.anyi.achievements.repository.archives.ArchivesProcessRepository;
import com.anyi.achievements.repository.archives.ArchivesUserRepository;
import com.anyi.achievements.repository.archives.PerformanceArchivesRepository;
import com.anyi.achievements.repository.archives.personal.PersonalArchivesProcessRepository;
import com.anyi.achievements.repository.archives.personal.PersonalArchivesRepository;
import com.anyi.achievements.repository.certification.CertificationRecordRepository;
import com.anyi.achievements.repository.common.CommonResourceRepository;
import com.anyi.achievements.repository.common.ResourceFileInfoRepository;
import com.anyi.achievements.repository.feedback.FeedbackRepository;
import com.anyi.achievements.repository.inform.InformRepository;
import com.anyi.achievements.repository.log.OperateLogRepository;
import com.anyi.achievements.repository.notepad.NotepadRepository;
import com.anyi.achievements.repository.notice.NoticeRepository;
import com.anyi.achievements.repository.notice.NoticeScopeRepository;
import com.anyi.achievements.repository.phone.PhoneCodeRepository;
import com.anyi.achievements.repository.score.UserScoreRecordRepository;
import com.anyi.achievements.repository.score.UserScoreRepository;
import com.anyi.achievements.repository.task.TaskRepository;
import com.anyi.achievements.repository.task.TaskScopeRepository;
import com.anyi.achievements.repository.unit.DeptRepository;
import com.anyi.achievements.repository.unit.UnitRepository;
import com.anyi.achievements.repository.unit.UnitRoleRepository;
import com.anyi.achievements.repository.user.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author yanhu
 * @version 2019/6/7 下午 04:07
 */
public abstract class BaseService {

    @Autowired
    protected UserInfoRepository userInfoRepository;

    @Autowired
    protected UserAuthRepository userAuthRepository;

    @Autowired
    protected UserAccountRepository userAccountRepository;

    @Autowired
    protected UserScoreRepository userScoreRepository;

    @Autowired
    protected UserScoreRecordRepository userScoreRecordRepository;

    @Autowired
    protected NoticeRepository noticeRepository;

    @Autowired
    protected CommonResourceRepository commonResourceRepository;

    @Autowired
    protected UnitRepository unitRepository;

    @Autowired
    protected PhoneCodeRepository phoneCodeRepository;

    @Autowired
    protected NoticeScopeRepository noticeScopeRepository;

    @Autowired
    protected DeptRepository deptRepository;

    @Autowired
    protected TaskRepository taskRepository;

    @Autowired
    protected TaskScopeRepository taskScopeRepository;

    @Autowired
    protected CertificationRepository certificationRepository;

    @Autowired
    protected NotepadRepository notepadRepository;

    @Autowired
    protected CertificationRecordRepository certificationRecordRepository;

    @Autowired
    protected UnitRoleRepository unitRoleRepository;

    @Autowired
    protected ResourceFileInfoRepository resourceFileInfoRepository;

    @Autowired
    protected PersonalArchivesProcessRepository personalArchivesProcessRepository;

    @Autowired
    protected PersonalArchivesRepository personalArchivesRepository;

    @Autowired
    protected PerformanceArchivesRepository performanceArchivesRepository;

    @Autowired
    protected ArchivesProcessRepository archivesProcessRepository;

    @Autowired
    protected ArchivesProcessRecordRepository archivesProcessRecordRepository;

    @Autowired
    protected UserExperienceRepository userExperienceRepository;

    @Autowired
    protected UserInformRepository userInformRepository;

    @Autowired
    protected OperateLogRepository operateLogRepository;

    @Autowired
    protected InformRepository informRepository;

    @Autowired
    protected FeedbackRepository feedbackRepository;

    @Autowired
    protected ArchivesUserRepository archivesUserRepository;

}
