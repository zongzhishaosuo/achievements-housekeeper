package com.anyi.achievements.controller.app;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.entity.certification.Certification;
import com.anyi.achievements.service.app.certification.CertificationServiceImpl;
import com.anyi.achievements.vo.CertificationVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 实名认证
 *
 * @author yanhu
 * @version 2019/6/16 下午 06:33
 */
@RestController
@RequestMapping("/app/certification")
public class CertificationController {


    @Autowired
    private CertificationServiceImpl certificationService;

    /**
     * 获取自身实名认证记录
     *
     * @param userId
     * @return
     */
    @GetMapping("/get/{userId}")
    public DTO<?> get(@PathVariable String userId) {
        return certificationService.getOneself(userId);
    }


    @GetMapping("/get/byId/{id}")
    public DTO<?> getById(@PathVariable Long id) {
        return certificationService.getById(id);
    }

    /**
     * 新增或修改实名认证记录
     *
     * @param certification
     * @return
     */
    @PostMapping("/save")
    public DTO<?> save(@RequestBody Certification certification) {
        return certificationService.addOrUpdate(certification);
    }

    /**
     * 获取下级提交的实名认证记录
     *
     * @param vo
     * @return
     */
    @PostMapping("/subCertPage")
    public DTO<?> subCertPage(@RequestBody CertificationVO vo) {
        return certificationService
                .getSubordinateCertificationPage(
                        vo.getVerifier(), vo.getStatusList(),
                        vo.getNumber(), vo.getSize()
                );
    }

    /**
     * 上级审核人处理实名认证
     *
     * @param vo
     * @return
     */
    @PostMapping("/handle")
    public DTO<?> handle(@RequestBody CertificationVO vo) {
        return certificationService
                .verifyHandle(vo.getUserId(), vo.getStatus(), vo.getRemark());
    }
}
