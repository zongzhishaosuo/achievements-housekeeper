package com.anyi.achievements.controller.app;

import com.anyi.achievements.bo.UserBO;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.entity.user.UserExperience;
import com.anyi.achievements.service.app.user.ExperienceServiceImpl;
import com.anyi.achievements.service.app.user.UserServiceImpl;
import com.anyi.achievements.util.JwtTokenUtils;
import com.anyi.achievements.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * 人员接口
 *
 * @author yanhu
 * @version 2019/6/8 下午 10:49
 */
@RestController
@RequestMapping("/app/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private ExperienceServiceImpl experienceService;

    /**
     * 人员注册
     *
     * @param userBO
     * @return
     */
    @PostMapping("/register")
    public DTO<UserBO> register(@RequestBody UserBO userBO, HttpServletResponse response) {
        DTO<UserBO> userBODTO = userService.register(userBO);
        awardToken(userBODTO, response);
        return userBODTO;
    }

    /**
     * 手机号+密码登录
     *
     * @param vo
     * @return
     */
    @PostMapping("/login")
    public DTO<UserBO> login(@RequestBody UserVO vo, HttpServletResponse response) {
        DTO<UserBO> userBODTO =
                userService.login(
                        vo.getWay(),
                        vo.getPhone(),
                        vo.getPassword(),
                        vo.getCode());
        awardToken(userBODTO, response);
        return userBODTO;
    }

    /**
     * 授权 token
     *
     * @param response
     * @param userBODTO
     */
    private void awardToken(DTO<UserBO> userBODTO, HttpServletResponse response) {
        if (userBODTO.isSuccess()) {
            String userId = userBODTO.getData().getUserInfo().getUserId();
            if (null != userId) {
                String token = JwtTokenUtils.createToken(userId);
                response.setHeader("token", token);
                redisTemplate.opsForValue().set(userId, token);
            }
        }
    }

    /**
     * 更新人员信息
     *
     * @param userBO
     * @return
     */
    @PostMapping("/update")
    public DTO<UserBO> update(@RequestBody UserBO userBO) {
        return userService.updateUser(userBO);
    }

    /**
     * 获取人员列表
     *
     * @param vo
     * @return
     */
    @PostMapping("/page")
    public DTO<?> userInfoPage(@RequestBody UserVO vo) {
        return userService.getUserInfoPage(vo.getUnitCode(),
                vo.getDeptCode(), vo.getRoleCode(),
                vo.getNumber(), vo.getSize());
    }

    /**
     * 通过手机号获取人员信息
     *
     * @param phone
     * @return
     */
    @GetMapping("/get/byPhone/{phone}")
    public DTO<?> getByPhone(@PathVariable String phone) {
        return userService.getByPhone(phone);
    }

    /**
     * 保存个人经历
     *
     * @param experience
     * @return
     */
    @PostMapping("/experience/save")
    public DTO<?> saveExperience(@RequestBody UserExperience experience) {
        return experienceService.save(experience);
    }

    /**
     * 获取个人经历
     *
     * @param userId
     * @return
     */
    @GetMapping("/experience/get/{userId}")
    public DTO<?> getExperience(@PathVariable String userId) {
        return experienceService.get(userId);
    }

}
