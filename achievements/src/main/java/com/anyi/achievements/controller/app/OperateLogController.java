package com.anyi.achievements.controller.app;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.app.log.OperateLogServiceImpl;
import com.anyi.achievements.vo.OperateLogVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 操作日志
 *
 * @author yh
 * @version 2019/7/15 上午 11:40
 */
@RestController
@RequestMapping("/app/operateLog")
public class OperateLogController {

    @Autowired
    private OperateLogServiceImpl operateLogService;

    @PostMapping("/page")
    public DTO<?> page(@RequestBody OperateLogVO vo) {
        return operateLogService.page(vo.getNumber(), vo.getSize());
    }

}
