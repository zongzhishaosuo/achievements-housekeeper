package com.anyi.achievements.controller.app;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.app.score.ScoreServiceImpl;
import com.anyi.achievements.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 积分接口
 *
 * @author yanhu
 * @version 2019/6/12 下午 10:05
 */
@RestController
@RequestMapping("/app/score")
public class ScoreController {

    @Autowired
    private ScoreServiceImpl scoreService;

    /**
     * 获取个人积分排名
     *
     * @param rule
     * @param userId
     * @return
     */
    @GetMapping("/ranking/{rule}/{userId}")
    public DTO<?> ranking(@PathVariable Integer rule,
                          @PathVariable String userId) {
        return scoreService.getScoreRanking(userId, rule);
    }

    /**
     * 获取个人历年积分
     *
     * @param userId
     * @return
     */
    @GetMapping("/overYearsScore/{userId}")
    public DTO<?> overYearsScore(@PathVariable String userId) {
        return scoreService.getOverTheYearsScore(userId);
    }

    /**
     * 获取积分明细记录
     *
     * @param vo
     * @return
     */
    @PostMapping("/getScoreRecord/page")
    public DTO<?> getScoreRecordPage(@RequestBody UserVO vo) {
        return scoreService.getScoreRecordPage(vo.getUserId(), vo.getAward(),
                vo.getWay(), vo.getNumber(), vo.getSize());
    }

}
