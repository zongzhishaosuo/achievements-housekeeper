package com.anyi.achievements.controller.admin;

import com.anyi.achievements.bo.UserBO;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.admin.User2ServiceImpl;
import com.anyi.achievements.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author yanhu
 * @version 2019/7/30 下午 10:21
 */
@RestController
@RequestMapping("/admin/user")
public class User2Controller {


    @Autowired
    private User2ServiceImpl user2Service;

    @PostMapping("/page")
    public DTO<?> page(@RequestBody UserVO vo) {
        return user2Service.page(vo.getUnitCode(), vo.getDeptCode(), vo.getRoleCode(),
                vo.getName(), vo.getIdNumber(), vo.getPhone(),
                vo.getNumber(), vo.getSize());
    }

    @GetMapping("/details/{userId}")
    public DTO<?> details(@PathVariable String userId) {
        return user2Service.details(userId);
    }

    @PostMapping("/add")
    public DTO<?> add(@RequestBody UserBO userBO) {
        return user2Service.add(userBO);
    }

    @PostMapping("/addBatch")
    public DTO<?> addBatch(@RequestBody List<UserBO> userBOList) {
        return user2Service.addBatch(userBOList);
    }

}
