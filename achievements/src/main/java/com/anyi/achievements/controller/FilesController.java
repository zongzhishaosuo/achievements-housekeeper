package com.anyi.achievements.controller;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.FilesUploadServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件操作
 *
 * @author yanhu
 * @version 2019/6/13 上午 11:37
 */
@RestController
@RequestMapping("/files")
public class FilesController {

    @Autowired
    private FilesUploadServiceImpl filesUploadService;

    @PostMapping("/upload")
    public DTO<?> upload(MultipartFile file) {
        return filesUploadService.upload(file, false);
    }

    @PostMapping("/upload/headImg")
    public DTO<?> uploadHeadImg(MultipartFile file) {
        return filesUploadService.upload(file, true);
    }
    
}
