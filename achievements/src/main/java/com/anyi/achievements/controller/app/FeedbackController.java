package com.anyi.achievements.controller.app;

import com.anyi.achievements.bo.FeedbackBO;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.app.feedback.FeedbackServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yh
 * @version 2019/7/15 上午 11:59
 */
@RestController
@RequestMapping("/app/feedback")
public class FeedbackController {

    @Autowired
    private FeedbackServiceImpl feedbackService;

    @PostMapping("/add")
    public DTO<?> add(@RequestBody FeedbackBO bo) {
        return feedbackService.add(bo);
    }

}
