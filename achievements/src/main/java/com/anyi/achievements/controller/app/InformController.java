package com.anyi.achievements.controller.app;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.app.inform.InformServiceImpl;
import com.anyi.achievements.vo.InformVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 消息通知
 *
 * @author yh
 * @version 2019/7/15 上午 11:32
 */
@RestController
@RequestMapping("/app/inform")
public class InformController {

    @Autowired
    private InformServiceImpl informService;

    @PostMapping("/page")
    public DTO<?> page(@RequestBody InformVO vo) {
        return informService.page(vo.getInformer(), vo.getNumber(), vo.getSize());
    }

    @PostMapping("/read")
    public DTO<?> read(@RequestBody InformVO vo) {
        return informService.read(vo.getIds());
    }

}
