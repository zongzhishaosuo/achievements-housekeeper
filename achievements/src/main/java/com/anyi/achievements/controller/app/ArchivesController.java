package com.anyi.achievements.controller.app;

import com.anyi.achievements.bo.PerformanceArchivesBO;
import com.anyi.achievements.bo.PersonalArchivesBO;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.entity.archives.ArchivesProcess;
import com.anyi.achievements.service.app.archives.PerformanceArchivesServiceImpl;
import com.anyi.achievements.service.app.archives.PersonalArchivesServiceImpl;
import com.anyi.achievements.vo.ArchivesVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author yanhu
 * @version 2019/6/20 下午 04:23
 */
@RestController
@RequestMapping("/app/archives")
public class ArchivesController {

    @Autowired
    private PersonalArchivesServiceImpl personalArchivesService;

    @Autowired
    private PerformanceArchivesServiceImpl performanceArchivesService;

    @PostMapping("/personal/add")
    public DTO<?> personalAdd(@RequestBody PersonalArchivesBO bo) {
        return personalArchivesService.add(bo);
    }

    @PostMapping("/personal/modify")
    public DTO<?> personalModify(@RequestBody PersonalArchivesBO bo) {
        return personalArchivesService.modify(bo);
    }

    @GetMapping("/personal/submit/{id}")
    public DTO<?> personalSubmit(@PathVariable Long id) {
        return personalArchivesService.submit(id);
    }

    @GetMapping("/personal/getOwn/{userId}")
    public DTO<?> personalGetOwn(@PathVariable String userId) {
        return personalArchivesService.getOwnArchives(userId);
    }

    @GetMapping("/personal/getById/{id}")
    public DTO<?> personalById(@PathVariable Long id) {
        return personalArchivesService.getById(id);
    }

    @PostMapping("/personal/submittedPage")
    public DTO<?> personalSubmittedPage(@RequestBody ArchivesVO vo) {
        return personalArchivesService.getSubmittedPage(
                vo.getProcessor(), vo.getProcessed(),
                vo.getPassed(), vo.getNumber(), vo.getSize());
    }

    @PostMapping("/personal/process")
    public DTO<?> personalProcess(@RequestBody ArchivesVO vo) {
        return personalArchivesService.process(vo.getArchivesId(), vo.getPassed(), vo.getRemark());
    }

    @PostMapping("/performance/save")
    public DTO<?> performanceSave(@RequestBody PerformanceArchivesBO bo) {
        return performanceArchivesService.save(bo);
    }

    @PostMapping("/performance/submit")
    public DTO<?> performanceSubmit(@RequestBody ArchivesProcess process) {
        return performanceArchivesService.submit(process);
    }

    @PostMapping("/performance/own/page")
    public DTO<?> performanceOwnPage(@RequestBody ArchivesVO vo) {
        return performanceArchivesService
                .ownPage(vo.getUserId(), vo.getLocationMain(), vo.getBelong(),
                        vo.getYear(), vo.getNumber(), vo.getSize());
    }

    @GetMapping("/performance/details/{id}")
    public DTO<?> performanceDetails(@PathVariable Long id) {
        return performanceArchivesService.archivesDetails(id);
    }

    @PostMapping("/performance/process/page")
    public DTO<?> performanceProcessPage(@RequestBody ArchivesVO vo) {
        return performanceArchivesService
                .processPage(vo.getProcessor(), vo.getProcessed(),
                        vo.getLocationMain(), vo.getBelong(), vo.getYear(),
                        vo.getNumber(), vo.getSize());
    }

    @PostMapping("/performance/process/handle")
    public DTO<?> performanceProcessHandle(@RequestBody ArchivesProcess process) {
        return performanceArchivesService.processHandle(process);
    }

    @PostMapping("/performance/process/final")
    public DTO<?> performanceFinalHandle(@RequestBody ArchivesVO vo) {
        return performanceArchivesService.finalHandle(vo.getIds(), vo.getComment());
    }

}
