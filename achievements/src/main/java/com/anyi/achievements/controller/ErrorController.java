package com.anyi.achievements.controller;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.dto.error.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 处理404,500等
 *
 * @date 2018/3/13
 */
@Slf4j
@RestController
public class ErrorController implements org.springframework.boot.web.servlet.error.ErrorController {

    private static final String ERROR_PATH = "/error";

    @RequestMapping(value = ERROR_PATH)
    public DTO<?> handleError(HttpServletRequest request) {
        Integer statusCode = (Integer) (request.getAttribute("javax.servlet.error.status_code"));
        log.error("服务器异常: " + statusCode);
        return DTO.error(judgeStatusCode(statusCode));
    }

    @Override
    public String getErrorPath() {
        // TODO Auto-generated method stub
        return ERROR_PATH;
    }

    /**
     * 判断状态码返回不同错误码
     *
     * @param statusCode
     * @return
     */
    private ErrorCode judgeStatusCode(Integer statusCode) {
        ErrorCode errorCode = ErrorCode.server_an_undefined_exception;
        if (null != statusCode) {
            switch (statusCode) {
                case 400:
                    errorCode = ErrorCode.server_object_params_inconformity;
                    break;
                case 404:
                    errorCode = ErrorCode.server_request_url_not_found;
                    break;
                case 500:
                    errorCode = ErrorCode.server_error;
                    break;
            }
        }
        return errorCode;
    }
}
