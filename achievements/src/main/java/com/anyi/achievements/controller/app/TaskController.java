package com.anyi.achievements.controller.app;

import com.anyi.achievements.bo.TaskBO;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.app.task.TaskServiceImpl;
import com.anyi.achievements.vo.TaskVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author yanhu
 * @version 2019/6/16 下午 01:59
 */
@RestController
@RequestMapping("/app/task")
public class TaskController {

    @Autowired
    private TaskServiceImpl taskService;

    /**
     * 发布任务
     *
     * @param bo
     * @return
     */
    @PostMapping("/release")
    public DTO<?> release(@RequestBody TaskBO bo) {
        return taskService.releaseTask(bo);
    }

    /**
     * 更新任务
     *
     * @param bo
     * @return
     */
    @PostMapping("/update")
    public DTO<?> update(@RequestBody TaskBO bo) {
        return taskService.updateTask(bo);
    }

    /**
     * 删除任务
     *
     * @param vo
     * @return
     */
    @PostMapping("/delete")
    public DTO<?> delete(@RequestBody TaskVO vo) {
        return taskService.deleteTask(vo.getIds());
    }

    /**
     * 获取任务列表
     *
     * @param vo
     * @return
     */
    @PostMapping("/page")
    public DTO<?> page(@RequestBody TaskVO vo) {
        return taskService.page(vo.getUserId(), vo.getStatus(), vo.getNumber(), vo.getSize());
    }

    /**
     * 获取列表
     *
     * @param vo
     * @return
     */
    @PostMapping("/list")
    public DTO<?> list(@RequestBody TaskVO vo) {
        return taskService.list(vo.getUserId(), vo.getStatus());
    }

    /**
     * 获取自身的
     *
     * @param vo
     * @return
     */
    @PostMapping("/page/own")
    public DTO<?> ownPage(@RequestBody TaskVO vo) {
        Integer status = null;
        if (null != vo.getStatus()) {
            status = Integer.parseInt(vo.getStatus());
        }
        return taskService.ownPage(vo.getAuthor(), status, vo.getNumber(), vo.getSize());
    }

    /**
     * 获取任务详情
     *
     * @param id
     * @return
     */
    @GetMapping("/details/{id}")
    public DTO<?> details(@PathVariable Long id) {
        return taskService.getTaskDetails(id);
    }
}
