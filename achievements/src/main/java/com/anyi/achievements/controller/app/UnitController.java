package com.anyi.achievements.controller.app;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.app.unit.UnitServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 单位接口
 *
 * @author yanhu
 * @version 2019/6/9 下午 05:09
 */
@RestController
@RequestMapping("/app/unit")
public class UnitController {

    @Autowired
    private UnitServiceImpl unitService;

    /**
     * 获取所有单位及其下所有部门
     *
     * @return
     */
    @GetMapping("/all")
    public DTO<?> unitAll() {
        return unitService.getAllUnitAndDept();
    }

    /**
     * 获取上级角色
     *
     * @param roleCode
     * @return
     */
    @GetMapping("/superRole/{roleCode}")
    public DTO<?> superRole(@PathVariable String roleCode) {
        return unitService.getSuperUnitAndRole(roleCode);
    }
}
