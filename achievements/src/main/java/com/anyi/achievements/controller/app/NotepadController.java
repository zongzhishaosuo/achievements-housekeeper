package com.anyi.achievements.controller.app;

import com.anyi.achievements.bo.NotepadBO;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.app.notepad.NotepadServiceImpl;
import com.anyi.achievements.vo.NotepadVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 记事本接口
 *
 * @author yanhu
 * @version 2019/6/13 下午 09:09
 */
@RestController
@RequestMapping("/app/notepad")
public class NotepadController {


    @Autowired
    private NotepadServiceImpl notepadService;

    /**
     * 新增记事本
     *
     * @param notepadBO
     * @return
     */
    @PostMapping("/add")
    public DTO<?> add(@RequestBody NotepadBO notepadBO) {
        return notepadService.addNotepad(notepadBO);
    }

    /**
     * 更新记事本
     *
     * @param notepadBO
     * @return
     */
    @PostMapping("/update")
    public DTO<?> update(@RequestBody NotepadBO notepadBO) {
        return notepadService.updateNotepad(notepadBO);
    }

    /**
     * 删除记事本
     *
     * @param vo
     * @return
     */
    @PostMapping("/delete")
    public DTO<?> delete(@RequestBody NotepadVO vo) {
        return notepadService.deleteNotepad(vo.getIds());
    }

    /**
     * 获取记事本列表
     *
     * @param vo
     * @return
     */
    @PostMapping("/page")
    public DTO<?> page(@RequestBody NotepadVO vo) {
        return notepadService.notepadPage(vo.getUserId(), vo.getNumber(), vo.getSize());
    }

}
