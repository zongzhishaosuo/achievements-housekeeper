package com.anyi.achievements.controller.app;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.app.phone.PhoneServiceImpl;
import com.anyi.achievements.vo.PhoneVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author yanhu
 * @version 2019/6/15 下午 09:27
 */
@RestController
@RequestMapping("/app/phone")
public class PhoneController {


    @Autowired
    private PhoneServiceImpl phoneService;

    /**
     * 发送短信验证码
     *
     * @param phone
     * @return
     */
    @GetMapping("/code/send/{phone}")
    public DTO<?> send(@PathVariable String phone) {
        return phoneService.sendCode(phone);
    }

    /**
     * 验证短信验证码
     *
     * @param vo
     * @return
     */
    @PostMapping("/code/verify")
    public DTO<?> verify(@RequestBody PhoneVO vo) {
        return phoneService.verifyCode(vo.getPhone(), vo.getCode());
    }
}
