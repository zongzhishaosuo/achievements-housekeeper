package com.anyi.achievements.controller;

import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.TestServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yanhu
 * @version 2019/6/14 下午 04:13
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private TestServiceImpl testService;

    @GetMapping("/test")
    public DTO<?> test() {
        return testService.addUser();
    }

}
