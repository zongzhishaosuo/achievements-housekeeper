package com.anyi.achievements.controller.app;

import com.anyi.achievements.bo.NoticeBO;
import com.anyi.achievements.dto.DTO;
import com.anyi.achievements.service.app.notice.NoticeServiceImpl;
import com.anyi.achievements.vo.NoticeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 公告接口
 *
 * @author yanhu
 * @version 2019/6/9 下午 01:35
 */
@RestController
@RequestMapping("/app/notice")
public class NoticeController {


    @Autowired
    private NoticeServiceImpl noticeService;

    /**
     * 发布公告
     *
     * @param noticeBO
     * @return
     */
    @PostMapping("/release")
    public DTO<?> release(@RequestBody NoticeBO noticeBO) {
        return noticeService.releaseNotice(noticeBO);
    }

    /**
     * 更新公告
     *
     * @param noticeBO
     * @return
     */
    @PostMapping("/update")
    public DTO<?> update(@RequestBody NoticeBO noticeBO) {
        return noticeService.updateNotice(noticeBO);
    }

    /**
     * 删除公告
     *
     * @param vo
     * @return
     */
    @PostMapping("/delete")
    public DTO<?> delete(@RequestBody NoticeVO vo) {
        return noticeService.deleteNotice(vo.getIds());
    }

    /**
     * 分页获取公告
     *
     * @param vo
     * @return
     */
    @PostMapping("/page")
    public DTO<?> page(@RequestBody NoticeVO vo) {
        return noticeService.getNoticePage(vo.getUserId(), vo.getStatus(), vo.getNumber(), vo.getSize());
    }

    @PostMapping("/list")
    public DTO<?> list(@RequestBody NoticeVO vo) {
        return noticeService.getNoticeList(vo.getUserId(), vo.getStatus());
    }

    @PostMapping("/page/own")
    public DTO<?> ownPage(@RequestBody NoticeVO vo) {
        Integer status = null;
        if (null != vo.getStatus()) {
            status = Integer.parseInt(vo.getStatus());
        }
        return noticeService.getOwnPage(vo.getAuthor(), status, vo.getNumber(), vo.getSize());
    }

    /**
     * 获取公告详情
     *
     * @param id
     * @return
     */
    @GetMapping("/details/{id}")
    public DTO<?> details(@PathVariable Long id) {
        return noticeService.getNoticeDetails(id);
    }
}
